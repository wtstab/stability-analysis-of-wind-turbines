# Info

This folder contains turbine models and result files used for the book.

The folders are organized as:

- `IEA10MW_github_repo`: submodule of the Github repo of the IEA 10 MW turbine. It should not be modified, except for pulling updates.
- `IEA10MW_local` Local folder containg files for the IEA 10 MW. Contains the htc and results files. This is where all the work should happen.