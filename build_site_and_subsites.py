#%%
import git
import os
import subprocess
import shutil
import glob

repodir = os.getcwd()
repo = git.Repo(repodir)

remote_refs = [ref for ref in repo.remote().refs if ref not in repo.remote().stale_refs]
# default_branch = os.path.basename(repo.git.symbolic_ref("refs/remotes/origin/HEAD"))
default_branch = "master"

print(f"Building site with '{default_branch}' as base-site.")
print(f"Building {[branch.name for branch in remote_refs]} as sub-sites")

#%%
os.makedirs("branch_builds", exist_ok=True)
os.makedirs("public", exist_ok=True)

def buildsite(branch):
    repo.git.checkout(branch, force=True)
    repo.git.pull(branch.name.split("/"))
    p = subprocess.run(["jupyter-book", "build", "./wtstab_jupyter_book/", "--path-output", f"./branch_builds/{os.path.basename(branch.name)}"],
                    capture_output=False)
    return p      
            
for idx, branch in enumerate(remote_refs):
    if "HEAD" in branch.name:
        continue
    print(f"Building {branch.name}")
    p = buildsite(branch)
    if p.returncode != 0:
        print(f"ERROR building sub site for {branch.name}")
        try:            
            os.remove(f"./branch_builds/{os.path.basename(branch.name)}")
        except FileNotFoundError:
            pass
        print("Local build removed")
        #os.makedirs(f"public/{branch}", exist_ok=True)
        #shutil.move(f"./branch_builds/{branch}/_build/html", f"./wtstab_jupyter_book/_build/html/{os.path.basename(branch.name)}",)

# Move default branch
shutil.move(f"./branch_builds/{default_branch}/_build/", "./wtstab_jupyter_book/")
os.makedirs("./wtstab_jupyter_book/_build/html/branches", exist_ok=True)

branch_builds = glob.glob("branch_builds/*")
for branch in branch_builds:
    if os.path.basename(branch).lower() == default_branch:
        continue 
    shutil.move(f"{branch}/_build/html", f"./wtstab_jupyter_book/_build/html/branches/{os.path.basename(branch)}")
