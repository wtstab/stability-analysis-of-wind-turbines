# -*- coding: utf-8 -*-
"""
Plot PSD of loads when the turbine is forced by the wind and an external excitation.

@author: ricriv
"""

# %% Import.

import matplotlib as mpl
import matplotlib.pyplot as plt
import numpy as np
import pandas as pd
from scipy import signal
from wetb.prepost import windIO

# %% Set up.

mpl.use("Agg")
# mpl.use('Qt5Agg')

plt.style.use("rcparams.mplstyle")

plt.close("all")


# %% Load data.

# File name of the HAWC2 response file.
res_folder = "./../numerical_data/IEA10MW_local/res/"
res_file_name = "anisotropic_shake_1.hdf5"

# Load the data.
res = windIO.LoadResults(file_path=res_folder, file_name=res_file_name)
channel_names = [str(res.ch_details[i, 2]) for i in range(1, res.Nch)]
df_time = pd.DataFrame(
    data=res.sig[:, 1:],
    index=pd.Index(data=res.sig[:, 0], name="Time [s]"),
    columns=channel_names,
)

time_start = 302.0

# Compute average rotor speed [Hz].
rotor_speed = np.mean(df_time.loc[time_start:, "shaft_rot angle speed"]) / 60.0

# Compute PSD.
dt = np.mean(np.diff(res.sig[:, 0]))
sampling_frequency = 1 / dt
nyquist_frequency = sampling_frequency / 2

window = np.ones(df_time.loc[time_start:, :].index.size)
noverlap = 0
nperseg = len(window)
nfft = nperseg
detrend = "constant"
return_onesided = True
scaling = "density"

y_freq, y_PSD = signal.welch(
    df_time.loc[time_start:, :],
    sampling_frequency,
    window,
    nperseg,
    noverlap,
    nfft,
    detrend,
    return_onesided,
    scaling,
    axis=0,
)

df_psd = pd.DataFrame(
    data=y_PSD,
    index=pd.Index(data=y_freq, name="Frequency [Hz]"),
    columns=channel_names,
)


# %% Plot PSD.

fig_name = "anisotropic_shake_1"
fig, ax = plt.subplots(num=fig_name, figsize=(6, 6 / 1.62), dpi=300)
#
ax.set_xlabel("Frequency [Hz]")
ax.set_ylabel("Moment [(kNm)²/Hz]")
ax.set_yscale("log")
ax.plot(
    df_psd.index.to_numpy(),
    df_psd.loc[:, "MomentMy Mbdy:blade1 nodenr:   1 coo: blade1"],
    label="Blade root edgewise",
)
ax.plot(
    df_psd.index.to_numpy(),
    df_psd.loc[:, "MomentMy Mbdy:tower nodenr:   1 coo: tower"],
    label="Tower bottom side-side",
)
# ax.plot(df_psd.index.to_numpy(), df_psd.loc[:, 'MomentMx Mbdy:tower nodenr:   1 coo: tower'], label='Tower bottom fore-aft')
ax.plot(
    df_psd.index.to_numpy(),
    df_psd.loc[:, "MomentMz Mbdy:shaft nodenr:   5 coo: shaft  outer generator rotor"],
    label="Shaft torsion",
)
ax.legend(loc="upper right")
ax.set_xlim(0.0, 1.5)
ax.set_ylim(1e-2, 1e12)

plt.tight_layout()
fig.savefig(
    "./../figures/IEA10MW/hawc2_wind_and_doublet_free_without_labels.svg",
    bbox_inches="tight",
)

# Name the modes and some nP.
max_psd = np.amax(
    np.column_stack(
        (
            # df_psd.loc[:, 'MomentMx Mbdy:tower nodenr:   1 coo: tower'].to_numpy(),
            df_psd.loc[:, "MomentMy Mbdy:tower nodenr:   1 coo: tower"].to_numpy(),
            df_psd.loc[:, "MomentMy Mbdy:blade1 nodenr:   1 coo: blade1"].to_numpy(),
            df_psd.loc[
                :, "MomentMz Mbdy:shaft nodenr:   5 coo: shaft  outer generator rotor"
            ].to_numpy(),
        )
    ),
    axis=1,
)

# 1P.
freq_peak = rotor_speed
i_freq = np.searchsorted(y_freq, freq_peak) - 1
ax.annotate(
    "1P",
    (y_freq[i_freq], max_psd[i_freq]),
    xytext=(y_freq[i_freq], max_psd[i_freq] * 100.0),
    arrowprops={"arrowstyle": "->", "color": "grey"},
    horizontalalignment="center",
    verticalalignment="bottom",
    color="grey",
    bbox={"boxstyle": "round", "facecolor": "white", "edgecolor": "grey"},
)

# Tower SS.
freq_peak = 0.226
i_freq = np.searchsorted(y_freq, freq_peak) - 1
ax.annotate(
    "Tower SS",
    (y_freq[i_freq], max_psd[i_freq]),
    xytext=(y_freq[i_freq] + 0.03, max_psd[i_freq] * 100.0),
    arrowprops={"arrowstyle": "->", "color": "black"},
    horizontalalignment="center",
    verticalalignment="bottom",
    color="black",
    bbox={"boxstyle": "round", "facecolor": "white", "edgecolor": "black"},
)

# 3P.
freq_peak = 3 * rotor_speed
i_freq = np.searchsorted(y_freq, freq_peak) - 1
ax.annotate(
    "3P",
    (y_freq[i_freq], max_psd[i_freq]),
    xytext=(y_freq[i_freq], max_psd[i_freq] * 100.0),
    arrowprops={"arrowstyle": "->", "color": "grey"},
    horizontalalignment="center",
    verticalalignment="bottom",
    color="grey",
    bbox={"boxstyle": "round", "facecolor": "white", "edgecolor": "grey"},
)

# Edge BW.
freq_peak = 0.553
i_freq = np.searchsorted(y_freq, freq_peak) - 1
ax.annotate(
    "Edge BW",
    (y_freq[i_freq], max_psd[i_freq]),
    xytext=(y_freq[i_freq], max_psd[i_freq] * 200.0),
    arrowprops={"arrowstyle": "->", "color": "black"},
    horizontalalignment="center",
    verticalalignment="bottom",
    color="black",
    bbox={"boxstyle": "round", "facecolor": "white", "edgecolor": "black"},
)

# Edge BW + Ω + Edge FW - Ω.
freq_peak = 0.694
i_freq = np.searchsorted(y_freq, freq_peak) - 1
ax.annotate(
    "Edge BW + Ω\n+\nEdge FW - Ω",
    (y_freq[i_freq], max_psd[i_freq]),
    xytext=(y_freq[i_freq], max_psd[i_freq] * 500.0),
    arrowprops={"arrowstyle": "->", "color": "black"},
    horizontalalignment="center",
    verticalalignment="bottom",
    color="black",
    bbox={"boxstyle": "round", "facecolor": "white", "edgecolor": "black"},
)

# Edge FW.
freq_peak = 0.829
i_freq = np.searchsorted(y_freq, freq_peak) - 1
ax.annotate(
    "Edge FW",
    (y_freq[i_freq], max_psd[i_freq]),
    xytext=(y_freq[i_freq], max_psd[i_freq] * 150.0),
    arrowprops={"arrowstyle": "->", "color": "black"},
    horizontalalignment="center",
    verticalalignment="bottom",
    color="black",
    bbox={"boxstyle": "round", "facecolor": "white", "edgecolor": "black"},
)

# Edge collective.
freq_peak = 1.27
i_freq = np.searchsorted(y_freq, freq_peak) - 1
ax.annotate(
    "Edge collective",
    (y_freq[i_freq], max_psd[i_freq]),
    xytext=(y_freq[i_freq], max_psd[i_freq] * 300.0),
    arrowprops={"arrowstyle": "->", "color": "black"},
    horizontalalignment="center",
    verticalalignment="bottom",
    color="black",
    bbox={"boxstyle": "round", "facecolor": "white", "edgecolor": "black"},
)

# 9P.
freq_peak = 9 * rotor_speed
i_freq = np.searchsorted(y_freq, freq_peak) - 1
ax.annotate(
    "9P",
    (y_freq[i_freq], max_psd[i_freq]),
    xytext=(y_freq[i_freq] - 0.1, max_psd[i_freq]),
    arrowprops={"arrowstyle": "->", "color": "grey"},
    horizontalalignment="center",
    verticalalignment="bottom",
    color="grey",
    bbox={"boxstyle": "round", "facecolor": "white", "edgecolor": "grey"},
)

plt.tight_layout()
fig.savefig(
    "./../figures/IEA10MW/hawc2_wind_and_doublet_free_with_labels.svg",
    bbox_inches="tight",
)
