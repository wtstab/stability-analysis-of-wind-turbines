# -*- coding: utf-8 -*-
"""
Counter example on why the time-frozen frequency and damping do not have any physical meaning.
Developed by Vinograd and described in S. Bittanti, P. Colaneri, Periodic systems filetring and control.

@author: ricriv
"""

# %% Import.

import os

import matplotlib as mpl
import matplotlib.pyplot as plt
import numpy as np
import numpy.testing as npt
from scipy.integrate import solve_ivp
from scipy.linalg import eig
from scipy.signal import welch

# %% Set up.

mpl.use("Agg")
# mpl.use('Qt5Agg')

plt.style.use("rcparams.mplstyle")

plt.close("all")

os.makedirs("./../figures/vinograd", exist_ok=True)


# %% Define Vinograd's system.


def amat_fun(t):
    """
    Compute matrix A(t).

    Parameters
    ----------
    t : float
        Time [s].

    Returns
    -------
    amat : ndarray, shape (2, 2)
        matrix A(t).
    """
    amat = np.zeros((2, 2))
    s = np.sin(6 * t)
    c = np.cos(6 * t)
    amat[0, 0] = -1 - 9 * c**2 + 12 * s * c
    amat[0, 1] = 12 * c**2 + 9 * s * c
    amat[1, 0] = -12 * s**2 + 9 * s * c
    amat[1, 1] = -1 - 9 * s**2 - 12 * s * c
    return amat


def sys_fun(t, x):
    """
    Compute A(t) x.

    Parameters
    ----------
    t : float
        Time [s].
    x : ndarray, shape (2,)
        State.

    Returns
    -------
    dx_dt : ndarray, shape (2,)
        State derivative.

    """
    return amat_fun(t) @ x


# System period.
period = np.pi / 3

# Test.
err = amat_fun(0.0) - amat_fun(period)


# %% Solve ODE.

# Define time array over a few periods.
time_long = np.linspace(0.0, 20, 10001)

# Set arbitrary initial conditions.
x0 = np.ones((2,))

# Integrate ODE.
sol = solve_ivp(
    sys_fun,
    (time_long[0], time_long[-1]),
    x0,
    t_eval=time_long,
    vectorized=True,
    # method="LSODA",
    # jac=amat_fun,
)

# Plot solution.
fig, ax = plt.subplots(dpi=300)
ax.set_xlabel("Time [s]")
ax.set_ylabel("Solution")
ax.plot(sol.t, sol.y[0, :], label="$x_1$")
ax.plot(sol.t, sol.y[1, :], label="$x_2$")
ax.set_xlim(0.0, 5.0)
ax.set_ylim(-30000, 20000)
ax.legend(loc="upper left")
plt.tight_layout()
fig.savefig("./../figures/vinograd/simulation_time_history.svg", bbox_inches="tight")

# Compute spectral properties.
dt = time_long[1]
sampling_frequency = 1.0 / dt
nyquist_frequency = sampling_frequency / 2.0
duration = time_long[-1] - time_long[0]
df = 1.0 / duration

# Compute PSD.
frequency, y_psd = welch(
    sol.y,
    fs=sampling_frequency,
    # window="hann",
    window=np.ones(sol.y.shape[1]),
    nperseg=sol.y.shape[1],
    noverlap=0,
    axis=1,
)

# Plot PSD.
fig, ax = plt.subplots(dpi=300)
ax.set_xlabel("Frequency [Hz]")
ax.set_ylabel("Signal PSD")
ax.grid(True)
ax.set_xlim(0.0, 3.0)
ax.set_ylim(1e31, 1e33)
ax.semilogy(frequency, y_psd[0, :], label="$x_1$")
ax.semilogy(frequency, y_psd[1, :], label="$x_2$")
ax.legend(loc="upper right")
plt.tight_layout()
fig.savefig("./../figures/vinograd/simulation_psd.svg", bbox_inches="tight")


# %% Compute time-frozen eigenvalues.

# Define time array over 1 period.
time_period = np.linspace(0.0, period, 10000)

# Compute eigenvalues for all time instants.
eig_val = np.zeros((2, time_period.size), dtype=np.complex128)
for i in range(time_period.size):
    eig_val[:, i] = eig(amat_fun(time_period[i]), right=False)

# Compute natural frequencies and damping ratios.
frequency_st = np.abs(eig_val)
damping_st = -np.real(eig_val) / frequency_st
frequency_st /= 2 * np.pi

# Lazy sort.
i_sort = np.argsort(frequency_st, axis=0)
frequency_st = np.take_along_axis(frequency_st, i_sort, axis=0)
damping_st = np.take_along_axis(damping_st, i_sort, axis=0)

# Plot natural frequencies and damping ratios.
fig, ax = plt.subplots(dpi=300)
ax.set_xlabel("Time [s]")
ax.set_ylabel("Natural frequency [Hz]")
ax.plot(time_period, frequency_st[0, :], label="Mode 1")
ax.plot(time_period, frequency_st[1, :], label="Mode 2")
ax.legend(loc="center left")
plt.tight_layout()
fig.savefig("./../figures/vinograd/short_term_frequency.svg", bbox_inches="tight")

fig, ax = plt.subplots(dpi=300)
ax.set_xlabel("Time [s]")
ax.set_ylabel("Damping ratio [%]")
ax.plot(time_period, damping_st[0, :] * 100, label="Mode 1")
ax.plot(time_period, damping_st[1, :] * 100, label="Mode 2")
ax.legend(loc="upper left")
plt.tight_layout()
fig.savefig("./../figures/vinograd/short_term_damping.svg", bbox_inches="tight")


# %% Apply Floquet theory.

# Compute monodromy matrix.
e2 = np.exp(2.0 * period)
em13 = np.exp(-13.0 * period)
monodromy = 0.2 * np.array(
    [[e2 + 4.0 * em13, 2.0 * e2 - 2.0 * em13], [2.0 * e2 - 2.0 * em13, 4.0 * e2 + em13]]
)

# Compute characteristic multipliers.
characteristic_multiplier = eig(monodromy, right=False)

# Test against analytical solution.
npt.assert_allclose(characteristic_multiplier.real, np.array([em13, e2]))

# Compute principal characteristic exponents.
characteristic_exponent_principal = np.log(characteristic_multiplier) / period

# Apply shift.
omega = 2.0 * np.pi / period
shift = np.arange(0, 3) * omega
characteristic_exponent = (
    characteristic_exponent_principal[np.newaxis, :] + shift[:, np.newaxis] * 1j
)

# Compute correct frequnecy and damping ratios.
natural_frequency_floquet = np.abs(characteristic_exponent)
damping_floquet = -np.real(characteristic_exponent) / natural_frequency_floquet
natural_frequency_floquet /= 2 * np.pi
damped_frequency_floquet = np.imag(characteristic_exponent) / (2 * np.pi)
