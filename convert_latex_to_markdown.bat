if not exist ".\markdown" mkdir .\markdown

for %%f in (.\chapters\*.tex) do (
    pandoc -s %%f -o .\markdown\%%~nf.md
)