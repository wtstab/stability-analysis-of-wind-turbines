# -*- coding: utf-8 -*-
"""
Plot HAWCStab2 results.

@author: ricriv
"""


# %% Initialization.

import os

import matplotlib as mpl
import matplotlib.pyplot as plt
import numpy as np
from routines_read_hs2 import (
    read_hs2_output_amp_blade,
    read_hs2_output_amp_turbine,
    read_hs2_output_cmb,
)

plt.close("all")

mpl.use("Agg")
# mpl.use('Qt5Agg')

plt.style.use("rcparams.mplstyle")
colors = mpl.rcParams["axes.prop_cycle"].by_key()["color"]

# Page dimensions.
# page_width_pt = 372.0 - 10.0
# page_height_pt = 601.90594 + 10

# page_width_in = page_width_pt / 72.27
# page_height_in = page_height_pt / 72.27

page_width_in = 25.0 / 2.54
page_height_in = page_width_in * 9 / 16


# %% Name blade modes.


def name_blade_modes(amp, op=0.0, cmb=None):
    """
    Name the blade modes.

    Parameters
    ----------
    amp : pandas DataFrame
        HAWCStab2 amp file containing the blade mode shapes.
        Must be provided by `read_hs2_output_amp_blade()`.
    op : float, optional
        Operational point, either wind speed [m/s] or rotor speed [rad/s]. The default is 0.0.
    cmb : pandas DataFrame, optional
        HAWCStab2 cmb file containing the blade frequencies and damping ratios.
        Must be provided by `read_hs2_output_cmb()`.
        It is needed to label the pitch actuator mode.

    Returns
    -------
    mode_names : list of strings
        Blade mode names.

    """
    # Check that op exists.
    assert op in amp.index.levels[0], f"Operational point {op} is not in the amp file."

    # Get tip radius.
    radius = amp.index[-1][1]

    # Number of modes.
    n_modes = amp.columns[-1][0]

    # Create mode names.
    # This will be overwritten.
    mode_names = [f"Mode {i+1}" for i in range(n_modes)]

    # Basic mode names. The order matches x, y and torsion.
    basic_modes = ("Edgewise", "Flapwise", "Torsional")

    # Mode number in each direction (edge, flap, torsional).
    i_number = np.zeros((len(basic_modes)), dtype=np.int32)

    # True if the pitch actuator has been found.
    pitch_actuator_found = False

    # Loop over the modes.
    for i_mode in range(n_modes):
        # Find the pitch actuator.
        if not pitch_actuator_found and cmb is not None:
            # Compute damping ratio gradient with respect to operational points.
            damp_diff = np.diff(cmb.loc[:, "Damping [%]"].to_numpy(copy=False), axis=0)
            # Look for constant damping ratio.
            if np.all(np.isclose(damp_diff[:, i_mode], 0.0)):
                pitch_actuator_found = True
                mode_names[i_mode] = "Pitch actuator"
                continue

        # Get the tip mode shape.
        # Magnitude.
        tip_mode_shape_mag = amp.loc[
            (op, radius), (i_mode, ["u_x bld [m]", "u_y bld [m]", "theta [rad]"])
        ].to_numpy(copy=False)
        # Phase.
        tip_mode_shape_ang = amp.loc[
            (op, radius), (i_mode, ["phase_x [deg]", "phase_y [deg]", "phase_t [deg]"])
        ].to_numpy(copy=False)

        # Compute deflection shape.
        tip_deflection_shape = tip_mode_shape_mag * np.cos(
            np.deg2rad(tip_mode_shape_ang)
        )

        # Find the maximum component.
        i_max = tip_deflection_shape.argmax()

        # Name the mode.
        i_number[i_max] += 1
        mode_names[i_mode] = f"{i_number[i_max]} {basic_modes[i_max]}"

    return mode_names


# %% Name turbine modes.


def name_turbine_modes(amp, op=0.0, cmb=None):
    """
    Name the turbine modes.

    Parameters
    ----------
    amp : pandas DataFrame
        HAWCStab2 amp file containing the turbine mode shapes.
        Must be provided by `read_hs2_output_amp_turbine()`.
    op : float, optional
        Operational point, either wind speed [m/s] or rotor speed [rad/s]. The default is 0.0.
    cmb : pandas DataFrame, optional
        HAWCStab2 cmb file containing the turbine frequencies and damping ratios.
        Must be provided by `read_hs2_output_cmb()`.
        It is needed to label the pitch actuator mode.

    Returns
    -------
    mode_names : list of strings
        Turbine mode names.

    """
    # Check that op exists.
    assert op in amp.index, f"Operational point {op} is not in the amp file."

    # Number of modes.
    n_modes = amp.columns.levels[0][-1]

    # Number of operational points.
    n_op = amp.shape[0]

    # Create mode names.
    # This will be overwritten.
    mode_names = [f"Mode {i+1}" for i in range(n_modes)]

    # The first mode is always the rigid rotor rotation.
    mode_names[0] = "Rotor rotation"

    # Basic mode names.
    basic_modes = (
        "Tower side-side",
        "Tower fore-aft",
        "Tower torsional",
        "Shaft horizontal",
        "Shaft vertical",
        "Shaft torsion",
        "Edgewise symmetric",
        "Edgewise backward whirl",
        "Edgewise forward whirl",
        "Flapwise symmetric",
        "Flapwise backward whirl",
        "Flapwise forward whirl",
        "Torsion symmetric",
        "Torsion backward whirl",
        "Torsion forward whirl",
    )

    # Mode number in each direction.
    i_number = np.zeros((len(basic_modes)), dtype=np.int32)

    # Find the 3 pitch actor modes.
    # Strategy if there is only 1 opearational point.
    if n_op == 1:
        # Compute gradient of damping with respect to modes.
        damp_diff = np.diff(cmb.loc[:, "Damping [%]"].to_numpy(copy=False))
        # Find the first pitch actuator mode.
        i_pitch = np.argmax(np.isclose(damp_diff, 0.0))
        mode_names[i_pitch] = "Pitch actuator backward whirl"
        mode_names[i_pitch + 1] = "Pitch actuator symmetric"
        mode_names[i_pitch + 2] = "Pitch actuator forward whirl"
    else:
        # Compute gradient of damping with respect to operational points.
        damp_diff = np.diff(cmb.loc[:, "Damping [%]"].to_numpy(copy=False), axis=0)
        # Look for constant damping ratio.
        i_pitch = np.argmax(np.all(np.isclose(damp_diff, 0.0), axis=0)[1:]) + 1
        mode_names[i_pitch] = "Pitch actuator"

    # Loop over the modes.
    # Skip the rigid rotor rotation.
    for i_mode in range(1, n_modes):
        # Name the 1st Tower Fore-Aft. This mode might be confused with one of the flap ones.
        if i_mode <= 3 and amp.loc[op, (i_mode, "TWR y mag. [m]")] > 0.7:
            i_number[1] = 1
            mode_names[i_mode] = "1 Tower fore-aft"
            continue

        # Skip the pitch actuator modes.
        if op == 0.0 and (i_mode >= i_pitch and i_mode <= i_pitch + 2):
            # standing still turbine structural analysis
            continue
        elif n_op > 1 and i_mode == i_pitch:
            # operating aeroelastic turbine analysis.
            continue

        # Get the mode shape.
        tip_mode_shape_mag = amp.loc[op, (i_mode,)].iloc[0:-1:2].to_numpy()
        # tip_mode_shape_ang = amp.loc[op, (i_mode,)].iloc[1:amp.shape[1]:2].to_numpy()

        # Compute deflection shape.
        # tip_deflection_shape = tip_mode_shape_mag * np.cos(np.deg2rad(tip_mode_shape_ang))

        # Find the maximum component.
        i_max = tip_mode_shape_mag.argmax()

        # Name the mode.
        i_number[i_max] += 1
        mode_names[i_mode] = f"{i_number[i_max]} {basic_modes[i_max]}"

    return mode_names


# %% Plot blade modes.


def plot_blade_modes(amp, op=0.0, mode_names=None):
    """
    Plot the blade modes.

    Parameters
    ----------
    amp : pandas DataFrame
        HAWCStab2 amp file containing the blade mode shapes.
        Must be provided by `read_hs2_output_amp_blade()`.
    op : float, optional
        Operational point, either wind speed [m/s] or rotor speed [rad/s]. The default is 0.0.
    mode_names: list of strings
        Blade mode names. If none, each mode will be called Mode n.

    Returns
    -------
    fig_list : list of figures
        All figures.
    """
    # Check that op exists.
    assert op in amp.index.levels[0], f"Operational point {op} is not in the amp file."

    # Number of modes.
    n_modes = amp.columns[-1][0]

    # Name the modes if needed.
    if mode_names is None:
        mode_names = [f"Mode {i+1}" for i in range(n_modes)]

    # Get the blade radius.
    radius = amp.index.levels[1]
    radius_round = np.ceil(radius[-1] / 10) * 10

    # Loop over the modes backward, so that mode 1 is on top.
    fig_list = []
    for i_mode in range(n_modes + 1):
        fig, ax = plt.subplots(
            nrows=3,
            ncols=3,
            sharex=True,
            num=f"Blade mode {i_mode+1}",
            figsize=(page_width_in, page_height_in),
            dpi=300,
        )
        fig_list.append(fig)

        # fig.suptitle(mode_names[i_mode])

        ax[0, 0].set_title("Magnitude [m,deg]")
        ax[0, 1].set_title("Phase [deg]")
        ax[0, 2].set_title("Deflection shape [m,deg]")
        ax[0, 0].set_ylabel("Edgewise")  # x
        ax[1, 0].set_ylabel("Flapwise")  # y
        ax[2, 0].set_ylabel("Torsion")
        ax[2, 0].set_xlabel("Radius [m]")
        ax[2, 1].set_xlabel("Radius [m]")
        ax[2, 2].set_xlabel("Radius [m]")

        ax[0, 0].set_ylim(0, 1)
        ax[1, 0].set_ylim(0, 1)
        # ax[2,0].set_ylim(bottom=0)
        ax[0, 1].set_ylim(-180, +180)
        ax[1, 1].set_ylim(-180, +180)
        ax[2, 1].set_ylim(-180, +180)

        ax[0, 0].set_xticks(np.arange(0.0, radius_round + 1, 10.0))
        ax[0, 1].set_xticks(np.arange(0.0, radius_round + 1, 10.0))
        ax[0, 2].set_xticks(np.arange(0.0, radius_round + 1, 10.0))

        ax[0, 1].set_yticks([-180, -90, 0, 90, 180])
        ax[1, 1].set_yticks([-180, -90, 0, 90, 180])
        ax[2, 1].set_yticks([-180, -90, 0, 90, 180])

        ax[0, 0].plot(radius, amp.loc[(op,), (i_mode, "u_x bld [m]")], color=colors[0])
        ax[1, 0].plot(radius, amp.loc[(op,), (i_mode, "u_y bld [m]")], color=colors[1])
        ax[2, 0].plot(
            radius,
            amp.loc[(op,), (i_mode, "theta [rad]")] * 180.0 / np.pi,
            color=colors[2],
        )

        ax[0, 1].plot(
            radius, amp.loc[(op,), (i_mode, "phase_x [deg]")], color=colors[0]
        )
        ax[1, 1].plot(
            radius, amp.loc[(op,), (i_mode, "phase_y [deg]")], color=colors[1]
        )
        ax[2, 1].plot(
            radius, amp.loc[(op,), (i_mode, "phase_t [deg]")], color=colors[2]
        )

        ax[0, 2].plot(
            radius,
            amp.loc[(op,), (i_mode, "u_x bld [m]")]
            * np.cos(np.deg2rad(amp.loc[(op,), (i_mode, "phase_x [deg]")])),
            color=colors[0],
        )
        ax[1, 2].plot(
            radius,
            amp.loc[(op,), (i_mode, "u_y bld [m]")]
            * np.cos(np.deg2rad(amp.loc[(op,), (i_mode, "phase_y [deg]")])),
            color=colors[1],
        )
        ax[2, 2].plot(
            radius,
            amp.loc[(op,), (i_mode, "theta [rad]")]
            * 180.0
            / np.pi
            * np.cos(np.deg2rad(amp.loc[(op,), (i_mode, "phase_t [deg]")])),
            color=colors[2],
        )

        defl_min = np.min([ax[0, 2].get_ylim()[0], ax[1, 2].get_ylim()[0]])
        defl_max = np.max([ax[0, 2].get_ylim()[1], ax[1, 2].get_ylim()[1]])

        ax[0, 2].set_ylim(defl_min, defl_max)
        ax[1, 2].set_ylim(defl_min, defl_max)

        plt.tight_layout()

    return fig_list


# %% Plot turbine modes.


def plot_turbine_mode_shapes(amp, op=0.0, mode_names=None):
    """
    Plot the turbine modes.

    Parameters
    ----------
    amp : pandas DataFrame
        HAWCStab2 amp file containing the blade mode shapes.
        Must be provided by `read_hs2_output_amp_blade()`.
    op : float, optional
        Operational point, either wind speed [m/s] or rotor speed [rad/s]. The default is 0.0.
    mode_names: list of strings
        Turbine mode names. If none, each mode will be called Mode n.

    Returns
    -------
    fig_list : list of figures
        All figures.
    """
    # Check that op exists.
    assert op in amp.index, f"Operational point {op} is not in the amp file."

    # Number of modes.
    n_modes = amp.columns[-1][0]

    # Name the modes if needed.
    if mode_names is None:
        mode_names = [f"Mode {i+1}" for i in range(n_modes)]

    # Channels to extract the magnitude and phase.
    channels_mag = [
        "TWR x mag. [m]",
        "TWR y mag. [m]",
        "TWR yaw mag. [rad]",
        "SFT x mag. [m]",
        "SFT y mag. [m]",
        "SFT tor mag. [rad]",
        "Sym edge mag. [m]",
        "BW edge mag. [m]",
        "FW edge mag. [m]",
        "Sym flap mag. [m]",
        "BW flap mag. [m]",
        "FW flap mag. [m]",
        "Sym tors mag. [rad]",
        "BW tors mag. [rad]",
        "FW tors mag. [rad]",
    ]

    channel_ang = [
        "TWR x phase [deg]",
        "TWR y phase [deg]",
        "TWR yaw phase [deg]",
        "SFT x phase [deg]",
        "SFT y phase [deg]",
        "SFT tor phase [deg]",
        "Sym edge phase [deg]",
        "BW edge phase [deg]",
        "FW edge phase [deg]",
        "Sym flap phase [deg]",
        "BW flap phase [deg]",
        "FW flap phase [deg]",
        "Sym tors phase [deg]",
        "BW tors phase [deg]",
        "FW tors phase [deg]",
    ]

    # Labels for plotting.
    xlabels = [
        "TWR x",
        "TWR y",
        "TWR yaw",
        "SFT x",
        "SFT y",
        "SFT tor",
        "Sym edge",
        "BW edge",
        "FW edge",
        "Sym flap",
        "BW flap",
        "FW flap",
        "Sym tors",
        "BW tors",
        "FW tors",
    ]
    dummy_x = np.arange(len(xlabels))

    # Loop over the modes backward, so that mode 1 is on top.
    fig_list = []

    for i_mode in range(n_modes - 1, -1, -1):
        fig, ax = plt.subplots(
            nrows=2, sharex=True, num=f"Turbine mode {i_mode+1}", dpi=300
        )
        fig_list.append(fig)

        fig.suptitle(mode_names[i_mode])
        ax[0].set_ylabel("Magnitude [m], [rad]")
        ax[1].set_ylabel("Phase [deg]")
        ax[1].set_ylim(-190.0, +190.0)
        ax[1].set_yticks(np.arange(-180, +181, 90))
        mag = amp.loc[op, (i_mode, channels_mag)].to_numpy(copy=False)
        ang = amp.loc[op, (i_mode, channel_ang)].to_numpy(copy=True)
        # Delete phase for very small magnitudes.
        ang[mag < 0.02] = np.nan
        ax[0].bar(dummy_x, mag)
        ax[1].bar(dummy_x, ang)
        ax[1].set_xticks(dummy_x, labels=xlabels, rotation=45)

    return fig_list


# %% Run script.

if __name__ == "__main__":
    # %% Plot blade structural modes.

    if True:
        # File name of the blade structural modes.
        # Frequencies and damping ratios.
        blade_struct_freqdamp_file_name = (
            "./../numerical_data/IEA10MW_local/res/blade_stability_hs2_Blade_struc.cmb"
        )
        # Mode shapes.
        blade_struct_mode_shapes_file_name = (
            "./../numerical_data/IEA10MW_local/res/blade_stability_hs2_Blade_struc.amp"
        )

        # Read results.
        cmb_blade_struct = read_hs2_output_cmb(blade_struct_freqdamp_file_name)
        amp_blade_struct = read_hs2_output_amp_blade(blade_struct_mode_shapes_file_name)

        # Name the blade modes.
        blade_struct_mode_names = name_blade_modes(
            amp_blade_struct, op=0.0, cmb=cmb_blade_struct
        )

        # Plot the modes.
        fig_list = plot_blade_modes(
            amp_blade_struct, op=0.0, mode_names=blade_struct_mode_names
        )

        # Save.
        fig_folder = "./../figures/IEA10MW/blade_structural_mode_shapes"
        os.makedirs(fig_folder, exist_ok=True)
        i = 0
        for fig in fig_list:
            i += 1
            fig.savefig(f"{fig_folder}/Mode_{i}.svg", bbox_inches="tight")

    # %% Test blade aeroelastic modes.

    if False:
        # File name of the blade aeroelastic modes.
        # Frequencies and damping ratios.
        blade_freqdamp_file_name = (
            "./../numerical_data/IEA10MW_local/res/blade_stability_hs2_Blade.cmb"
        )
        # Mode shapes.
        blade_mode_shapes_file_name = (
            "./../numerical_data/IEA10MW_local/res/blade_stability_hs2_Blade.amp"
        )

        # Read results.
        cmb_blade = read_hs2_output_cmb(blade_freqdamp_file_name)
        amp_blade = read_hs2_output_amp_blade(blade_mode_shapes_file_name)

        # Name the blade modes.
        blade_mode_names = name_blade_modes(amp_blade, op=4.0, cmb=cmb_blade)

        # Plot the modes.
        fig_list = plot_blade_modes(amp_blade, op=4.0, mode_names=blade_mode_names)

        # Save.
        fig_folder = "./../figures/IEA10MW/blade_aeroelastic_mode_shapes"
        os.makedirs(fig_folder, exist_ok=True)
        i = 0
        for fig in fig_list:
            i += 1
            fig.savefig(f"{fig_folder}/Mode_{i}.svg", bbox_inches="tight")

    # %% Test standing still turbine modes.

    if False:
        raise NotImplementedError()

        # File name of the turbine structural mode shapes.
        turbine_struc_mode_shapes_file_name = "./../Models/DTU_10MW/res/DTU_10MW_test_4/HS2_master_x64/DTU_10MW_test_4_struc.amp"

        # File name of the turbine structural frequencies and damping ratios.
        turbine_struc_damp_file_name = "./../Models/DTU_10MW/res/DTU_10MW_test_4/HS2_master_x64/DTU_10MW_test_4_struc.cmb"

        # Read blade mode shapes.
        amp_turbine_struc = read_hs2_output_amp_turbine(
            turbine_struc_mode_shapes_file_name
        )

        # Read blade frequencies and damping ratios.
        cmb_turbine_struc = read_hs2_output_cmb(turbine_struc_damp_file_name)

        # Name the blade modes.
        turbine_struc_mode_names = name_turbine_modes(
            amp_turbine_struc, op=0.0, cmb=cmb_turbine_struc
        )

        # Plot the modes.
        plot_turbine_mode_shapes(
            amp_turbine_struc, op=0.0, mode_names=turbine_struc_mode_names
        )

    # %% Test operating turbine modes.

    if False:
        # File name of the turbine structural mode shapes.
        turbine_aero_mode_shapes_file_name = "./../Models/DTU_10MW/res/DTU_10MW_test_4/HS2_master_x64/DTU_10MW_test_4.amp"

        # File name of the turbine structural frequencies and damping ratios.
        turbine_aero_damp_file_name = "./../Models/DTU_10MW/res/DTU_10MW_test_4/HS2_master_x64/DTU_10MW_test_4.cmb"

        # Read blade mode shapes.
        amp_turbine_aero = read_hs2_output_amp_turbine(
            turbine_aero_mode_shapes_file_name
        )

        # Read blade frequencies and damping ratios.
        cmb_turbine_aero = read_hs2_output_cmb(turbine_aero_damp_file_name)

        # Name the blade modes.
        turbine_aero_mode_names = name_turbine_modes(
            amp_turbine_aero, op=7.0, cmb=cmb_turbine_aero
        )

        # Plot the modes.
        plot_turbine_mode_shapes(
            amp_turbine_aero, op=7.0, mode_names=turbine_aero_mode_names
        )
