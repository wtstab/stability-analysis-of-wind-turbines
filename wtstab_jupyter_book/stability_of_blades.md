(chap:stability_of_blades)=
# Stability of blades

To describe the modes of an isolated blade we will take as example the IEA 10 MW Reference Wind Turbine {cite}`PBortolotti_etal_2019_IEA`, whose properties are summarized in {numref}`tab:IEA10MW_basic_properties`.

```{table} Key parameters of the IEA 10 MW Reference Wind Turbine.
:name: tab:IEA10MW_basic_properties

Parameter               |  Value
------------------------|---------------------------------------
Wind regime             |  IEC class 1A
Rated electrical power  |  10 MW
Rotor orientation       |  Clockwise rotation, upwind
Control                 |  Variable speed, collective pitch
Cut-in wind speed       |  4 m/s
Cut-out wind speed      |  25 m/s
Rated wind speed        |  10 m/s
Hub height              |  119 m
Nominal rotor diameter  |  198 m
Drivetrain              |  Direct-drive
Minimum rotor speed     |  6.0 rpm
Maximum rotor speed     |  8.68 rpm
Shaft tilt angle        |  6.0 deg
Rotor cone angle        |  4 deg
Blade prebend           |  6.2 m
```

To perform its stability analysis we will use HAWCStab2 {cite}`MHHansen_2011`. HAWCStab2 models each turbine structural member using Timoshenko beam elements. Large deflections and rotations are modeled using a co-rotational formulation, while the aerodynamics is modeled using the Blade Element Momentum (BEM) method.

The structural frequencies and damping ratios of the IEA 10 MW blade are reported in {numref}`tab:IEA10MW_blade_struct_frequency_and_damping`, while the mode shapes are shown in {numref}`fig:IEA10MW_blade_mode_shape_1`--{numref}`fig:IEA10MW_blade_mode_shape_9`.

A few comments are:
- The blade modes are named after the mode shapes.

- The mode shapes are computed for all degrees of freedom (6 per node), but in the figures we show only the most relevant ones, namely: flapwise, edgewise and torsion.

- Because of the structural couplings (geometrical and material) the modes are not pure, but have non-zero components in all degrees of freedom.

- The number associated to each mode equals the number of zero crossing in the deflection shape plus 1.

- The order in {numref}`tab:IEA10MW_blade_struct_frequency_and_damping` is quite common, but some blades might show differences.

- With increasing order, the modes tend to have comparable magnitude in flapwise, edgewise and torsion, therefore the naming loses meaning.

- The structural damping ratios are generally low, and increase with the frequency according to the Rayleigh model.

```{table} Structural frequencies and damping ratios of the IEA 10 MW blade.
:name: tab:IEA10MW_blade_struct_frequency_and_damping

#   | Name                       | Damped frequency <br /> [Hz] | Damping ratio <br /> [%]
----|----------------------------|------------------------------| ------------------------
1   |  1<sup>st</sup> Flapwise   |  0.41                        |  0.325
2   |  1<sup>st</sup> Edgewise   |  0.68                        |  0.361
3   |  2<sup>nd</sup> Flapwise   |  1.08                        |  0.830
4   |  2<sup>nd</sup> Edgewise   |  1.80                        |  0.9265
5   |  3<sup>rd</sup> Flapwise   |  2.27                        |  1.733
6   |  3<sup>rd</sup> Edgewise   |  3.73                        |  2.306
7   |  4<sup>th</sup> Flapwise   |  3.94                        |  2.511
8   |  5<sup>th</sup> Flapwise   |  5.78                        |  4.365
9   |  1<sup>st</sup> Torsional  |  6.48                        |  2.568
```

:::{figure-md} fig:IEA10MW_blade_mode_shape_1
<img src="../figures/IEA10MW/blade_structural_mode_shapes/Mode_1.svg">

Blade structural mode 1 -- 1<sup>st</sup> Flapwise.
:::

:::{figure-md} fig:IEA10MW_blade_mode_shape_2
<img src="../figures/IEA10MW/blade_structural_mode_shapes/Mode_2.svg">

Blade structural mode 2 -- 1<sup>st</sup> Edgewise.
:::

:::{figure-md} fig:IEA10MW_blade_mode_shape_3
<img src="../figures/IEA10MW/blade_structural_mode_shapes/Mode_3.svg">

Blade structural mode 3 -- 2<sup>nd</sup> Flapwise.
:::

:::{figure-md} fig:IEA10MW_blade_mode_shape_4
<img src="../figures/IEA10MW/blade_structural_mode_shapes/Mode_4.svg">

Blade structural mode 4 -- 2<sup>nd</sup> Edgewise.
:::

:::{figure-md} fig:IEA10MW_blade_mode_shape_5
<img src="../figures/IEA10MW/blade_structural_mode_shapes/Mode_5.svg">

Blade structural mode 5 -- 3<sup>rd</sup> Flapwise.
:::

:::{figure-md} fig:IEA10MW_blade_mode_shape_6
<img src="../figures/IEA10MW/blade_structural_mode_shapes/Mode_6.svg">

Blade structural mode 6 -- 3<sup>rd</sup> Edgewise.
:::

:::{figure-md} fig:IEA10MW_blade_mode_shape_9
<img src="../figures/IEA10MW/blade_structural_mode_shapes/Mode_9.svg">

Blade structural mode 9 -- 1<sup>st</sup> Torsional.
:::

By coupling an aerodynamic model to the structural one, it is possible to compute the aeroelastic blade modes, whose frequencies and damping ratios are reported in {numref}`tab:IEA10MW_blade_aero_frequency_and_damping`. The frequencies are not particularly affected by the aerodynamics, but the change in the damping ratios is instead significant. When the blade moves in the flapwise direction, it causes a variation in the angle of attack, and therefore the aeroelastic damping of flapwise modes tends to be high. On the other hand, when the blade moves in the edgewise direction, it causes a variation in the relative wind speed, and therefore the aeroelastic damping of edgewise modes tends to be low. Further details on the aerodynamics of airfoils can be found in {cite}`MHHansen_2007`.

```{table} Aeroelastic frequencies and damping ratios of the IEA 10 MW blade at 4 m/s.
:name: tab:IEA10MW_blade_aero_frequency_and_damping

#   | Name                       | Damped frequency <br /> [Hz] | Damping ratio <br /> [%]
----|----------------------------|------------------------------| ------------------------
1   |  1<sup>st</sup> Flapwise   | 0.45                         |   56.846
2   |  1<sup>st</sup> Edgewise   | 0.69                         |   1.359
3   |  2<sup>nd</sup> Flapwise   | 1.13                         |   20.155
4   |  2<sup>nd</sup> Edgewise   | 1.86                         |   1.168
5   |  3<sup>rd</sup> Flapwise   | 2.34                         |   9.161
6   |  3<sup>rd</sup> Edgewise   | 3.88                         |   3.730
7   |  4<sup>th</sup> Flapwise   | 3.95                         |   5.111
8   |  5<sup>th</sup> Flapwise   | 5.85                         |   6.402
9   |  1<sup>st</sup> Torsional  | 6.31                         |   3.855
```