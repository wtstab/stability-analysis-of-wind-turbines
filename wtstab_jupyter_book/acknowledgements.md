# Acknowledgements

The presentation that lead to this book has been developed for a lecture on wind turbines stability, within the Wind turbine measurement technique course offered by DTU Wind.

The presentation has later been converted into a report during a project between DTU Wind and Xinjiang Goldwind Science & Technology Co Ltd, that is gratefully acknowledged.

Many thanks to [Rasmus Sode Lund](https://www.linkedin.com/in/rasmus-sode-lund/) for setting up the CI/CD.