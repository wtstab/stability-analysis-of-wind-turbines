(chap:isotropic_wind_turbines)=
# Isotropic wind turbines

Isotropic wind turbines are a particular case of in-operation wind turbines, which provide a strong insight into their stability analysis.

Let us introduce two definitions from {cite}`PFSkjoldan_PhD_thesis`:

````{prf:definition} Isotropic external conditions
:label: def:isotropic_external_conditions
:nonumber:

Uniform wind field, constant in time and aligned in tilt and yaw to be perpendicular to the rotor plane, and no gravity is present.
````

````{prf:definition} Isotropic rotor
:label: def:isotropic_rotor
:nonumber:

Polarly symmetric and balanced rotor.
````

A rotor is isotropic if its blades are symmetrical mounted, and have the same mass distribution and aerodynamic properties. The pitch angle can be fixed to the collective value, or have a phase shift of $2 \pi / B$ rad, like in the Individual Pitch Controller (IPC) {cite}`Burton_WindEnergyHandbook_3ed`.

Isotropic rotors operating in isotropic external conditions are characterized by stationary steady states (i.e. constant deflection of turbine members), when measured in local coordinates. If $B \ge 3$, this property allows the Coleman transformation to convert the system into a LTI one.


## Example of isotropic conditions

To better understand how an isotropic turbine looks like, we have set up an isotropic simulation with HAWC2. First, we have taken the HAWC2 model of the IEA 10 MW turbine {cite}`PBortolotti_etal_2019_IEA`, which can be downloaded from the [GitHub repo](https://github.com/IEAWindTask37/IEA-10.0-198-RWT/tree/master/hawc2). Then, we have:
- Set the gravity to 0 for all main bodies.
- Set the upflow angle to -6 deg, so that the wind is aligned with the shaft.
- Set the tower to stiff. This is not necessary for isotropic conditions, but it avoids that the tower deflection changes the relative upflow angle.
- Removed the tower shadow and tower drag.

The wind speed was set to the rated value of 10 m/s with the addition of a ramp function. The blades tip deflection in flap is shown in {numref}`fig:asymptotic_stability`. As we can see, when the deflections are measured in blade coordinates, then all blades provide the same response, which is not a function of the azimuth angle. Since the rotor is rotating, and the shaft has a non-zero tilt angle, we observe a periodic motion in the inertial (i.e. global) frame. In the inertial frame the blades appear to move at the 1P, with a phase difference of 120 deg.

:::{figure-md} fig:asymptotic_stability
<img src="../figures/IEA10MW/isotropic_blade_tip_flap.svg">

Flapwise tip deflection for the isotropic IEA 10 MW.
:::


(sec:Stability_analysis_of_isotropic_rotors)=
## Stability analysis of isotropic rotors

The free response of an isotropic rotor, operating in isotropic external conditions, is governed by the LTP system {eq}`eq:ltv_autonomous_sys`, where $\mat{A}(t) = \mat{A}(t + T)$, and $\vec{x}$ contains the states in the rotating reference frame. By applying the Coleman transformation we transform the blade degrees of freedom in Multi-Blade Coordinates. If the number of blades is greater or equal than 3, the transformed system is
\begin{equation}
    \dot{\vec{x}}_{\mathit{MBC}}(t) = \bar{\mat{A}} \vec{x}_{\mathit{MBC}}(t),
    %
    \quad
    %
    \vec{x}_{\mathit{MBC}}(0) = \vec{x}_{\mathit{MBC}_0},
\end{equation}
with
```{math}
:label: eq:periodic_to_invariant_a_mat_with_coleman
    \bar{\mat{A}} = \mat{\mathcal{M}}(t) \left(\mat{A}(t) \mat{\mathcal{M}}^{-1}(t) - \frac{\diff \mat{\mathcal{M}}^{-1}(t)}{\diff t}\right)
```
a constant matrix. The modal decomposition of this system is provided by Eq. {eq}`eq:lti_modal_decomposition_of_state`, here reported for convenience with $\tau=0$
\begin{equation}
    \vec{x}_{\mathit{MBC}}(t)
        = \sum_{j=1}^{N_x} \vec{\psi}_{\mathit{MBC}_j} r_j e^{\lambda_{\mathit{MBC}_j} t}.
\end{equation}
The blades motion is recovered by using the inverse Coleman transformation
```{math}
:label: eq:from_mbc_to_blade_modal_decomposition
    \vec{x}(t)
        = \mat{\mathcal{M}}^{-1}(t) \sum_{j=1}^{N_x} \vec{\psi}_{\mathit{MBC}_j} r_j e^{\lambda_{\mathit{MBC}_j} t}.
```
The inclusion of the Coleman transformation alters both the eigenvalues and mode shapes. To see how, we convert $\mat{\mathcal{M}}^{-1}(t)$ in complex form, using the Euler formula
\begin{align}
    \cos{n\psi} &= \frac{e^{-\imath n \psi} + e^{+\imath n \psi}}{2}
    %
    \\
    %
    \sin{n\psi} &= \imath \frac{e^{-\imath n \psi} - e^{+\imath n \psi}}{2}
\end{align}
we get
% Complete matrix (very long)
%\begin{equation}
%    \mat{\mathcal{M}}_R^{-1} = \begin{bmatrix}
%        1        &   \frac{1}{2}(e^{-\imath \psi_1}+e^{+\imath \psi_1})   &   \frac{1}{2} \imath (e^{-\imath \psi_1} - e^{+\imath \psi_1})   &   \cdots   &   \frac{1}{2}(e^{-\imath \tilde{B} \psi_1}+e^{+\imath \tilde{B} \psi_1})   &   \frac{1}{2} \imath (e^{-\imath \tilde{B} \psi_1} - e^{+\imath \tilde{B} \psi_1})   &            -  1   \\
%        1        &   \frac{1}{2}(e^{-\imath \psi_2}+e^{+\imath \psi_2})   &   \frac{1}{2} \imath (e^{-\imath \psi_2} - e^{+\imath \psi_2})   &   \cdots   &   \frac{1}{2}(e^{-\imath \tilde{B} \psi_2}+e^{+\imath \tilde{B} \psi_2})   &   \frac{1}{2} \imath (e^{-\imath \tilde{B} \psi_2} - e^{+\imath \tilde{B} \psi_2})   &   \phantom{-} 1   \\
%        1        &   \frac{1}{2}(e^{-\imath \psi_3}+e^{+\imath \psi_3})   &   \frac{1}{2} \imath (e^{-\imath \psi_3} - e^{+\imath \psi_3})   &   \cdots   &   \frac{1}{2}(e^{-\imath \tilde{B} \psi_3}+e^{+\imath \tilde{B} \psi_3})   &   \frac{1}{2} \imath (e^{-\imath \tilde{B} \psi_3} - e^{+\imath \tilde{B} \psi_3})   &            -  1   \\
%        \vdots   &   \vdots                                               &   \vdots                                                         &   \cdots   &   \vdots                   &   \vdots                   &   \vdots          \\
%        1        &   \frac{1}{2}(e^{-\imath \psi_B}+e^{+\imath \psi_B})   &   \frac{1}{2} \imath (e^{-\imath \psi_B} - e^{+\imath \psi_B})   &   \cdots   &   \frac{1}{2}(e^{-\imath \tilde{B} \psi_B}+e^{+\imath \tilde{B} \psi_B})   &   \frac{1}{2} \imath (e^{-\imath \tilde{B} \psi_B} - e^{+\imath \tilde{B} \psi_B})   &   (-1)^B          \\
%    \end{bmatrix} \otimes \mat{I}_{N_b}.
%\end{equation}
% Splitted matrix
\begin{multline}
    \mat{\mathcal{M}}_R^{-1} = \left[\begin{matrix}
        1        &   \frac{1}{2}(e^{-\imath \psi_1}+e^{+\imath \psi_1})   &   \frac{1}{2} \imath (e^{-\imath \psi_1} - e^{+\imath \psi_1})   &   \cdots   \\[3pt]
        1        &   \frac{1}{2}(e^{-\imath \psi_2}+e^{+\imath \psi_2})   &   \frac{1}{2} \imath (e^{-\imath \psi_2} - e^{+\imath \psi_2})   &   \cdots   \\[3pt]
        1        &   \frac{1}{2}(e^{-\imath \psi_3}+e^{+\imath \psi_3})   &   \frac{1}{2} \imath (e^{-\imath \psi_3} - e^{+\imath \psi_3})   &   \cdots   \\
        \vdots   &   \vdots                                               &   \vdots                                                         &   \cdots   \\
        1        &   \frac{1}{2}(e^{-\imath \psi_B}+e^{+\imath \psi_B})   &   \frac{1}{2} \imath (e^{-\imath \psi_B} - e^{+\imath \psi_B})   &   \cdots   \\[2pt]
    \end{matrix}\right.
	\\
	\left.\begin{matrix}
        \frac{1}{2}(e^{-\imath \tilde{B} \psi_1}+e^{+\imath \tilde{B} \psi_1})   &   \frac{1}{2} \imath (e^{-\imath \tilde{B} \psi_1} - e^{+\imath \tilde{B} \psi_1})   &            -  1   \\[2pt]
        \frac{1}{2}(e^{-\imath \tilde{B} \psi_2}+e^{+\imath \tilde{B} \psi_2})   &   \frac{1}{2} \imath (e^{-\imath \tilde{B} \psi_2} - e^{+\imath \tilde{B} \psi_2})   &   \phantom{-} 1   \\[2pt]
        \frac{1}{2}(e^{-\imath \tilde{B} \psi_3}+e^{+\imath \tilde{B} \psi_3})   &   \frac{1}{2} \imath (e^{-\imath \tilde{B} \psi_3} - e^{+\imath \tilde{B} \psi_3})   &            -  1   \\
        \vdots                                                                   &   \vdots                                                                             &   \vdots          \\
        \frac{1}{2}(e^{-\imath \tilde{B} \psi_B}+e^{+\imath \tilde{B} \psi_B})   &   \frac{1}{2} \imath (e^{-\imath \tilde{B} \psi_B} - e^{+\imath \tilde{B} \psi_B})   &   (-1)^B          \\[2pt]
    \end{matrix}\right] \otimes \mat{I}_{N_b}.
\end{multline}
For a mode $j$, the eigenvalue is $\lambda_{\mathit{MBC}_j}$, and the mode shape in MBC is
\begin{equation}
    \vec{\psi}_{\mathit{MBC}_j}
        = (\vec{\psi}_a^\transpose,
           \vec{\psi}_{1c}^\transpose,
           \vec{\psi}_{1s}^\transpose,
           \vec{\psi}_{2c}^\transpose,
           \vec{\psi}_{2s}^\transpose,
           \dots,
           \vec{\psi}_{\tilde{B}c}^\transpose,
           \vec{\psi}_{\tilde{B}s}^\transpose,
           \vec{\psi}_{B/2}^\transpose,
           \vec{\psi}_T^\transpose)_j^\transpose,
\end{equation}
where for ease of notation the subscript $j$ has been written only at the end. A direct evaluation of Eq. {eq}`eq:from_mbc_to_blade_modal_decomposition` shows that the modal decomposition of the tower motion is
\begin{equation}
    \vec{x}_{T}
        = \sum_{j=1}^{N_x} \vec{\psi}_{T_j} r_j e^{\lambda_{\mathit{MBC}_j} t},
\end{equation}
which as expected is the same in both reference frames. However, for blade $b$ we find
\begin{equation}
\begin{split}
    \vec{x}_{b}
        &= \sum_{j=1}^{N_x} r_j \Bigl(
            (\vec{\psi}_{a_j} + (-\mat{I}_{N_b})^b \vec{\psi}_{B/2_j}) e^{\lambda_{\mathit{MBC}_j} t}
        \\
        &\quad + \sum_{p=1}^{\tilde{B}} \frac{1}{2} (\vec{\psi}_{pc_j} - \imath \vec{\psi}_{ps_j}) e^{+ \imath p (b-1) \frac{2 \pi}{B}} e^{(\lambda_{\mathit{MBC}_j} + \imath p \Omega) t}
        \\
        &\quad + \sum_{p=1}^{\tilde{B}} \frac{1}{2} (\vec{\psi}_{pc_j} + \imath \vec{\psi}_{ps_j}) e^{- \imath p (b-1) \frac{2 \pi}{B}} e^{(\lambda_{\mathit{MBC}_j} - \imath p \Omega) t}
        \Bigr).
\end{split}
\end{equation}
This expression can be simplified by defining
\begin{align}
    \vec{\psi}_{{\mathit{BW}, b, p}_j} &= \frac{1}{2} (\vec{\psi}_{pc_j} - \imath \vec{\psi}_{ps_j}) e^{+ \imath p (b-1) \frac{2 \pi}{B}},
    %
    \\
    %
    \vec{\psi}_{{\mathit{FW}, b, p}_j} &= \frac{1}{2} (\vec{\psi}_{pc_j} + \imath \vec{\psi}_{ps_j}) e^{- \imath p (b-1) \frac{2 \pi}{B}}.
\end{align}
These definitions differ by the ones derived in Ref. {cite}`MHHansen_2016`, because in that article Hansen resorted to a complex version of the Coleman transformation, which allowed to obtain a perfect parallel between MBC and whirling components. The modal decomposition of the blades motion becomes
\begin{equation}
\begin{split}
    \vec{x}_{b}
        &= \sum_{j=1}^{N_x} r_j \Bigl(
            (\vec{\psi}_{a_j} + (-\mat{I}_{N_b})^b \vec{\psi}_{B/2_j}) e^{\lambda_{\mathit{MBC}_j} t}
        \\
        &\quad + \sum_{p=1}^{\tilde{B}} \vec{\psi}_{{\mathit{BW}, b, p}_j} e^{(\lambda_{\mathit{MBC}_j} + \imath p \Omega) t}
        \\
        &\quad + \sum_{p=1}^{\tilde{B}} \vec{\psi}_{{\mathit{FW}, b, p}_j} e^{(\lambda_{\mathit{MBC}_j} - \imath p \Omega) t}
        \Bigr).
\end{split}
\end{equation}

Since three-bladed turbines are by far the most common type, it is worth specializing the modal decomposition of the blade motion for this case. Setting $B = 3$ we get $\tilde{B} = 1$ and hence
\begin{equation}
    \vec{x}_{b}
        = \sum_{j=1}^{N_x} r_j \left(
            \vec{\psi}_{\mathit{BW}, b_j}  e^{(\lambda_{\mathit{MBC}_j} + \imath \Omega) t}
        +   \vec{\psi}_{a_j} e^{\lambda_{\mathit{MBC}_j} t}    
        +   \vec{\psi}_{\mathit{FW}, b_j}  e^{(\lambda_{\mathit{MBC}_j} - \imath \Omega) t}
        \right).
\end{equation}
This expression is equivalent to the one obtained in {cite}`MHHansen_2007` and {cite}`PFSkjoldan_MHHansen_2009` in trigonometric form.

This expression allows to draw several conclusions:

- An isotropic rotor has $N_x$ modes, each composed by $2 \tilde{B} + 1$ harmonics.
- Depending on which components of the mode shapes prevail, the modes will be named according to {numref}`tab:mode_shapes_names_isotropic`.
- The harmonics range from $- \tilde{B} \Omega$ to $+ \tilde{B} \Omega$, with the middle one called principal.
- The whirl direction is determined by the sign of the phase shift in $p (b-1) \frac{2 \pi}{B}$.
- The mode shapes of the blades have a phase difference of $2 \pi / B$ rad.
- In the ground-fixed frame, only the principal harmonic is observed, while in the rotating one all harmonics participate to the response.
- For a three-bladed turbine, some harmonics are more relevant than others, as listed in table~\ref{tab:most_important_harmonics}.
- Whirling modes exist only for $B > 3$ and antisymmetric modes only for $B$ even.
- Standing still asymmetric modes become whirling modes.
- The principal damped frequency of the backward whirling modes decrease as $- p \Omega$, while the principal damped frequency of the forward whirling modes increase as $+ p \Omega$.
- In the rotating frame, the whirling modes are observed with a frequency close to the one of the isolated blade.
- In general, the frequency of the collective mode does not equal the one of the whirling modes in the rotating frame. Its value depends on the flexibility of the support structure (tower and shaft).

```{table} Mode shapes names for an isotropic rotor.
:name: tab:mode_shapes_names_isotropic

| Most important component               | Mode name                                                                                                                    | 
|----------------------------------------|------------------------------------------------------------------------------------------------------------------------------|
| $\vec{\psi}_{a_j}$                     |   Symmetric (also known as collective).                                                                                      |
| $\vec{\psi}_{B/2_j}$                   |   Antisymmetric.                                                                                                             |
| $\vec{\psi}_{{\mathit{BW}, b, p}_j}$   |   Flapwise / Edgewise / Torsional $k$ Backward Whirl -- $p$, with $k-1$ the number of zero crossing of the blade mode shape. |
| $\vec{\psi}_{{\mathit{FW}, b, p}_j}$   |   Flapwise / Edgewise / Torsional $k$ Forward Whirl -- $p$, with $k-1$ the number of zero crossing of the blade mode shape.  |
```

```{table} Most relevant harmonics for a three bladed wind turbine observed in different frames.
:name: tab:most_important_harmonics

| Mode             | Ground-fixed frame | Rotating frame          |
|------------------|--------------------|-------------------------|
| Tower            |   principal        |   $-\Omega$, $+\Omega$  |
| Symmetric        |   principal        |   principal             |
| Backward Whirl   |   principal        |   $+\Omega$             |
| Forward Whirl    |   principal        |   $-\Omega$             |
```

We can now revisit the example provided in the introduction in light of the theory of isotropic rotors. Although that turbine was operating in anisotropic conditions, the theory of isotropic rotors allows to qualitatively describe its modes. The interpretation of the peaks is shown in {numref}`fig:IEA10MW_hawc2_wind_and_doublet_free_with_labels`. As we can see, the tower side-side mode is observed with its principal harmonic in the ground-fixed frame. This mode would be observed with the $\pm \Omega$ harmonics on the blades, but for this turbine, and experiment, the peaks are too small to be seen. The edgewise whirling modes are observed with their principal harmonic in the ground-fixed frame, and with only one harmonic in the middle of them in the rotating frame. The collective edgewise is not in the middle of the whirling modes, but at a higher frequency.

:::{figure-md} fig:IEA10MW_hawc2_wind_and_doublet_free_with_labels
<img src="../figures/IEA10MW/hawc2_wind_and_doublet_free_with_labels.svg" width=100%>

PSD of 3 loads for the IEA 10 MW wind turbine, subject to a Normal Wind Profile.
:::
