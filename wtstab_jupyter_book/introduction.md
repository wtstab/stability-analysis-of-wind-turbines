# Introduction

Wind turbines are subject to vibrations due to complex aero-servo-hydro-elastic phenomena, which can be explained through the stability analysis. The stability analysis allows to understand resonances and instabilities, to validate numerical models and simulation codes. Old turbine designs were rather stiff, and therefore their stability was not particularly concerning. However, modern turbines have very long blades, and in order for them to be cost-effective they are also very flexible. This implies that the turbine frequencies are closer to the energetic part of the wind and wave spectrum. To avoid resonances, a classical solution is to introduce a constraint between some turbine frequencies and the multiples of the rotor speed ($n\mathrm{P}$).

This book will introduce the reader to the modes of:
-   isolated blades;
-   standing still turbines;
-   operating turbines.

As an example, let us consider the following virtual experiment. A 10 MW wind turbine has been modeled in a multi-body aero-hydro-servo-elastic simulator HAWC2. The wind is steady and follows a normal profile. We have applied a force at the tower top, in the fore-aft direction, with the trend of a doublet. At the same time, we have applied also a torque doublet to the shaft. {numref}`fig:IEA10MW_hawc2_wind_and_doublet_free_without_labels` shows the Power Spectral Density (PSD) of 3 loads, computed after the doublets have finished.

:::{figure-md} fig:IEA10MW_hawc2_wind_and_doublet_free_without_labels
<img src="../figures/IEA10MW/hawc2_wind_and_doublet_free_without_labels.svg" width=100%>

PSD of  of 3 loads for a 10 MW wind turbine, subject to a Normal Wind Profile.
:::

At the end of this book, the reader should be able to explain the various peaks present in this figure. In particular, he/she should be able to separate the peaks due to the free response, from the ones due to the forced response, and to explain the difference between measuring on the blades and on the tower.


## Learning objectives

- Understand the concepts of frequency, damping ratio and mode shape.
- Identify the blade modes.
- Identify the standing still turbine modes.
- Identify the operating turbine modes.
- Sketch the Campbell diagram of a three-bladed turbine, and explain the difference between measuring on the rotor and on the tower.


## About the author

My name is Riccardo Riva. I have obtained a Ph.D. in aerospace engineering from Politecnico di Milano in 2018, and currently I am a special consultant at DTU Wind and Energy Systems. I have worked on several projects related to wind turbine stability. Please visit my [LinkedIn](https://www.linkedin.com/in/riccardo-riva-792651a7/) and [Research Gate](https://www.researchgate.net/profile/Riccardo-Riva-4) profiles for more details.


## Q&A

### Why this book exists?

The stability analysis of wind turbines is an important topic, that is barely addressed in classical books like {cite}`JFManwell_etal_2009` and {cite}`Burton_WindEnergyHandbook_3ed`. The knowledge on this field is spread over a few reports and several research papers, that are often behind a paywall. Since the goal of any paper should be to answer one specific question, it follows that even by reading them it is quite difficult to grasp the full picture. This book should introduce the reader to wind turbines stability, and cover all the important concepts in this field.

### Why writing a website, rather than a physical book or a research paper?

I want this book to be free to access, easy to find and simple to navigate. Its content should span from fundamental to advanced, and therefore not be limited in space. It should be possible to update it quickly as new knowledge is developed or errors are spotted. Finally, it should leverage modern web technologies, and not face the limitations of paper and pdf documents.

### Is this book finished?

Of course it isn't 🙂 There are lots of things that I haven't written about, and many more that I still have to learn. Slowly slowly I plan to expand this book by writing about aeroelasticity, interactions with the hydrodynamics and control system, and Operational Modal Analysis.

### Is this book peer reviewed?

No, it's not. Although the peer review is an important process to ensure the quality of research articles, it also has several drawbacks. Instead, I preferred to adopt an open source approach. Nearly all text, data, source codes and models used in this book are visible in the repo. Everybody is welcome to check it and contact me for explanations.

### How can I contribute to this book?

Do you have ideas about how to improve this book, or you have found an error? If you already have an account on Gitlab, please open an issue and assign it to me. Otherwise, please write me an email.


### How to cite this book.

Please cite: Riccardo Riva and Rasmus Sode Lund. (2022). Stability analysis of wind turbines (v1.0). Zenodo. [https://doi.org/10.5281/zenodo.7514961](https://doi.org/10.5281/zenodo.7514961)
