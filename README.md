# Stability analysis of wind turbines

[![pipeline status](https://gitlab.windenergy.dtu.dk/wtstab/stability-analysis-of-wind-turbines/badges/master/pipeline.svg)](https://gitlab.windenergy.dtu.dk/wtstab/stability-analysis-of-wind-turbines/-/commits/master)
[![Jupyter Book Badge](https://jupyterbook.org/badge.svg)](https://wtstab.pages.windenergy.dtu.dk/stability-analysis-of-wind-turbines)
[![Poetry](https://img.shields.io/endpoint?url=https://python-poetry.org/badge/v0.json)](https://python-poetry.org/)
[![Code style: black](https://img.shields.io/badge/code%20style-black-000000.svg)](https://github.com/psf/black)
[![DOI](https://zenodo.org/badge/DOI/10.5281/zenodo.7514961.svg)](https://doi.org/10.5281/zenodo.7514961)

Book on the stability analysis of wind turbines.

The book is available at [https://wtstab.pages.windenergy.dtu.dk/stability-analysis-of-wind-turbines](https://wtstab.pages.windenergy.dtu.dk/stability-analysis-of-wind-turbines)


## Folders structure

- `wtstab_jupyter_book`: jupyter book.
- `chapters`: old LaTeX report from which the jupyter book started. It is not updated anymore.
- `source_codes`: all the source codes used to generate pictures and animations.
- `figures`: figures that cannot be generated during the CI script.
- `numerical_data`: HAWC2 and HAWCStab2 turbine models, and results.
