# %%
"""
Harmonic oscillator.
"""

# %%

import matplotlib as mpl
import matplotlib.pyplot as plt
import numpy as np
import pandas as pd
from scipy import signal
from scipy.integrate import solve_ivp
from scipy.integrate._ivp.ivp import OdeResult

mpl.use("Agg")
# mpl.use('Qt5Agg')

plt.style.use("rcparams.mplstyle")

plt.close("all")


# %%

omega_0_list = np.array([0.1, 0.5, 1]) * 2.0 * np.pi
xi_list = np.array([0.0, 0.01, 0.05, 1.0])

x0 = np.array([1.0, 0.0])

t_span = (0.0, 1000.0)
dt = 0.01


# %% Integrate.


def a_mat_fun(omega_0, xi):
    # fmt: off
    a_mat = np.array([[0,               1                 ],
                     [-(omega_0**2.0), -2.0 * xi * omega_0]])
    # fmt: on
    return a_mat


def motion(t, x, a_mat):
    Dx = a_mat @ x
    return Dx


t_eval = np.linspace(t_span[0], t_span[-1], round((t_span[-1] - t_span[0]) / dt) + 1)

sol_all = np.empty((len(omega_0_list), len(xi_list)), dtype=OdeResult)

for i_omega0 in range(len(omega_0_list)):
    for i_xi in range(len(xi_list)):
        omega_0 = omega_0_list[i_omega0]
        xi = xi_list[i_xi]
        a_mat = a_mat_fun(omega_0, xi)

        sol_all[i_omega0, i_xi] = solve_ivp(
            fun=lambda t, x: motion(t, x, a_mat),
            t_span=t_span,
            y0=x0,
            t_eval=t_eval,
            rtol=1e-8,
            vectorized=True,
        )


# %% PSD.

dt = np.mean(np.diff(t_eval))
sampling_frequency = 1 / dt
nyquist_frequency = sampling_frequency / 2.0
df = 1 / (t_eval[-1] - t_eval[0])
window = np.ones(len(t_eval) - 1)
noverlap = 0
nperseg = len(window)
nfft = nperseg
detrend = "constant"
return_onesided = True
scaling = "density"

psd_all = np.empty((len(omega_0_list), len(xi_list)), dtype=pd.DataFrame)

for i_omega0 in range(len(omega_0_list)):
    for i_xi in range(len(xi_list)):
        y_freq, y_PSD = signal.welch(
            sol_all[i_omega0, i_xi].y[0, :-1].T,
            sampling_frequency,
            window,
            nperseg,
            noverlap,
            nfft,
            detrend,
            return_onesided,
            scaling,
            axis=0,
        )

        psd_all[i_omega0, i_xi] = pd.Series(
            data=y_PSD, index=pd.Index(data=y_freq, name="Frequency [Hz]")
        )


# %% Plot time series.

fig, ax = plt.subplots(
    num="effect_of_omega0_and_xi_time",
    nrows=len(omega_0_list),
    ncols=len(xi_list),
    sharex=True,
    sharey=True,
    figsize=(25.87 / 2.54, 12.63 / 2.54),
)

for i_xi in range(len(xi_list)):
    ax[0, i_xi].set_title(f"ξ = {xi_list[i_xi]}")
    ax[-1, i_xi].set_xlabel("Time [s]")

for i_omega0 in range(len(omega_0_list)):
    # ax[i_omega0,0].set_ylabel('Displacement [m]')
    ax[i_omega0, 0].set_ylabel(f"ω₀ = {omega_0_list[i_omega0]:.2}")
    for i_xi in range(len(xi_list)):
        ax[i_omega0, i_xi].set_xlim(0.0, 10.0)
        ax[i_omega0, i_xi].plot(
            sol_all[i_omega0, i_xi].t, sol_all[i_omega0, i_xi].y[0, :]
        )

plt.tight_layout()

fig.savefig(
    "./../figures/harmonic_oscillator/effect_of_omega0_and_xi_time.svg",
    bbox_inches="tight",
)


# %% Plot PSD.

fig, ax = plt.subplots(
    num="effect_of_omega0_and_xi_PSD",
    nrows=len(omega_0_list),
    ncols=len(xi_list),
    sharex=True,
    sharey=True,
    figsize=(25.87 / 2.54, 12.63 / 2.54),
)

for i_xi in range(len(xi_list)):
    ax[0, i_xi].set_title(f"ξ = {xi_list[i_xi]}")
    ax[-1, i_xi].set_xlabel("Frequency [Hz]")

for i_omega0 in range(len(omega_0_list)):
    ax[i_omega0, 0].set_ylabel(f"ω₀ = {omega_0_list[i_omega0]:.2}")
    for i_xi in range(len(xi_list)):
        ax[i_omega0, i_xi].set_xlim(0.0, 2.0)
        ax[i_omega0, i_xi].set_ylim(1e-6, None)
        ax[i_omega0, i_xi].set_yscale("log")
        ax[i_omega0, i_xi].plot(
            psd_all[i_omega0, i_xi].index, psd_all[i_omega0, i_xi].values
        )

plt.tight_layout()

fig.savefig(
    "./../figures/harmonic_oscillator/effect_of_omega0_and_xi_PSD.svg",
    bbox_inches="tight",
)
