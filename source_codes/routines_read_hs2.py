# -*- coding: utf-8 -*-
"""
Functions for reading HAWCStab2 output files.

@author: ricriv
"""

# %% Initialization.

import re

import numpy as np
import numpy.testing
import pandas as pd

# %% Functions.


def read_hs2_output_generic(file):
    """
    Read matrix-like files produced by HAWCStab2.
    It has been tested against the following file types:
    .pwr, .pcs, _aero.geo, _struct.geo, _uXX.ind, _defl_uXX.ind, _fext_uXX.ind,
    with normal, full precision and gradients.

    Parameters
    ----------
    file : str or pathlib.Path
        The file to be read.

    Returns
    -------
    pandas DataFrame
        Table with the file content.
    """
    # Open the file and read the first two lines.
    with open(file, "r") as fid:
        header = []
        while True:
            line = fid.readline()
            if line.lstrip()[0] == "#":
                header.append(line)
            else:
                line_num = line
                break

    # Parse the header.
    columns = []
    # This regular expression matches a numeric field in the first line of data.
    iter = re.finditer("( *[^ ]+)(?= *)", line_num)
    for m in iter:
        start = m.start(0)
        end = m.end(0)
        columns.append([h[start:end].strip() for h in header])
    # Remove comment character.
    for i in range(len(columns[0])):
        if columns[0][i][0] == "#":
            columns[0][i] = columns[0][i][1:].strip()
    # Concatenate the rows of the header.
    for i in range(len(columns)):
        columns[i] = (" ".join(columns[i])).strip()

    # Read the data and store them in a pandas DataFrame
    df = pd.DataFrame(data=np.loadtxt(file), columns=columns)

    return df


def read_hs2_output_opt(file):
    """
    Read .opt files produced by HAWCStab2.

    Parameters
    ----------
    file : str or pathlib.Path
        The file to be read.

    Returns
    -------
    pandas DataFrame
        Table with the file content.
    """
    data = np.loadtxt(file, skiprows=1)

    index = pd.Index(data=data[:, 0], name="wind speed [m/s]")

    columns = ("pitch [deg]", "rot. speed [rpm]", "aero power [kW]", "aero thrust [kN]")

    df = pd.DataFrame(
        data=data[:, 1:], index=index, columns=columns[: data.shape[1] - 1]
    )

    return df


def read_hs2_output_cmb(file):
    """
    Read .cmb files produced by HAWCStab2.
    The single dataframes can be obtained with
      frequencies = cmb.loc[:,'Frequency [Hz]']
      dampings    = cmb.loc[:,'Damping [%]']
      real_parts  = cmb.loc[:,'Real part']

    Parameters
    ----------
    file : str or pathlib.Path
        The file to be read.

    Returns
    -------
    cmb: pandas DataFrame
        index: rotor or wind speed
        columns: multi-index. First level: Frequency [Hz], Damping [%]
                 and optionally Real part. Second level: mode number
    """
    # Read number of modes.
    with open(file, "r") as fid:
        txt = fid.readline()
    n_mode = int(re.match(r".+ (\d+)\n$", txt).group(1))

    # Read all the results.
    # The atleast_2d is needed if the file contains results only one operating point.
    data_num = np.atleast_2d(np.loadtxt(file))
    n_col = data_num.shape[1] - 1

    # Rotor or wind speed.
    index = data_num[:, 0]

    if n_col <= 2 * n_mode + 1:
        type = ["Frequency [Hz]", "Damping [%]"]
    else:
        type = ["Frequency [Hz]", "Damping [%]", "Real part"]

    columns = pd.MultiIndex.from_product(
        [type, range(n_mode)], names=["Type", "Mode number"]
    )

    df = pd.DataFrame(data=data_num[:, 1:], index=index, columns=columns)

    return df


def read_h2_output_syseig(file):
    """
    Read frequency and damping files produced by HAWC2 command system_eigenanalysis.

    Parameters
    ----------
    file : str or pathlib.Path
        The file to be read.

    Returns
    -------
    h2eig: pandas DataFrame
        index: mode number
        columns: Damped frequency [Hz], Natural frequency [Hz],
                 Logarithmic decrement [%]
    """
    # Read the file.
    with open(file, "r") as fid:
        data_txt = fid.readlines()

    # Remove header lines.
    data_txt = data_txt[8:]

    # Number of modes.
    n_mode = len(data_txt)

    # Index of character from which parse the lines.
    i_start = data_txt[0].rfind(":") + 1  # = 12

    # Preallocate the array for the results.
    data_num = np.ndarray((n_mode, 3))

    # Parse the lines.
    for i_mode in range(n_mode):
        data_num[i_mode, :] = [float(x) for x in data_txt[i_mode][i_start:].split()]

    # Assemble the DataFrame.
    columns = ["fd [Hz]", "fn [Hz]", "log_decr [%]"]
    index = range(n_mode)
    df = pd.DataFrame(data=data_num, index=index, columns=columns)

    return df


def convert_h2eig_into_cmb(h2eig):
    """
    Convert the results of HAWC2 command system_eigenanalysis into the ones
    obtained by HAWCStab2 command compute_structural_modal_analysis nobladeonly.

    Parameters
    ----------
    h2eig: pandas DataFrame
        The result of function read_h2_output_syseig().
        index: mode number
        columns: Damped frequency [Hz], Natural frequency [Hz],
                 Logarithmic decrement [%]

    Returns
    -------
    cmb: pandas DataFrame
        A variable like the one returned from read_hs2_output_cmb()
        index: rotor or wind speed
        columns: multi-index. First level: Frequency [Hz], Damping [%]
                 and optionally Real part. Second level: mode number
    """
    # Number of modes.
    n_mode = len(h2eig)

    # HAWC2 performs the stability analysis with wind and rotor speed equal to 0.
    index = [0.0]  # The [] matters.

    # Create the columns.
    type = ["Frequency [Hz]", "Damping [%]"]
    columns = pd.MultiIndex.from_product(
        [type, range(n_mode)], names=["Type", "Mode number"]
    )

    # Comvert the logarithmic decrement to damping ratio.
    log_decr = h2eig["log_decr [%]"] / 100.0
    damping_ratio = 1.0 / np.sqrt(1.0 + (2.0 * np.pi / log_decr) ** 2.0)
    damping_ratio = damping_ratio * 100.0

    # Assemble the data array.
    data = [pd.concat([h2eig["fd [Hz]"], damping_ratio]).values]  # The [] matters.

    # Assemble the DataFrame.
    df = pd.DataFrame(data=data, index=index, columns=columns)

    return df


def read_hs2_output_amp_blade(file):
    """
    Read .amp files produced by HAWCStab2 for a blade only analysis.

    Parameters
    ----------
    file : str or pathlib.Path
        The file to be read.

    Returns
    -------
    df: pandas DataFrame
        Table with the mode shapes.
        Index: first level = wind/rotor speed, second level = radius.
        Columns: first level = mode number, second level = channel.
    """
    # Read the header.
    with open(file, "r") as fid:
        line_1 = fid.readline()  # Mode number
        fid.readline()  # Skip
        line_3 = fid.readline()  # header

    # Get the number of modes.
    n_mode = int(re.match(r".+ (\d+)\n$", line_1).group(1))

    # List of modes.
    i_modes = list(range(n_mode))

    # Read the whole content.
    data = np.loadtxt(file)

    # Get the radius.
    i_last_radius = np.where((np.diff(data[:, 1]) < 0))[0]
    if i_last_radius.size == 0:
        # If there is only 1 rotor/wind speed.
        i_last_radius = data.shape[0]
    else:
        # If there is more than 1 rotor/wind speed.
        i_last_radius = i_last_radius[0]
    #
    radius = data[0 : i_last_radius + 1, 1]

    # Get the wind or rotor speed.
    x = data[0 : data.shape[0] : i_last_radius + 1, 0]
    if line_3.startswith("# Rotor"):
        x_type = "Rotor speed [rad/s]"
    else:
        x_type = "Wind speed [m/s]"

    # Create the index.
    # First level:  wind/rotor speed
    # Second level: radius.
    index = pd.MultiIndex.from_product([x, radius], names=[x_type, "Radius [m]"])

    # List of channels for the standard Timoshenko beam element.
    # fmt: off
    channel_0 = [
        "u_x bld [m]", "phase_x [deg]",
        "u_y bld [m]", "phase_y [deg]",
        "theta [rad]", "phase_t [deg]",
    ]

    # List of channels for the FPM beam element.
    channel_fpm = [
        "u_x bld [m]", "phase_x [deg]",
        "u_y bld [m]", "phase_y [deg]",
        "u_z bld [m]", "phase_z [deg]",
        "theta_x [rad]", "phase_tx [deg]",
        "theta_y [rad]", "phase_ty [deg]",
        "theta_z [rad]", "phase_tz [deg]",
    ]
    # fmt: on
    if data.shape[1] - 2 == len(i_modes) * len(channel_0):
        channel = channel_0
    else:
        channel = channel_fpm

    # Create the columns.
    # First level:  mode number
    # Second level: channel.
    columns = pd.MultiIndex.from_product(
        [i_modes, channel], names=["Mode number", "Channel"]
    )

    # Assemble the DataFrame.
    df = pd.DataFrame(data=data[:, 2:], index=index, columns=columns)

    return df


def read_hs2_output_amp_turbine(file):
    """
    Read .amp files produced by HAWCStab2 for a complete turbine analysis.

    To get the mode shape for a specific mode write df.loc[:,(i_mode)]

    Parameters
    ----------
    file : str or pathlib.Path
        The file to be read.

    Returns
    -------
    df: pandas DataFrame
        Table with the mode shapes.
        Index: wind/rotor speed.
        Columns: first level = mode number, second level = channel.
    """
    # Get the number of modes.
    with open(file, "r") as fid:
        fid.readline()  # Skip line
        fid.readline()  # Skip line
        txt = fid.readline()  # Read Mode number

        n_mode = int(re.match(r".+ (\d+)\n$", txt).group(1))

        # List of modes.
        i_modes = list(range(n_mode))

        # Read the whole content.
        # The atleast_2d is needed if the file contains results only one operating point.
        data = np.atleast_2d(np.loadtxt(file))

        # Get the wind or rotor speed.
        x = data[:, 0]
        fid.readline()  # Skip line
        txt = fid.readline()  # Read type of x
        if txt.startswith("# Rotor"):
            x_type = "Rotor speed [rad/s]"
        else:
            x_type = "Wind speed [m/s]"

    # Create the index.
    index = pd.Index(x, name=x_type)

    # List of channels.
    if ((data.shape[1] - 1) // n_mode) % (15 * 2) == 0:  # Normal case.
        # fmt: off
        channel = [
            "TWR x mag. [m]", "TWR x phase [deg]",
            "TWR y mag. [m]", "TWR y phase [deg]",
            "TWR yaw mag. [rad]", "TWR yaw phase [deg]",
            "SFT x mag. [m]", "SFT x phase [deg]",
            "SFT y mag. [m]", "SFT y phase [deg]",
            "SFT tor mag. [rad]", "SFT tor phase [deg]",
            "Sym edge mag. [m]", "Sym edge phase [deg]",
            "BW edge mag. [m]", "BW edge phase [deg]",
            "FW edge mag. [m]", "FW edge phase [deg]",
            "Sym flap mag. [m]", "Sym flap phase [deg]",
            "BW flap mag. [m]", "BW flap phase [deg]",
            "FW flap mag. [m]", "FW flap phase [deg]",
            "Sym tors mag. [rad]", "Sym tors phase [deg]",
            "BW tors mag. [rad]", "BW tors phase [deg]",
            "FW tors mag. [rad]", "FW tors phase [deg]",
        ]
        # fmt: on

    elif ((data.shape[1] - 1) // n_mode) % (
        18 * 2
    ) == 0:  # If there are super elements.
        # fmt: off
        channel = [
            "TWR x mag. [m]", "TWR x phase [deg]",
            "TWR y mag. [m]", "TWR y phase [deg]",
            "TWR yaw mag. [rad]", "TWR yaw phase [deg]",
            "SFT x mag. [m]", "SFT x phase [deg]",
            "SFT y mag. [m]", "SFT y phase [deg]",
            "SFT tor mag. [rad]", "SFT tor phase [deg]",
            "Sym edge mag. [m]", "Sym edge phase [deg]",
            "BW edge mag. [m]", "BW edge phase [deg]",
            "FW edge mag. [m]", "FW edge phase [deg]",
            "Sym flap mag. [m]", "Sym flap phase [deg]",
            "BW flap mag. [m]", "BW flap phase [deg]",
            "FW flap mag. [m]", "FW flap phase [deg]",
            "Sym tors mag. [rad]", "Sym tors phase [deg]",
            "BW tors mag. [rad]", "BW tors phase [deg]",
            "FW tors mag. [rad]", "FW tors phase [deg]",
            "SE x mag. [m]", "SE x phase [deg]",
            "SE y mag. [m]", "SE y phase [deg]",
            "SE yaw mag. [rad]", "SE yaw phase [deg]",
        ]
        # fmt: on

    else:  # Generic case that should never happen.
        channel = ["Ch {}".format(i) for i in range(data.shape[1] - 1) // n_mode]

    # Create the columns. First: mode number, second level: channel.
    columns = pd.MultiIndex.from_product(
        [i_modes, channel], names=["Mode number", "Channel"]
    )

    # Assemble the DataFrame.
    df = pd.DataFrame(data=data[:, 1:], index=index, columns=columns)

    return df


def read_hs2_output_bea(file):
    """
    Read .bea files produced by HAWCStab2.
    Only the first part of the file is read, till the --------.

    Parameters
    ----------
    file : str or pathlib.Path
        The file to be read.

    Returns
    -------
    list of list of pandas DataFrame
        Each element of the list is a different substructure. Each substructure
        contains a number of bodies

    """
    # Define a list of three substructures, each with a variable number of bodies.
    substructure = [
        [],
        [],
        [],
    ]  # Don't write [[]]*3, as this will duplicate the pointers.

    # Header.
    columns = [
        "Elem. no.",
        "Length [m]",
        "Curve s [m]",
        "m [kg/m]",
        "x_cg_elem [m]",
        "y_cg_elem [m]",
        "rI about x [m]",
        "rI about y [m]",
        "Mass pitch [deg]",
        "x_sh_elem [m]",
        "y_sh_elem [m]",
        "E-modulus [GPa]",
        "G-modulus [GPa]",
        "I about x [m^4]",
        "I about y [m^4]",
        "K torsion [m^4]",
        "k_x shear [-]",
        "k_y shear [-]",
        "Area [m^2]",
    ]

    # Read the whole file.
    with open(file, "r") as fid:
        text = fid.readlines()
    i_line = -1  # current line.

    # Loop over the substructures.
    while True:
        # Read the current substructure.
        i_line += 1
        line = text[i_line]
        if line.startswith("----"):
            # The last substructure has been read.
            break
        i_sub = int(re.findall(r"Bodies of substructure no\. *(\d+)", line)[0]) - 1
        # Loop over the bodies.
        while True:
            i_line += 1
            line = text[i_line]
            if line.startswith("Bodies of") or line.startswith("----"):
                # There are no more bodies for this substructure.
                i_line -= 1
                break
            i_body = int(re.findall(r"Elements of body no\. *(\d+)", line)[0]) - 1
            # Skip the header.
            i_line += 1
            # Line at which the data block starts.
            i_line_start_data = i_line + 1
            # Loop over the data.
            while True:
                i_line += 1
                line = text[i_line]
                if (
                    line.startswith("Elements")
                    or line.startswith("Bodies")
                    or line.startswith("----")
                ):
                    # There are no more numeric data.
                    i_line_end_data = i_line
                    i_line -= 1
                    break
            # Get the numeric data.
            data_txt = text[i_line_start_data:i_line_end_data]
            # Parse the data.
            # Alternatively use np.fromstring().
            data_num = np.array(
                [list(map(float, data_txt[i].split())) for i in range(len(data_txt))]
            )
            # Save the data.
            substructure[i_sub].append(pd.DataFrame(data=data_num, columns=columns))
            if i_body != len(substructure[i_sub]) - 1:
                raise Exception("Mismatch on body indices.")

    return substructure


def read_hs2_frf(file):
    """
    Read HAWCStab2 Frequency Response Function file.

    To get the FRF for a specific input-output pair write:

        amplitude = df.loc[:, ('y( 5) <- v( 1)', 'Amplitude')]
        phase     = df.loc[:, ('y( 5) <- v( 1)', 'Phase [rad]')]

    Parameters
    ----------
    file : str or pathlib.Path
        The file to be read.

    Returns
    -------
    df: pandas DataFrame
        Table containing the FRF
        Index: frequency
        Columns: first level=input-output pair,
                 second level=amplitude and phase.
    """
    # Read the file.
    with open(file, "r") as fid:
        header = fid.readline()

    data = np.loadtxt(file)

    # Parse the header.
    # io_pairs = header.split(sep='                    ')
    io_pairs = re.findall(r"y\( *\d+\) <- [uv]\( *\d+\)", header)

    quantities = ["Amplitude", "Phase [rad]"]

    # Assemble the dataframe.
    df = pd.DataFrame(
        data=data[:, 1:],
        index=pd.Index(data=data[:, 0], name="Frequency [Hz]"),
        columns=pd.MultiIndex.from_product(
            [io_pairs, quantities], names=["IO Pairs", "Quantities"]
        ),
    )

    return df


def read_hs2_controller_tuning(file):
    """
    Read HAWCStab2 controller input file.

    Parameters
    ----------
    file : str or pathlib.Path
        The file to be read.

    Returns
    -------
    out: dict
         Dictionary containing the controller gains.
    """
    exp1 = re.compile(r"\w+ = +(.+) .+")
    exp2 = re.compile(r"K1 = +(.+) \[deg\], K2 = +(.+) \[deg\^2\].+")

    with open(file, "r") as fid:
        lines = fid.readlines()

    out = {}
    out["Kopt_partial"] = float(re.findall(exp1, lines[1])[0])
    out["Kp_partial"] = float(re.findall(exp1, lines[4])[0])
    out["Ki_partial"] = float(re.findall(exp1, lines[5])[0])
    out["Kp_full"] = float(re.findall(exp1, lines[7])[0])
    out["Ki_full"] = float(re.findall(exp1, lines[8])[0])
    out["K1_theta"] = float(re.findall(exp2, lines[9])[0][0])
    out["K2_theta"] = float(re.findall(exp2, lines[9])[0][1])

    return out


def read_h2_st_file(file, set=1, subset=1):
    """
    Read HAWC2 st file.

    Parameters
    ----------
    file : str or pathlib.Path
        The file to be read.
    set : int
        Set number
    subset : int
        Subset number.

    Returns
    -------
    df: pandas DataFrame
        Table with the structural properties.
        Index: Span
        Columns: Structural properties.

    """
    # Read the whole st file.
    with open(file, "r") as fid:
        lines = fid.readlines()

    # Regular expression to parse the set number.
    set_re = re.compile(r"#(\d+).*")
    # Regular expression to parse the subset number and number of stations.
    subset_re = re.compile(r"[$](\d+)\s+(\d+).*")

    # Line number.
    i = -1
    # Current set.
    set_i = 0
    # Current subset.
    subset_i = 0
    # Look for the set number.
    while True:
        i += 1
        try:
            line = lines[i]
        except IndexError:
            raise ValueError(f"Set {set} requested, but last set is {set_i}.")
        mm = re.match(set_re, line)
        if mm is not None:
            set_i = int(mm.group(1))
            if set_i == set:
                # Set found. Now look for the subset number.
                while True:
                    i += 1
                    try:
                        line = lines[i]
                    except IndexError:
                        raise ValueError(
                            f"Subset {subset} requested, but last subset is {subset_i}."
                        )
                    mm = re.match(subset_re, line)
                    if mm is not None:
                        subset_i = int(mm.group(1))
                        n_lines = int(mm.group(2))
                        if subset_i == subset:
                            # Subset found. Now read the data.
                            i += 1
                            data = np.array(
                                [
                                    [float(n) for n in s.split()]
                                    for s in lines[i : i + n_lines]
                                ]
                            )
                            if data.shape[1] == 19:
                                cols = (
                                    "m",
                                    "x_cg",
                                    "y_cg",
                                    "ri_x",
                                    "ri_y",
                                    "x_sh",
                                    "y_sh",
                                    "E",
                                    "G",
                                    "I_x",
                                    "I_y",
                                    "K",
                                    "k_x",
                                    "k_y",
                                    "A",
                                    "pitch",
                                    "x_e",
                                    "y_e",
                                )
                            elif data.shape[1] == 30:
                                cols = (
                                    "m",
                                    "x_cg",
                                    "y_cg",
                                    "ri_x",
                                    "ri_y",
                                    "pitch",
                                    "x_e",
                                    "y_e",
                                    # fmt: off
                                    "K_11", "K_12", "K_13", "K_14", "K_15", "K_16",
                                            "K_22", "K_23", "K_24", "K_25", "K_26",
                                                    "K_33", "K_34", "K_35", "K_36",
                                                            "K_44", "K_45", "K_46",
                                                                    "K_55", "K_56",
                                                                            "K_66",
                                    # fmt: on
                                )
                            else:
                                raise ValueError(
                                    f"Number of columns is {data.shape[1]}, while it must be either 19 or 30."
                                )
                            df = pd.DataFrame(
                                data=data[:, 1:],
                                index=pd.Index(data=data[:, 0], name="r"),
                                columns=cols,
                            )
                            return df


def compute_error(res_1, res_2):
    """
    Compute the error between two set of data.

    Parameters
    ----------
    res_1 : numpy array of pandas DataFrame
        The reference results.
    res_2 : numpy array of pandas DataFrame
        The new results.

    Returns
    -------
    abs_err : numpy array or pandas DataFrame
        The absolute error between the results.
    rel_err : numpy array or pandas DataFrame
        The relative error between the results.

    """
    abs_err = res_2 - res_1
    rel_err = res_2 / res_1 - 1.0
    return abs_err, rel_err
