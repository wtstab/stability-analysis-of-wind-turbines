# Motion due to Multi-Blade Coordinates

To better understand the concept of Multi-Blade Coordinates we will draw a wind turbine model and animate them.

We start by defining the reference frames used to build the structure, for which we have chosen the HAWC2 convention {cite}`HAWC2Manual`. The global frame $\mathcal{G}$ has its origin at the tower bottom. $z^{\mathcal{G}}$ is vertical pointing vertical downward, $y^{\mathcal{G}}$ is horizontal pointing downwind and $x^{\mathcal{G}}$ is horizontal pointing right when looking upwind. The tower frame $\mathcal{T}$ coincides with $\mathcal{G}$. The shaft frame $\mathcal{S}$ has its origin at the tower top. $z^{\mathcal{S}}$ is aligned with the shaft pointing upwind, $y^{\mathcal{S}}$ points down and $x^{\mathcal{S}}$ points right when looking upwind. The hub frame $\mathcal{H}$ has its origin at the shaft end. $z^{\mathcal{H}}$ points down, $y^{\mathcal{H}}$ is aligned with the shaft pointing downwind and $x^{\mathcal{H}}$ points right when looking upwind. The blade frames $\mathcal{B}_b$ have their origin at the blade roots. $z^{\mathcal{B}_b}$ is aligned with the blade pointing towards the tip, $y^{\mathcal{B}_b}$ is the flap at the root and $x^{\mathcal{B}_b}$ the edge at the root.

Our simple turbine is composed by a stiff tower and shaft, and 3 flexible blades. Each array is defined as a 2D array with shape $3 \times N_{\text{nodes}}$, where each column contains $(x^{\mathcal{B}_b}, y^{\mathcal{B}_b}, z^{\mathcal{B}_b})$ of a node. The length of each beam is arbitrary, as we are only interested in drawing a structure with the realistic proportions. Since the purpose of this code is to make an animation, we will do the computations in mixed precision. We will keep double precision for accuracy-critical operations like the 3D rotations, but lower it to single for the displacements. Let us now define the tower, shaft and blades in their local reference frames $\mathcal{T}$, $\mathcal{S}$, $\mathcal{B}_1$, $\mathcal{B}_2$ and $\mathcal{B}_3$.

```python
import numpy as np

tower_span = np.linspace(0.0, 1.5, 2, dtype=np.float32)
tower = np.zeros((3, tower_span.size), dtype=np.float32)
tower[2, :] = - tower_span

shaft_span = np.linspace(0.0, 0.1, 2, dtype=np.float32)
shaft_body = np.zeros((3, shaft_span.size), dtype=np.float32)
shaft_body[2, :] = shaft_span

blade_span = np.linspace(0.0, 1.0, 21, dtype=np.float32)
blade_1_body = np.zeros((3, blade_span.size), dtype=np.float32)
blade_1_body[2, :] = blade_span

blade_2_body = blade_1_body.copy()
blade_3_body = blade_1_body.copy()
```

To assemble the bodies it is necessary to rotate them. To this end we define the rotation matrix around the axes $x$, $y$ and $z$
\begin{align}
    \mat{R}_x(\phi) &= \begin{bmatrix}
        1  &           0  &                  0       \\
        0  &  \cos(\phi)  &  -\sin(\phi)             \\
        0  &  \sin(\phi)  &  \phantom{-}\cos(\phi)
    \end{bmatrix},
    \\
    \mat{R}_y(\phi) &= \begin{bmatrix}
         \cos(\phi)  &  0  &  \sin(\phi)   \\
                  0  &  1  &           0   \\
        -\sin(\phi)  &  0  &  \cos(\phi)
    \end{bmatrix},
    \\
    \mat{R}_z(\phi) &= \begin{bmatrix}
         \cos(\phi)  &            -\sin(\phi)  &  0   \\
         \sin(\phi)  &  \phantom{-}\cos(\phi)  &  0   \\
                  0  &                      0  &  1
    \end{bmatrix}.
\end{align}

```python
def rot_x(x):
    s = np.sin(x)
    c = np.cos(x)
    return np.array([
        [1.0, 0.0, 0.0],
        [0.0,   c,  -s],
        [0.0,   s,   c]])

def rot_y(x):
    s = np.sin(x)
    c = np.cos(x)
    return np.array([
        [  c, 0.0,   s],
        [0.0, 1.0, 0.0],
        [ -s, 0.0,   c]])

def rot_z(x):
    s = np.sin(x)
    c = np.cos(x)
    return np.array([
        [  c,  -s, 0.0],
        [  s,   c, 0.0],
        [0.0, 0.0, 1.0]])
```

We are now ready to assemble the structure. The tower is the base of our turbine, and is already written in the global frame, therefore we do not need to do anything. The shaft needs to be rotated by 90 deg around $x$ and then translated to the tower top.

```python
# Get the tower top.
base = tower[:, [-1]]

rot_base = rot_x(np.deg2rad(90))
shaft_global = np.float32(rot_base) @ shaft_body
shaft_global += base
base = shaft_global[:, [-1]]
```

The hub frame is obtained by composing the 3D rotations. We start from the shaft frame, and then rotate it by -90 deg around $x$.

```python
rot_base = rot_x(np.deg2rad(-90)) @ rot_base
```

We are now ready to create the blade frames. Blade 1 is obtained by rotating by 180 deg around $y$, blade 2 by rotating by +60 deg and blade 3 by -60 deg. At this stage we are not yet considering the rotation due to the azimuth angle.

```python
rot_bld1_ref = rot_base.copy()
rot_bld1_ref = np.float32(rot_y(np.deg2rad(180)) @ rot_base)

rot_bld2_ref = rot_base.copy()
rot_bld2_ref = np.float32(rot_y(np.deg2rad(+60)) @ rot_base)

rot_bld3_ref = rot_base.copy()
rot_bld3_ref = np.float32(rot_y(np.deg2rad(-60)) @ rot_base)
```

Next, we have to set the animation parameters. We will consider two cases: standing still and operating. The rotor rotates at an arbitrary rotor speed $\Omega$, while the blades vibrate harmonically, with a frequency $f$ at an arbitrary amplitude. For the operating case we set:

```python
# Amplitude of vibration (exaggerated on purpose).
amplitude = 0.3

# Rotor speed [rpm].
rotor_speed_rpm = 5.0

# Rotor speed [rad/s].
rotor_speed = rotor_speed_rpm * np.pi / 30.0

# Number of periods to simulate [-].
n_periods = 1

# Period of rotation [s].
period = 2 * np.pi / rotor_speed

# Time step [s].
dt = period / 180

# Time array [s].
time = np.arange(0.0, period * n_periods, dt)

# Rotor azimuth for all time instants [rad].
azimuth = np.mod(rotor_speed * time, 2 * np.pi)

# Frequency of multi-blade coordinate vibration [rad/s].
frequency = rotor_speed * 2.0
```

while for the standing still one we set:

```python
rotor_speed_rpm = 0.0
rotor_speed = 0.0
n_periods = 1
frequency = 2 * np.pi * 0.5
period = 2 * np.pi / frequency
dt = period / 100
time = np.arange(0.0, period * n_periods, dt)
azimuth = np.zeros((time.size))
```

These settings ensure that an animation will cover a full period, both of rotor rotation and blade vibration. 

Now that the azimuth array is defined, we can compute the rotation matrix for the rotor rotation. This is obtained by rotating around $y$ by the opposite azimuth angle, to obtain a clockwise rotation. Since all time steps and degrees of freedom are independent, we can vectorize the whole code. To this end, the time must be the first dimension. Obviously, the possibilities of vectorizing a real simulation code are much more limited, since the structure deforms according to the load path (blade tip to root) and each time step depends on the previous ones.

```python
s = np.sin(- azimuth)
c = np.cos(- azimuth)
rot_azi = np.zeros((time.size, 3, 3), dtype=np.float32)
rot_azi[:, 0, 0] = c
rot_azi[:, 0, 2] = s
rot_azi[:, 1, 1] = 1.0
rot_azi[:, 2, 0] = - s
rot_azi[:, 2, 2] = c
```

The azimuth of each blade is computed efficiently by broadcasting.
```python
# Azimuth of all blades. Axes ordered as:
#  - 0: time
#  - 1: blade (0, 1, 2)
psi = - azimuth[:, np.newaxis] + np.array([np.pi, +np.pi / 3, -np.pi / 3])[np.newaxis, :]
```

We assume that the blades move harmonically according to 1 MBC at a time.

```python
# Motion in Multi-Blade Coordinates. The order is: average, cosine, sine.
imbc = 1  # cos.
mbc = np.zeros((time.size, 3), dtype=np.float32)
mbc[:, imbc] = np.sin(frequency * time)
```

The motion in MBC must be converted to the blade ones by means of the inverse Coleman transformation {eq}`eq:inverse_coleman_3_blades`.

```python
# Define the inverse Coleman transformation.
invcoleman = np.ones((time.size, 3, 3), dtype=np.float32)
invcoleman[:, :, 1] = np.cos(psi)
invcoleman[:, :, 2] = np.sin(psi)

# Convert MBC to blade coordinates. Axes ordered as:
#  - 0: time
#  - 1: blade (0, 1, 2)
bld = (invcoleman @ mbc[:, :, np.newaxis])[:, :, 0]
```

We then define the position of the blades at all time instants in body coordinates. We use `np.tile()` instead of `np.zeros()` to copy the blades span immediately.

```python
# Motion of the deformed blades in body coordinates. Axes ordered as:
#  - 0: time
#  - 1: direction (x, y, z)
#  - 2: nodes (root to tip)
# Copy blades initial condition to all time instants.
blade_1_body_def = np.tile(blade_1_body, (time.size, 1, 1))
blade_2_body_def = np.tile(blade_2_body, (time.size, 1, 1))
blade_3_body_def = np.tile(blade_3_body, (time.size, 1, 1))
```

The blades are deformed in either flap or edge, by an amount that is quadratic with the span.

```python
ixy = 1  # Flap.
blade_1_body_def[:, ixy, :] = amplitude * bld[:, [0]] * blade_span[np.newaxis, :] ** 2
blade_2_body_def[:, ixy, :] = amplitude * bld[:, [1]] * blade_span[np.newaxis, :] ** 2
blade_3_body_def[:, ixy, :] = amplitude * bld[:, [2]] * blade_span[np.newaxis, :] ** 2
```

The deformed blades are converted to the global frame, by first rotating them to their reference position and then by the azimuth angle.

```python
# Axes ordered as:
#  - 0: time
#  - 1: direction (x, y, z)
#  - 2: nodes (root to tip)
blade_1_global = rot_azi @ rot_bld1_ref @ blade_1_body_def
blade_2_global = rot_azi @ rot_bld2_ref @ blade_2_body_def
blade_3_global = rot_azi @ rot_bld3_ref @ blade_3_body_def
```

Finally, the blades are moved to the hub.

```python
blade_1_global += base
blade_2_global += base
blade_3_global += base
```

In the next sections we will show these animations using the Plotly library {cite}`plotly` . We have not relied on the more popular matplotlib because matplotlib requires an active python kernel to display a 3D plot or an animation, while Plotly relies on javascript.
