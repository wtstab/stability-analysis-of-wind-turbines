(chap:Stability_analysis_of_anisotropic_wind_turbines)=
# Stability analysis of anisotropic wind turbines

(sec:Introduction)=
## Introduction

A wind turbine subject to any of:

- gravity,
- tower shadow,
- horizontal or vertical wind shear,
- upflow, yaw, veer,
- mass or aerodynamic imbalances,

is called anisotropic. These turbines are characterized by a periodic equilibrium (i.e. a periodic deflection of the turbine members), and hence are correctly modeled as LTP systems.

When subject to a steady wind, an anisotropic wind turbine settles on a periodic trajectory. By indicating with $T$ the average period of the rotor, the input, degrees of freedom and output will be periodic with period $T$, and indicated with $\tilde{\vec{w}}(t)$, $\tilde{\vec{q}}(t)$ and $\tilde{\vec{z}}(t)$ respectively. We can thus linearize the system over the periodic trajectory $\{\tilde{\vec{q}}(t), \tilde{\vec{w}}(t)\}$. The resulting LTP system is
```{math}
:label: eq:ltp_sys
    \dot{\vec{x}}(t) &= \mat{A}(t) \vec{x}(t) + \mat{B}(t) \vec{u}(t),
    %
    \\
    %
    \vec{y}(t) &= \mat{C}(t) \vec{x}(t) + \mat{D}(t) \vec{u}(t),
```
with
\begin{align}
    \vec{x}(t) &= \vec{q}(t) - \tilde{\vec{q}}(t),
    \\
    \vec{u}(t) &= \vec{w}(t) - \tilde{\vec{w}}(t),
    \\
    \vec{y}(t) &= \vec{z}(t) - \tilde{\vec{z}}(t).
\end{align}
$\vec{x}(t) \in \mathbb{R}^{N_x}$ is the state, $\vec{u}(t) \in \mathbb{R}^{N_u}$ the input and $\vec{y}(t) \in \mathbb{R}^{N_y}$ the output. The system matrices are given by
\begin{align}
    \mat{A}(t) &= \left. \vec{\nabla}_{\vec{q}} \vec{f} \right|_{\tilde{\vec{q}}, \tilde{\vec{w}}},
    &
    \mat{B}(t) &= \left. \vec{\nabla}_{\vec{w}} \vec{f} \right|_{\tilde{\vec{q}}, \tilde{\vec{w}}},
    \\
    \mat{C}(t) &= \left. \vec{\nabla}_{\vec{q}} \vec{g} \right|_{\tilde{\vec{q}}, \tilde{\vec{w}}},
    &
    \mat{D}(t) &= \left. \vec{\nabla}_{\vec{w}} \vec{g} \right|_{\tilde{\vec{q}}, \tilde{\vec{w}}},
\end{align}
and are periodic with period $T$, meaning that $\mat{A}(t) = \mat{A}(t+T)$, $\mat{B}(t) = \mat{B}(t+T)$, $\mat{C}(t) = \mat{C}(t+T)$, $\mat{D}(t) = \mat{D}(t+T)$.

If the rotor or the external conditions are anisotropic, the Coleman transformation {eq}`eq:periodic_to_invariant_a_mat_with_coleman` does not provide anymore a LTI system, but leaves a remaining periodicity.

Before showing the correct solution of the stability problem for system {eq}`eq:ltp_sys`, we will show how to **not** solve it.


(sec:Time_frozen_approximation)=
## Time-frozen approximation

The first method to **not** solve the stability of an anisotropic turbine is the time-frozen approximation. In this case, the eigenvalue problem is solved for different, fixed, values of $t$, to provide time-varying eigenvalues and mode shapes, often called short-term frequency and damping. Although this is correct for slowly time-varying systems (an hypothesis that needs to be verified), it is in general wrong {cite}`SBittanti_PColaneri_2008`. In particular, for arbitrary periods:

- The obtained frequencies, damping ratios and mode shapes do not have any physical meaning, and are not observed in the measurements. 
- There are unstable systems with eigenvalues having negative real part for all $t$, and vice versa, stable systems with eigenvalues having positive real part for all $t$.

We will proceed to show a counter example, developed by Vinograd and described in {cite}`SBittanti_PColaneri_2008`.
````{prf:example} Vinograd's counter example
:label: vinograd_1

Let us consider the following LTP system
```{math}
:label: eq:vinograd
    \dot{\vec{x}}(t) = \begin{bmatrix}
            -1 -9\cos^2(6 t) + 12\sin(6t)\cos(6t)  &  12\cos^2(6t) + 9\sin(6t)\cos(6t)     \\
            -12\sin^2(6t) - 9\sin(6t)\cos(6t)      &  -1 -9\sin^2(6t) -12\sin(6t)\cos(6t)
        \end{bmatrix} \vec{x}(t),
```
with period $T = \pi/3$. By simulating this system from the arbitrary initial condition $\vec{x}_0 = (1, 1)^\transpose$ we obtain {numref}`fig:vinograd_simulation_time_history`, which clearly shows that this system is unstable.

:::{figure-md} fig:vinograd_simulation_time_history
<img src="../figures/vinograd/simulation_time_history.svg">

Simulation of system {eq}`eq:vinograd`.
:::

System {eq}`eq:vinograd` has constant eigenvalues -1 and -10. We then proceed to compute the short-term frequencies and damping ratios over the period, which are shown in {numref}`fig:vinograd_short_term_frequency` and {numref}`fig:vinograd_short_term_damping`.

:::{figure-md} fig:vinograd_short_term_frequency
<img src="../figures/vinograd/short_term_frequency.svg">

Short-term natural frequencies of system {eq}`eq:vinograd`.
:::

:::{figure-md} fig:vinograd_short_term_damping
<img src="../figures/vinograd/short_term_damping.svg">

Short-term damping ratios of system {eq}`eq:vinograd`.
:::

Since the damping ratios are constant and equal to +100%, we can conclude that the time-frozen approximation does not provide meaningfull results.

The correct solution for this system requires Floquet theory, as will be shown later in {prf:ref}`vinograd_2`.
````


(sec:Direct_averaging)=
## Direct averaging

A second method is the direct averaging. In this case, a time-invariant state matrix is obtained through an average over the period
\begin{equation}
    \bar{\mat{A}}
        = \frac{1}{T} \int_{0}^{T} \mat{A}(t) \diff t,
\end{equation}
and the eigenvalue problem is solved for $\bar{\mat{A}}$. This approach has been tested on a wind turbine model in {cite}`GBir_2008` and {cite}`KStol_etal_2009`, where it has been shown that the error is unacceptably large.

The results of the direct averaging can be greatly improved by applying the Coleman transformation before averaging over the period. This method has been tested in {cite}`GBir_2008` and {cite}`KStol_etal_2009`, where it was shown to provide accurate results. However, the error introduced by the averaging process has never been bounded.


(sec:Floquet_theory)=
## Floquet theory

The stability analysis of continuous-time periodic systems has been first tackled by Floquet in {cite}`MGFloquet_1883`. The discrete-time case is instead more involved and has been clarified only recently, in {cite}`PVanDooren_JSreedhar_1994`. For an up to date, comprehensive, reference the reader is referred to the book by Bittanti and Colaneri {cite}`SBittanti_PColaneri_2008`. Here we will consider the stability analysis as composed of two parts: the reducibility problem and the modal decomposition. The reducibility problem aims at transforming the periodic system into a time-invariant one, eventually obtaining the frequencies and damping of its modes. The modal decomposition instead, shows how the evolution of the state is determined from the modes of the system.

Let us consider the following autonomous LTP system, with period $T$,

```{math}
:label: eq:autonomous_LTP_system_continuous_time
    \dot{\vec{x}}(t)
        &= \mat{A}(t) \vec{x}(t),
    %
    \qquad
    %
    \vec{x}(0) = \vec{x}_0,
    %
    \\
    %
    \vec{y}(t)
        &= \mat{C}(t) \vec{x}(t).
```
where $\vec{x} \in \mathbb{R}^{N_x}$ is the state vector, and $\vec{y} \in \mathbb{R}^{\ell}$ the output vector. 
By hypothesis, the periodicity is embedded in the $\mat{A}(t)$ and $\mat{C}(t)$ matrices, so that
```{math}
:label: eq:periodicity_of_A_and_C
    \mat{A}(t) = \mat{A}(t+T),
    \qquad
    \mat{C}(t) = \mat{C}(t+T),
```
for all $t$. The smallest $T$ satisfying Eq. {eq}`eq:periodicity_of_A_and_C` is defined as the system period. 

A fundamental tool to study the stability of this system is the state transition matrix $\mat{\Phi}(t,\tau)$, that relates the state at time $\tau$ with the one at time $t$, according to
```{math}
:label: eq:STM_between_states
    \vec{x}(t)
        = \mat{\Phi}(t,\tau) \vec{x}(\tau).
```
The evolution of the state transition matrix is governed by the following matrix ODE
```{math}
:label: eq:STM_ODE
    \dot{\mat{\Phi}}(t,\tau)
        = \mat{A}(t) \mat{\Phi}(t,\tau),
    %
    \qquad
    %
    \mat{\Phi}(\tau,\tau)
        = \mat{I},
```
with $\mat{I}$ the identity matrix.
If we indicate the initial time with $\tau$, the state after $k$ periods is $\tilde{\vec{x}}_\tau(k) = \vec{x}(\tau+kT)$. We then introduce the state transition matrix over one period, that is indicated with $\mat{\Psi}_\tau = \mat{\Phi}(\tau+T,\tau)$, and termed monodromy matrix. By definition the monodromy matrix relates two states separated by one period, and therefore determines the evolution of $\tilde{\vec{x}}_\tau(k)$. It can be easily seen that the sampled state obeys a linear invariant, discrete-time, system
```{math}
:label: eq:system_with_sampled_state
    \tilde{\vec{x}}_\tau[k+1]
        = \mat{\Psi}_\tau \tilde{\vec{x}}_\tau[k],
```
where we have indicated $\tau$ as a subscript to emphasize that it's a fixed parameter, and $k$ is the discrete time. The stability of the original system {eq}`eq:autonomous_LTP_system_continuous_time` can be assessed by checking the one of system {eq}`eq:system_with_sampled_state` for all $\tau \in [0, T]$. In the following we will always assume that the monodromy matrix can be diagonalized, therefore we can write
\begin{equation}
    \mat{\Psi}_\tau
        = \mat{S}_\tau \diag(\theta_j) \mat{S}_\tau^{-1},
\end{equation}
where the eigenvalues $\theta_j$ are named characteristic multipliers. It can be proved (see {cite}`SBittanti_PColaneri_2008`) that the characteristic multipliers, and their multiplicity, do not depend on $\tau$, therefore we have omitted the subscript $\tau$. From this discussion it follows that system {eq}`eq:autonomous_LTP_system_continuous_time` is asymptotically stable if all the characteristic multipliers lie within the open unit disk, i.e. if
\begin{equation}
    \abs{\theta_j} < 1,
    \qquad
    \forall j \in [1, N_x].
\end{equation}

The characteristic multipliers are very useful to assess the stability of the system, but they do not have a direct relationship with the evolution of the state. Therefore, we seek a different strategy. The reducibility problem is the one of finding, if any, a similarity transformation, such that in the new coordinates the system becomes time-invariant {cite}`RAJohnson_GRSell_1981`. It is important to remark that, for continuous-time periodic systems the reducibility problem can always be solved {cite}`SBittanti_PColaneri_2008`.

Starting from system {eq}`eq:autonomous_LTP_system_continuous_time`, we construct a similarity transformation $\mat{P}(t)$, such that the original state is transformed into $\bar{\vec{x}} = \mat{P}^{-1}(t) \vec{x}$. By hypothesis, the evolution of this new state is governed by a LTI equation
```{math}
:label: eq:autonomous_LTI_system_continuous_time
    \dot{\bar{\vec{x}}}
        = \mat{R} \bar{\vec{x}},
```
and $\mat{R}$ is named Floquet factor. A straightforward computation provides
```{math}
:label: eq:Floquet_factor
    \mat{R}
        = \mat{P}^{-1}(t)(\mat{A}(t) \mat{P}(t) - \dot{\mat{P}}(t)),
    %
    \quad
    %
    \forall t.
```
To evaluate this formula, $\mat{P}(t)$, $\mat{P}^{-1}(t)$ and $\dot{\mat{P}}(t)$ have to be nonsingular for all $t$. If this is the case, then $\mat{P}(t)$ is said to be a Lyapunov-Perron transformation {cite}`RAJohnson_GRSell_1981`. If furthermore $\mat{P}(t)$ is also periodic, with period $T$, then it is called Lyapunov-Floquet transformation. Clearly the stability of system {eq}`eq:autonomous_LTP_system_continuous_time` cannot depend on the basis in which the state is expressed, and hence it is equivalent to the one of system {eq}`eq:autonomous_LTI_system_continuous_time`. The $\bar{\mat{A}}$ in Eq. {eq}`eq:periodic_to_invariant_a_mat_with_coleman` is a particular Floquet factor in the case of isotropic rotor {cite}`PFSkjoldan_MHHansen_2009`. The state transition matrix associated to system {eq}`eq:autonomous_LTI_system_continuous_time` is
\begin{equation}
    \mat{\Phi}_{\mat{R}}(t,\tau)
        = e^{\mat{R}(t-\tau)},
\end{equation}
and by definition
\begin{equation}
    \bar{\vec{x}}(t)
        = \mat{\Phi}_{\mat{R}}(t,\tau) \bar{\vec{x}}(\tau).
\end{equation}
By applying the transformation matrix we can recover the original state
\begin{equation}
\begin{split}
    \vec{x}(t)
        &= \mat{P}(t) \mat{\Phi}_{\mat{R}}(t,\tau) \mat{P}^{-1}(\tau) \vec{x}(\tau)
        \\
        &= \mat{P}(t) e^{\mat{R}(t-\tau)} \mat{P}^{-1}(\tau) \vec{x}(\tau).
\end{split}
\end{equation}
A direct comparison with Eq.~\eqref{eq:STM_between_states} provides
```{math}
:label: eq:Floquet_form_of_state_transition_matrix
    \mat{\Phi}(t,\tau)
        = \mat{P}(t) e^{\mat{R}(t-\tau)} \mat{P}^{-1}(\tau).
```
This expression is known as Floquet form representation of the state transition matrix. An alternative derivation is provided in {cite}`SBittanti_PColaneri_2008`. The term $\mat{P}(t)$ contains the periodicity of the solution, while $\mat{R}$ determines its contractivity. By exploiting this decomposition we can write the monodromy matrix as
\begin{equation}
    \mat{\Psi}_\tau
        = \mat{P}(\tau+T) e^{\mat{R} T} \mat{P}^{-1}(\tau),
\end{equation}
and since $\mat{P}(t)$ is periodic we have
\begin{equation}
    \mat{\Psi}_\tau
        = \mat{P}(\tau) e^{\mat{R} T} \mat{P}^{-1}(\tau).
\end{equation}
We will always assume that also the Floquet factor can be diagonalized, and hence we write it as
\begin{equation}
    \mat{R} = \mat{V} \diag(\eta_j) \mat{V}^{-1}.
\end{equation}
The eigenvalues $\eta_j$ are called characteristic exponents. To find the eigenvalues and the eigenvectors of the Floquet factor, we equate the spectral and the Floquet decompositions of the monodromy matrix
\begin{align}
    \mat{S}_\tau \diag(\theta_j) \mat{S}_\tau^{-1}
        &= \mat{P}(\tau) e^{\mat{R} T} \mat{P}^{-1}(\tau)
    %
    \\
    %
    \mat{S}_\tau \diag(\theta_j) \mat{S}_\tau^{-1}
        &= \mat{P}(\tau) \mat{V} \diag(e^{\eta_j T}) \mat{V}^{-1} \mat{P}^{-1}(\tau)
    %
    \\
    %
     \diag(\theta_j)
        &= \mat{S}_\tau^{-1} \mat{P}(\tau) \mat{V} \diag(e^{\eta_j T}) \mat{V}^{-1} \mat{P}^{-1}(\tau) \mat{S}_\tau.
\end{align}

This last expression shows that $\mat{V} = \mat{P}^{-1}(\tau) \mat{S}_\tau$, and more importantly that
```{math}
:label: eq:characteristic_multipliers_from_exponents
    \theta_j = e^{\eta_j T},
    \qquad
    \forall j \in [1, N_x].
```
It follows that the characteristic exponents are given by
```{math}
:label: eq:characteristic_exponents_from_multipliers
    \eta_{j_n}
        = \frac{1}{T} \ln(\theta_j)
        = \frac{1}{T} \left( \ln(\abs{\theta_j}) + \imath (\angle(\theta_j) + 2 n \pi) \right),
```
with $n \in \mathbb{Z}$ an arbitrary integer. To simplify the notation we can introduce the angular speed $\Omega = 2 \pi / T$, and the reference characteristic exponent $\hat{\eta}_j = \eta_{j_0}$, so that
\begin{equation}
\label{eq:all_characteristic_exponents_of_a_mode}
    \eta_{j_n}
        = \hat{\eta}_j + \imath n \Omega,
    \qquad
    \forall n \in \mathbb{Z}.
\end{equation}
Since system {eq}`eq:autonomous_LTI_system_continuous_time` belongs to the  LTI class, it will be asymptotically stable if all the characteristic exponents have negative real part, i.e. if
\begin{equation}
    \Re(\eta_{j_n}) < 0,
    \qquad
    \text{$\forall j \in [1, \dots, N_x]$ and an arbitrary $n$}.
\end{equation}
As we have seen the reducibility problem for a periodic system has not a unique solution, as not only $\mat{P}(\tau)$ is arbitrary, but any $n$ will allow to obtain a valid Floquet factor. In other words
```{math}
:label: eq:floquet_factor_spectral_decomposition
    \mat{R}
         = \mat{V} \diag(\eta_{j_n}) \mat{V}^{-1},
    \quad
    \text{$\forall n \in \mathbb{Z}$, and any invertible $\mat{P}(\tau)$}.
```
A common choice is to choose as $\mat{P}(\tau)$ the identity matrix. Once the Floquet factor is known, it is possible to recover the periodic transformation by evaluating
```{math}
:label: eq:periodic_transformation_from_floquet_factor
\begin{split}
    \mat{P}(t)
        &= \mat{\Phi}(t,\tau) \mat{P}(\tau) e^{-\mat{R}(t-\tau)}
        \\
        &= \mat{\Phi}(t,\tau) \mat{S}_\tau \diag(e^{-\hat{\eta}_j (t-\tau)}) \mat{S}_\tau^{-1} \mat{P}(\tau).
\end{split}
```
The knowledge of $\mat{P}(t)$ allows to compute the modal decomposition of the state transition matrix, as explained in the next paragraph.

In the first part of this section, we have seen two criteria to assess the stability of a continuous-time periodic system, one based on the characteristic multipliers, and the other on the characteristic exponents. Naturally, these two criteria provide the same result, as the characteristic multipliers are linked to the characteristic exponents by Eq. {eq}`eq:characteristic_multipliers_from_exponents`. We will now show the relationship between the characteristic exponents and the evolution of the state, which is provided by the modal decomposition of the state transition matrix, first derived in {cite}`PFSkjoldan_MHHansen_2009`. The following derivation is based on {cite}`RRiva_SCacciola_CLBottasso_2016_Turbulent`.

By defining the matrix $\mat{\Xi}(t) = \mat{P}(t) \mat{V}$, the state transition matrix in Eq. {eq}`eq:Floquet_form_of_state_transition_matrix` becomes
```{math}
    \mat{\Phi}(t,\tau)
        = \mat{\Xi}(t) \diag(e^{\hat{\eta}_j(t-\tau)}) \mat{\Xi}^{-1}(\tau).
```
This expression can be written as a sum of modes by introducing the vectors
\begin{equation}
    \vec{\psi}_j(t)
        = \col_j(\mat{\Xi}(t)),
    %
    \qquad
    %
    \vec{\varrho}_j^\transpose(t)
        = \row_j(\mat{\Xi}^{-1}(t)).
\end{equation}
from thus
```{math}
:label: eq:expansion_of_state_transition_matrix_over_modes
    \mat{\Phi}(t,\tau)
        = \sum_{j=1}^{N_x} \vec{\psi}_j(t) \vec{\varrho}_j^\transpose(\tau) e^{\hat{\eta}_j(t-\tau)}.
```
Since $\mat{P}(t)$ is periodic, so is $\mat{\Xi}(t)$, and in turn $\vec{\psi}_j(t)$. Therefore, we can expand this vector in Fourier series
```{math}
    \vec{\psi}_j(t)
        = \sum_{n = -\infty}^{+\infty} \vec{\psi}_{j_n} e^{\imath n \Omega t},
```
and upon substitution in the previous, assuming for simplicity $\tau = 0$, we finally get
```{math}
:label: eq:modal_decomposition_of_state_transition_matrix
    \mat{\Phi}(t,\tau)
        = \sum_{j=1}^{N_x}
           \sum_{n=-\infty}^{+\infty}
            \vec{\psi}_{j_n} \vec{\varrho}_j^\transpose(\tau) e^{(\hat{\eta}_j+\imath n \Omega) t}.
```
This expression is the modal decomposition of the state transition matrix. The state is given by $\vec{x}(t) = \mat{\Phi}(t,\tau) \vec{x}(\tau)$, i.e.
```{math}
:label: eq:state_Floquet_decomposition_continuous_time
    \vec{x}(t)
        = \sum_{j=1}^{N_x}
           \sum_{n=-\infty}^{+\infty}
            \vec{\psi}_{j_n} r_j e^{(\hat{\eta}_j+\imath n \Omega) t},
```
with $r_j = \vec{\varrho}_j^\transpose(\tau) \vec{x}(\tau)$. A direct comparison of this formula with the LTI case highlights that:

- The characteristic exponents $\eta_{j_n} = \hat{\eta}_j+\imath n \Omega$ play the same role of the eigenvalues of a LTI system, and hence provide the natural frequencies and damping factors of the system.
- The arrays $\vec{\psi}_{j_n} \in \mathbb{C}^{N_x}$ are the mode shapes, and $r_j$ are the associated scaling factors. It must be stressed that the scaling factor depends solely on the mode, and not on the harmonics $n$.
- While an isotropic rotor has only $2 \tilde{B} + 1$ harmonics per mode, an anisotropic rotor has an infinite number of harmonics.

Eq. {eq}`eq:modal_decomposition_of_state_transition_matrix` shows that a continuous-time periodic system has $N_x$ modes, each characterized by an infinite number of characteristic exponents, that are separated in the imaginary part by an integer multiple of the angular speed.  It follows that each mode $j$ can be either stable or unstable, although every $\eta_{j_n}$ has a different frequency and damping ratio, as shown in {numref}`tab:ltp_sys_stability`. Each mode $j \in [1, \dots, N_x]$ is thus described by an infinite number of mode shapes $\vec{\psi}_{j_n}$ and characteristic exponents $\eta_{j_n}$. We thus see how the indeterminacy in the Floquet factor is reflected in the expression of the state transition matrix.

The modal decomposition of the output is obtained by combining {eq}`eq:autonomous_LTP_system_continuous_time` and {eq}`eq:STM_between_states`, to get
```{math}
:label: eq:output_from_state_with_STM
    \vec{y}(t)
        = \mat{C}(t) \mat{\Phi}(t,\tau) \vec{x}(\tau).
```
The derivation is the same as before, and we arrive at computing 
```{math}
    \mat{C}(t) \mat{\Phi}(t,\tau)
        = \sum_{j=1}^{N_x} \mat{C}(t) \vec{\psi}_j(t) \vec{\varrho}_j^\transpose(\tau) e^{\hat{\eta}_j(t-\tau)}.
```
This time, we need to expand $\mat{C}(t) \vec{\psi}_j(t)$ in Fourier series
```{math}
    \mat{C}(t) \vec{\psi}_j(t)
        = \sum_{n = -\infty}^{+\infty} \vec{\psi^y}_{j_n} e^{\imath n \Omega t},
```
where $\vec{\psi^y}_{j_n} \in \mathbb{C}^{\ell}$ are the observed mode shapes. The modal decomposition of the output is then
```{math}
:label: eq:output_Floquet_decomposition_continuous_time
    \vec{y}(t)
        = \sum_{j=1}^{N_x}
           \sum_{n=-\infty}^{+\infty}
            \vec{\psi^y}_{j_n} r_j e^{(\hat{\eta}_j+\imath n \Omega) t}.
```

Wind turbine models are often obtained using the finite element method, and in turn have a very large number of states. It is thus common practice to keep a relatively small set of low frequency modes, since they are the ones that will show up in the response. This technique is not applicable to LTP systems, since changing $n$ causes $\eta_{j_n}$ to go from nearly 0 to infinite frequency. In {cite}`CLBottasso_SCacciola_2015` and {cite}`RRiva_SCacciola_CLBottasso_2016_Turbulent` the authors introduced a particularly effective way to condense the information, which is based on the mode shapes, and is thus objective. For each mode $j$ and harmonic $n$ they defined the participation factor as
```{math}
:label: eq:participation_factor
    \phi_{j_n}
        = \frac{||\vec{\psi}_{j_n}||}
               {\sum_n ||\vec{\psi}_{j_n}||}.
```
From this definition, it follows that $0 \le \phi_{j_n} \le 1$. The closer a participation factor is to 1, the more that combination of mode $j$ and harmonic $n$ will participate to the response. The output-specific participation factors are defined as
```{math}
:label: eq:output_specific_participation_factor
    \phi^y_{j_n}
        = \frac{||\vec{\psi^y}_{j_n}||}
               {\sum_n ||\vec{\psi^y}_{j_n}||}.
```
The output-specific participation factors are straightforward to apply when there is only 1 output channel, but their application to multiple-output systems requires some care. As we have previously observed for isotropic rotors, changing reference frame, from ground-fixed to rotating, affects which harmonics participate to the response. It is thus important to compute the observed mode shapes always in the same reference frame, typically ground-fixed, and only afterwards compute the participation factors. The participation factors in the ground-fixed and rotating frames have been computed in {cite}`RRiva_2018_mode_shapes`, where we can see that changing frame causes the highest participation factor to switch by $n = \pm 1$. We can introduce the following definition

```{prf:definition} Principal harmonic
:label: principal_harmonic
:nonumber:

For each mode, the principal harmonic is the one with the highest participation factor in the ground-fixed frame.
```

This definition generalizes {numref}`tab:most_important_harmonics` to anisotropic rotors. The harmonics are re-numbered from the principal one, which has now index $n = 0$.

```{table} Elements to quantify the stability of a Linear Time-Periodic system.
:name: tab:ltp_sys_stability
| Name                                  | Expression                                                                           | Unit
|---------------------------------------|--------------------------------------------------------------------------------------|------
| Natural frequency                     | $f = \abs{\eta_{j_n}} / (2 \pi)$                                                     | Hz
| Damped frequency                      | $\hat{f} = \abs{\Im(\eta_{j_n})} / (2 \pi)$                                          | Hz
| Damping ratio                         | $\zeta = - \Re(\eta_{j_n}) / \abs{\eta_{j_n}}$                                       | --
| Logarithmic decrement                 | $\delta = 2 \pi \zeta / \sqrt{1 - \zeta^2}$                                          | --
| Mode shape                            | $\vec{\psi}_{j_n}$                                                                   | Same as $\vec{x}$
| Observed mode shape                   | $\vec{\psi^y}_{j_n}$                                                                 | Same as $\vec{y}$
| Deflection shape                      | $\vec{\psi}_{{j_n}_r} = \abs{\vec{\psi}_{j_n}} \cos(\angle \vec{\psi}_{j_n})$        | Same as $\vec{x}$
| Observed deflection shape             | $\vec{\psi^y}_{{j_n}_r} = \abs{\vec{\psi^y}_{j_n}} \cos(\angle \vec{\psi^y}_{j_n})$  | Same as $\vec{y}$
| Participation factor                  | $\phi_{j_n}$                                                                         | --
| Output-specific participation factor  | $\phi^y_{j_n}$                                                                       | --
```

````{admonition} Tip for quickly labeling the natural frequencies of a mode
:class: tip

Compute the PSD of a signal, if applicable and possible in the ground-fixed frame. Then, start from the highest peak. That is the principal harmonic $0\Omega$, with natural frequency $f_{j_0}$. Compute the average rotor speed $\Omega$. Assuming that mode $j$ is low-damped, we can quickly compute all its harmonics with

```{math}
    f_{j_n} = \abs{f_{j_0} + \imath n \Omega} \qquad \text{for some $n \in \mathbb{Z}$}.
```

Notice how the absolute value allows the harmonics to "reflect" on the 0 frequency. We will observe this later in {numref}`fig:mathieu_oscillator_free_response_psd_with_labels`.
````

````{prf:example} Mathieu oscillator
:label: mathieu_stability

The Mathieu oscillator is a simple analytical model, that has been used countless times to study LTP systems. It represents an harmonic oscillator whose stiffness varies periodically in time. In this example, we will apply Floquet theory to do its stability analysis. The equation of motion can be found in {cite}`MSAllen_MWSracic_SChauhan_MHHansen_2011`. Its derivation is the same as in {prf:ref}`harmonic_oscillator`, with the only difference that the stiffness is now given by $k(t) = k_0 + k_1 \cos(\Omega t)$. We can then define $\omega_0 = k_0 / m$ and $\omega_1 = k_1 / m$, so that the EOM reads
    
```{math}
:label: eq:mathieu
    \begin{pmatrix}
        \dot{x}   \\
        \ddot{x}
    \end{pmatrix} &= \begin{bmatrix}
         0                                        &    1                 \\
        -\omega_0^2 - \omega_1^2 \cos(\Omega t)   &   -2\zeta\omega_0
    \end{bmatrix} \begin{pmatrix}
        x        \\
        \dot{x}
    \end{pmatrix} + \begin{bmatrix}
        0        \\
        1 / m
    \end{bmatrix} f,
    %
    \\
    %
    y &= \begin{bmatrix}
        1   &   0
    \end{bmatrix} \begin{pmatrix}
        x        \\
        \dot{x}
    \end{pmatrix},
```
with $m = 1$, $\omega_0^2 = 1$, $\omega_1^2 = 0.4$, $2\zeta\omega_0 = 0.04$ and $\Omega = 0.8$ rad/s.

To do the stability analysis we only need the autonomous version, and therefore set $f = 0$. We begin by simulating its free response, starting from the arbitrary initial condition $\vec{x}_0 = (1, 0)^\transpose$. The result is shown in {numref}`fig:mathieu_oscillator_free_response_time`.

:::{figure-md} fig:mathieu_oscillator_free_response_time
<img src="../figures/mathieu_oscillator/free_response_time.svg">

First 100 s of the free response of system {eq}`eq:mathieu`.
:::

The associated PSD is shown in {numref}`fig:mathieu_oscillator_free_response_psd`, and reveals that several harmonics participate to the response. Since the time-frozen approximation would have only provided 2 eigenvalues, this shows once again that its results are meaningless.

:::{figure-md} fig:mathieu_oscillator_free_response_psd
<img src="../figures/mathieu_oscillator/free_response_psd.svg" >

PSD of the free response of system {eq}`eq:mathieu`.
:::

We then proceed to apply Floquet theory. The first step is to compute the state transition matrix over 1 period, by integrating {eq}`eq:STM_ODE`. The result is shown in {numref}`fig:mathieu_oscillator_state_transition_matrix`. Since the solution contracts, we can conclude that the system is asymptotically stable.

:::{figure-md} fig:mathieu_oscillator_state_transition_matrix
<img src="../figures/mathieu_oscillator/state_transition_matrix.svg">

State transition matrix of system {eq}`eq:mathieu` over 1 period.
:::

```{admonition} Tip for integrating a matrix ODE
:class: tip

Since Matlab `ode45` and Scipy `solve_ivp` require a 1D array, it is possible to integrate a matrix ODE by converting back and forth between 1D and 2D arrays. The integration of matrix ODE should instead work out of the box with Julia DifferentialEquations.jl.
```

The state transition matrix at the end of the period is by definition the monodromy matrix. Let us use it to compute the state at every period, thus verifying {eq}`eq:system_with_sampled_state`. The result is shown in {numref}`fig:mathieu_oscillator_free_response_time_with_monodromy`, and as expected it matches the numerical integration

:::{figure-md} fig:mathieu_oscillator_free_response_time_with_monodromy
<img src="../figures/mathieu_oscillator/free_response_time_with_monodromy.svg">

First 100 s of the free response of system {eq}`eq:mathieu`. The points are obtained by evaluating {eq}`eq:system_with_sampled_state`.
:::

From the monodromy matrix, we can compute the characteristic multipliers. Since the Mathieu oscillator has 2 states, we obtain 2 modes. The characteristic multipliers are shown in {numref}`fig:mathieu_oscillator_characteristic_multiplier`. Since they are both within the open unit circle, we can confirm that the system is asymptotically stable.

:::{figure-md} fig:mathieu_oscillator_characteristic_multiplier
<img src="../figures/mathieu_oscillator/characteristic_multiplier.svg">

Characteristic multipliers of system {eq}`eq:mathieu`.
:::

The next step is to compute the output-specific participation factors, for which we only need one characteristic exponent per mode. We can arbitrarily set $n = 0$ and use {eq}`eq:characteristic_exponents_from_multipliers` to convert the characteristic multipliers into characteristic exponents, to obtain

```{math}
    \eta_{1_0} &= -0.02002913 + 0.98696917\imath,   \\
    \eta_{2_0} &= -0.02002913 + 0.61303083\imath.
```
As expected, the characteristic exponents have negative real part, which confirms that the system is asymptotically stable. To compute the Floquet factor we arbitrarily set $\mat{P}(0) = \mat{I}$ and immediately obtain that the eigenvectors of the Floquet factor are the same as the ones of the monodromy matrix, i.e. $\mat{V} = \mat{S}_0$. The Floquet factor is then obtained by evaluating its spectral decomposition {eq}`eq:floquet_factor_spectral_decomposition`, which gives
```{math}
    \mat{R}
        = \begin{bmatrix}
            -0.01665522 + 0.8\imath  &  0.16899765                 \\
            -0.20691917              &  -0.02340303 + 0.8\imath
          \end{bmatrix}.
```        
The indeterminacy of the Floquet factor is due to the arbitrary choice of $\mat{P}(0)$ and $n$. Next we can compute the periodic transformation $\mat{P}(t)$ using {eq}`eq:periodic_transformation_from_floquet_factor`. Since the matrix exponential is computationally expensive, the second line is preferred. Finally, we compute $\mat{C}(t) \mat{\Xi}(t)$ and expand it in Fourier series to obtain the observed mode shapes. The Fourier series is efficiently computed using the FFT algorithm. The last step is to compute the output-specific participation factors, to know which harmonic matters the most. Knowing this, we can finally compute all the characteristic exponents, which are plotted in {numref}`fig:mathieu_oscillator_characteristic_exponent`.

:::{figure-md} fig:mathieu_oscillator_characteristic_exponent
<img src="../figures/mathieu_oscillator/characteristic_exponent.svg">

Low frequency characteristic exponents of system {eq}`eq:mathieu`.
:::

Finally, we gather the most important harmonics for mode 1 in {numref}`tab:mathieu_modal_properties`. We can observe that the Mathieu oscillator has 2 modes, each comprised by an infinite number of frequencies and damping ratios. Only the harmonics with sufficient participation will show up in the response.

```{table} Modal properties of the Mathieu oscillator.
:name: tab:mathieu_modal_properties
|  Harmonic   | Natural frequency  <br /> [Hz] | Damping ratio <br /> [%] | Participation <br /> [%]
|-------------|--------------------------------|--------------------------|-------------------------
|      -4     |             0.352              |            0.91%         |          0.05%
|      -3     |             0.225              |            1.42%         |          0.97%
|      -2     |             0.098              |            3.27%         |          4.77%
|      -1     |             0.030              |           10.65%         |         15.83%
|      +0     |             0.157              |            2.03%         |         71.57%
|      +1     |             0.284              |            1.12%         |          6.55%
|      +2     |             0.412              |            0.77%         |          0.23%
|      +3     |             0.539              |            0.59%         |          0.00%
|      +4     |             0.666              |            0.48%         |          0.00%
```

We can now label each peak in the PSD plotted in {numref}`fig:mathieu_oscillator_free_response_psd_with_labels`, and conclude that Floquet theory has been able to predict all peaks.

:::{figure-md} fig:mathieu_oscillator_free_response_psd_with_labels
<img src="../figures/mathieu_oscillator/free_response_psd_with_labels.svg" >

PSD of the free response of system {eq}`eq:mathieu`, with harmonics labeled.
:::

````


````{prf:example}  Vinograd's counter example (continued)
:label: vinograd_2

Now we have all the elements to correctly do the stability analysis of Vinograd's system {eq}`eq:vinograd`.

The monodromy matrix, computed from time 0, is
```{math}
:label: eq:vinograd_monodromy
    \mat{\Psi}_0
        = 0.2 \begin{bmatrix}
                 e^{2T} + 4e^{-13T}  &  2e^{2T} - 2e^{-13T}   \\
                2e^{2T} - 2e^{-13T}  &  4e^{2T} + e^{-13T}
              \end{bmatrix}.
```
Its eigenvalues, i.e. characteristic multipliers, are $\theta_1 = e^{-13T}$ and $\theta_2 = e^{2T}$. Since $\abs{\theta_2} > 1$ **the system is unstable**. Knowing $\theta_j$, we can compute the characteristic exponents using {eq}`eq:characteristic_exponents_from_multipliers`. The principal characteristic exponents are $\eta_1 = -13$ and $\eta_2 = 2$. Once again, since $\Re(\eta_2) > 0$ the system is unstable.

Finally, we can compute the modal properties. Since the principal characteristic exponents are real, we can consider only the positive harmonics. Then, the damped frequencies are the shifts $n\Omega$. The damped frequencies, in Hz, are shown in the following table. The harmonic 0 (i.e. the principal) does not oscillate and therefore will not appear in the time histories.

| Harmonic  | Mode 1  | Mode 2                                  
|-----------|---------|-------
| 0         | 0.0     |  0.0
| 1         | 0.95    |  0.95
| 2         | 1.91    |  1.91

Next, we can look at the damping ratios.

| Harmonic  | Mode 1  | Mode 2                                  
|-----------|---------|-------
| 0         | 100%    | -100%
| 1         | 90.79%  | -31.62%
| 2         | 73.48%  | -16.44%

All damping ratios of mode 1 are positive and high, therefore mode 1 will not appear in the time histories. Mode 2 instead has negative damping ratios and is therefore unstable. The PSD of the time history is shown in {numref}`fig:vinograd_simulation_psd`.

:::{figure-md} fig:vinograd_simulation_psd
<img src="../figures/vinograd/simulation_psd.svg" width=100%>

PSD of system {eq}`eq:vinograd`.
:::

The PSD has only one peak at about 0.95 to 1 Hz, which reveals that harmonic 1 of mode 2 is the one observed for this simulation. For reference, its natural frequency is 1.00 Hz.
````

At this point, it is fair to ask why studying isotropic rotors, when Floquet theory is more general. The reasons is that the the theory of isotropic rotors allows to:
- Qualitatively explains the turbine modes, while Floquet theory has no knowledge of them.
- Provide the principal, $-\Omega$ and $+\Omega$ harmonics for all modes, which are the most important ones.
- Perform the stability analysis at an acceptable computational cost, while the one of Floquet is very high.

The validity of the isotropic assumption in anisotropic conditions has been questioned in a few papers. See for example {cite}`PFSkjoldan_MHHansen_2013`, {cite}`CLBottasso_SCacciola_2015` and {cite}`KStol_etal_2009`. Usually, the conclusion is that the Coleman approximation provides accurate results, but it should be stressed that these studies are always turbine- and external conditions-dependent. To conclude, it is best to keep in mind the following quote

```{epigraph}
Wind turbines operating in normal conditions are very much isotropic.

-- Torben Juul Larsen, personal communication
```


(sec:Forced_response_of_LTP_systems)=
## Forced response of Linear Time-Periodic systems

We will now turn our attention to the forced response, by adopting the frequency domain representation. The forced response of LTP systems has been developed by N. M. Wereley {cite}`NMWereley_1990`. See {cite}`SBittanti_PColaneri_2008` for a recent explanation.

To study this problem we introduce the following definition.

```{prf:definition} Exponentially Modulated Periodic (EMP) signal
:label: emp_signal
:nonumber:

A signal $q(t)$ written as

$$
q(t) = \sum_{n=-\infty}^{+\infty} q_n e^{(s + \imath n \Omega) t}.
$$
```

In the LTI case we have seen that a periodic input leads to a periodic output with the same period, but this is not the case for a LTP system. In fact, it can be proved that:

```{prf:theorem} EMP regime
:label: emp_regime
:nonumber:

A LTP system with period $T$, subject to an EMP input with period $T$, admits an EMP regime with the same period.
```


Knowing this, we can rewrite all vectors as EMP signals. For the state we find
\begin{equation}
    \vec{x}(t)
        = \sum_{n=-\infty}^{+\infty} \vec{x}_n e^{(s + \imath n \Omega) t},
\end{equation}
and similarly for $\vec{u}(t)$ and $\vec{y}(t)$. Taking the derivative of the previous expression we get
\begin{equation}
    \dot{\vec{x}}(t)
        = \sum_{n=-\infty}^{+\infty} (s + \imath n \Omega) \vec{x}_n e^{(s + \imath n \Omega) t}.
\end{equation}
We can then expand all matrices in Fourier series. For the $\mat{A}(t)$ matrix we get
\begin{equation}
    \mat{A}(t)
        = \sum_{n=-\infty}^{+\infty} \mat{A}_n e^{\imath n \Omega t}.
\end{equation}
Since $\mat{A}(t)$ is real, the harmonics are in complex-conjugate pairs, i.e. $\mat{A}_{-n} = \mat{A}_n^*$. The $\mat{B}(t)$, $\mat{C}(t)$, $\mat{D}(t)$ matrices are written in the same manner. We can collect all harmonics of the vectors as
\begin{equation}
    \hat{\vec{x}}
        = \left(
            \dots,
            \vec{x}_{-1}^\transpose,
            \vec{x}_{ 0}^\transpose,
            \vec{x}_{+1}^\transpose,
            \dots
          \right)^\transpose,
\end{equation}
and similarly the harmonics of the input and output are collected in $\hat{\vec{u}}$ and $\hat{\vec{y}}$ respectively. The harmonics of the $\mat{A}(t)$ matrix are instead collected as
\begin{equation}
    \mat{\mathcal{A}}
        = \begin{bmatrix}
               \ddots   &  \vdots        &  \vdots        &  \vdots        &            \\
               \cdots   &  \mat{A}_{ 0}  &  \mat{A}_{-1}  &  \mat{A}_{-2}  &  \cdots    \\
               \cdots   &  \mat{A}_{+1}  &  \mat{A}_{ 0}  &  \mat{A}_{-1}  &  \cdots    \\
               \cdots   &  \mat{A}_{+2}  &  \mat{A}_{+1}  &  \mat{A}_{ 0}  &  \cdots    \\
                        &  \vdots        &  \vdots        &  \vdots        &  \ddots    \\
        \end{bmatrix}.
\end{equation}
Similarly, the harmonics of the $\mat{B}(t)$, $\mat{C}(t)$, $\mat{D}(t)$ matrices are collected in $\mat{\mathcal{B}}$, $\mat{\mathcal{C}}$ and $\mat{\mathcal{D}}$ respectively. Assuming $\vec{x}_0 = \vec{0}$ we can apply the harmonic balance, to rewrite the system as
\begin{align}
    s \hat{\vec{x}}
        &= (\mat{\mathcal{A}} - \mat{\mathcal{N}}) \hat{\vec{x}} + \mat{\mathcal{B}} \hat{\vec{u}},
    %
    \\
    %
    \hat{\vec{y}}
        &= \mat{\mathcal{C}} \hat{\vec{x}} + \mat{\mathcal{D}} \hat{\vec{u}},
\end{align}
with $\mat{\mathcal{N}} = \diag(\imath n \Omega \mat{I})$ and $n \in \mathbb{Z}$. Just like in the LTI case, we can eliminate the state from this algebraic system, to get
\begin{equation}
    \hat{\vec{y}} = \mat{\mathcal{G}}(s) \hat{\vec{u}},
\end{equation}
with
\begin{equation}
    \mat{\mathcal{G}}(s)
        = \mat{\mathcal{C}} \left(s \mat{I} - (\mat{\mathcal{A}} - \mat{\mathcal{N}}) \right)^{-1}
        + \mat{\mathcal{D}}
\end{equation}
the Harmonic Transfer Function (HTF). Although this expression looks similar to the one of $\mat{G}(s)$, there are some important differences:

- $s$ is the one used to compute the EMP expansions of the signals.
- $\mat{\mathcal{G}}(s)$ is infinite dimensional, hence it needs to be truncated.


Computing the HTF is extremely expensive, therefore it is worth simplifying the problem. By setting $s = 0$ we turn the input into an ordinary periodic signal
\begin{equation}
    \vec{u}(t)
        = \sum_{n=-\infty}^{+\infty} \vec{u}_n e^{\imath n \Omega t}.
\end{equation}
We then have
\begin{equation}
    \hat{\vec{y}} = \mat{\mathcal{G}}(0) \hat{\vec{u}},
\end{equation}
where
\begin{equation}
    \mat{\mathcal{G}}(0)
        = \mat{\mathcal{C}} \left(\mat{\mathcal{N}} - \mat{\mathcal{A}} \right)^{-1}
        + \mat{\mathcal{D}}
\end{equation}
is the Harmonic Frequency Response Function (HFRF).


(ssec:Response_due_to_steady_wind)=
### Response due to a steady wind

We will now compute the output of a LTP system due to a constant input, as done in {cite}`RRiva_SCacciola_CLBottasso_2016_Turbulent`. In this case the input is $\vec{u}(t) = \bar{\vec{u}}$, hence
\begin{equation}
    \hat{\vec{u}}
        = \left(
            \dots,
            \vec{0}^\transpose,
            \bar{\vec{u}}^\transpose,
            \vec{0}^\transpose,
            \dots
          \right)^\transpose.
\end{equation}
By applying the HFRF we get
\begin{equation}
    \begin{pmatrix}
        \vdots         \\
        \vec{y}_{-1}   \\
        \vec{y}_{ 0}   \\
        \vec{y}_{+1}   \\
        \vdots         \\
    \end{pmatrix}
        = \mat{\mathcal{G}}(0) \begin{pmatrix}
                                    \vdots          \\
                                    \vec{0}         \\
                                    \bar{\vec{u}}   \\
                                    \vec{0}         \\
                                    \vdots          \\
                                \end{pmatrix}.
\end{equation}
By converting the output in time domain we find
\begin{equation}
    \vec{y}_{\text{steady state}}(t)
        = \sum_{n=-\infty}^{+\infty} \vec{y}_n e^{\imath n \Omega t},
\end{equation}
which shows that a wind turbine subject to a steady wind has an output periodic at the rotor speed and its integer multiples. This is the mathematical origin of the $n \times \mathrm{Rev}$ (often called $n\mathrm{P}$). More in general, if the input is periodic with frequency $\omega_0$, the output is periodic, with harmonics at frequencies $\omega_0 + n \Omega$, $\forall n \in \mathbb{Z}$. To better understand this result we have simulated the IEA 10 MW with: gravity, steady inflow of 8 m/s, upflow of 8 deg and a wind shear with power law exponent of 0.2. The simulation has been done for 10000 s, so that the very low-damped tower side-side mode has had enough time to settle, and only the forced response is left. The PSD computed on the last 200 s is shown in {numref}`fig:IEA10MW_hawc2_only_wind_np`.

:::{figure-md} fig:IEA10MW_hawc2_only_wind_np
<img src="../figures/IEA10MW/hawc2_only_wind_np.svg" width=100%>

Interpretation of the $n\mathrm{P}$ from the response of the IEA 10 MW wind turbine, under anisotropic conditions.
:::

From {numref}`fig:IEA10MW_hawc2_only_wind_np` we can see that the $n\mathrm{P}$ follow a pattern, with mainly the $3 n\mathrm{P}$ visible in the ground-fixed frame. This phenomenon is easily explained by using the concept of the rotor as a filter, introduced by C. L. Bottasso in {cite}`MBertele_CLBottasso_SCacciola_2018`. As seen above, when the wind is steady the behavior of a wind turbine becomes periodic. A force $f_b(t)$ for blade $b$ can thus be written in Fourier series
\begin{equation}
    f_b(t) = a_0 + \sum_{n = 1}^{+\infty} a_n \cos(n \psi_b(t)) + b_n \sin(n \psi_b(t)).
\end{equation}
The resulting force on the tower $f_T(t)$ is
\begin{equation}
    f_T(t) = \sum_{b = 1}^{B} f_b(t).
\end{equation}
By substituting the blade force we can see that many harmonics cancel, and the tower force is
\begin{equation}
    f_T(t) = B \left( a_0 + \sum_{n = 1}^{+\infty} a_{Bn} \cos(B n \psi_b(t)) + b_{Bn} \sin(B n \psi_b(t)) \right).
\end{equation}
We can thus see that only harmonics multiple of the number of blades are passed to the tower. If $\Omega$ is not constant the filtering action is not perfect, and additional harmonics are passed. The blades have equal harmonics if the rotor is isotropic (the external conditions can however be anisotropic). The concept of rotor as a filter is analogous to three-phase electrical power.


%\section{Response due to turbulent winds}
%\label{sec:Response_due_to_turbulent_winds}

The response due to turbulent winds have been investigated in {cite}`DTcherniak_SChauhan_MHHansen_2010`. Some conclusions from that paper are:

- The PSD of the aerodynamic forces shows the turbulence spectrum when measured at the hub, but when it is measured along the blades, it shows peaks at $n \times \mathrm{Rev}$.
- The peaks get stronger towards the blade tip.
- The peaks have thick tails, which smooth and broaden the peaks due to the steady wind.
- The aerodynamic forces at the blade sections are highly correlated both temporally and spatially.


















