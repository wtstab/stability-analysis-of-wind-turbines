# Animations of modes of an isotropic turbine

We have used HAWCStab2 to visualize the structural modes of the IEA 10 MW, at 5 rpm. We will show the animations hereafter and highlight the similarities with the Multi-Blade Coordinates (MBC).


## 1ˢᵗ tower fore-aft

The 1ˢᵗ tower fore-aft mode, sometimes called nodding, is characterized by a movement of the tower in the wind direction. The tower motion is coupled to the collective flap, which depending on the turbine model might even exceed the one of the tower. Because of this coupling, this motion has a quite high damping ratio (around 10% for the IEA 10 MW).

<video controls autoplay muted loop height="640">
  <source src="_static/hawcstab2_videos/IEA10MW/structural_5rpm/mode_2.mp4" type="video/mp4">
  Your browser does not support the video tag.
</video>


## 1ˢᵗ tower side-side

The 1ˢᵗ tower side-side mode, is characterized by a movement of the tower orthogonal to the wind direction. The blades move in the edgewise direction, and therefore this mode is very low-damped. Its frequency is just a little bit higher than the 1ˢᵗ tower fore-aft.

<video controls autoplay muted loop height="640">
  <source src="_static/hawcstab2_videos/IEA10MW/structural_5rpm/mode_3.mp4" type="video/mp4">
  Your browser does not support the video tag.
</video>


## 1ˢᵗ flapwise backward whirl

The 1ˢᵗ flapwise backward whirl, is characterized by a movement of the blades in the flapwise direction, and therefore its damping is high. Its mode shape contains both the flap cos and sin MBC. 

<video controls autoplay muted loop height="640">
  <source src="_static/hawcstab2_videos/IEA10MW/structural_5rpm/mode_4.mp4" type="video/mp4">
  Your browser does not support the video tag.
</video>


## 1ˢᵗ flapwise collective

The 1ˢᵗ flapwise collective, is characterized by a movement of the blades in the flapwise direction, and therefore its damping is high. Its mode shape is dominated by the flap - average MBC, and therefore the blades vibrate in-phase.

<video controls autoplay muted loop height="640">
  <source src="_static/hawcstab2_videos/IEA10MW/structural_5rpm/mode_5.mp4" type="video/mp4">
  Your browser does not support the video tag.
</video>


## 1ˢᵗ flapwise forward whirl

The 1ˢᵗ flapwise forward whirl, is characterized by a movement of the blades in the flapwise direction, and therefore its damping is high. Its mode shape contains both the flap cos and sin MBC. 

<video controls autoplay muted loop height="640">
  <source src="_static/hawcstab2_videos/IEA10MW/structural_5rpm/mode_6.mp4" type="video/mp4">
  Your browser does not support the video tag.
</video>


## 1ˢᵗ edgewise backward whirl

The 1ˢᵗ edgewise backward whirl, is characterized by a movement of the blades in the edgewise direction, and therefore its damping is low. Its mode shape contains both the edge cos and sin MBC. 

<video controls autoplay muted loop height="640">
  <source src="_static/hawcstab2_videos/IEA10MW/structural_5rpm/mode_7.mp4" type="video/mp4">
  Your browser does not support the video tag.
</video>


## 1ˢᵗ edgewise forward whirl

The 1ˢᵗ edgewise forward whirl, is characterized by a movement of the blades in the edgewise direction, and therefore its damping is low. Its mode shape contains both the edge cos and sin MBC. 

<video controls autoplay muted loop height="640">
  <source src="_static/hawcstab2_videos/IEA10MW/structural_5rpm/mode_8.mp4" type="video/mp4">
  Your browser does not support the video tag.
</video>


## 1ˢᵗ edgewise collective

The 1ˢᵗ edgewise collective, is characterized by a movement of the blades in the edgewise direction, and therefore its damping is low. Its mode shape is dominated by the edge - average MBC. 

<video controls autoplay muted loop height="640">
  <source src="_static/hawcstab2_videos/IEA10MW/structural_5rpm/mode_12.mp4" type="video/mp4">
  Your browser does not support the video tag.
</video>
