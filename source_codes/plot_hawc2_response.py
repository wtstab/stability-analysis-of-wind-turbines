# -*- coding: utf-8 -*-
"""
Plot response of a turbine and convert to multi-blade coordinates.

@author: ricriv
"""

# %% Import.

import matplotlib.pyplot as plt
import numpy as np
import pandas as pd
from wetb.prepost import windIO

# %% Set up.

plt.rc(
    "axes",
    labelsize=11,
    titlesize=11,
    labelweight="bold",
    titleweight="bold",
    grid=True,
)
plt.rc("grid", linewidth=0.5)
plt.rc("xtick", labelsize=10)
plt.rc("ytick", labelsize=10)

plt.close("all")


# %% Load data.

# File name of the HAWC2 response file.
res_folder = "./../numerical_data/IEA10MW_local/res/"
res_file_name = "isotropic.hdf5"

# Load the data.
res = windIO.LoadResults(file_path=res_folder, file_name=res_file_name)
channel_names = [str(res.ch_details[i, 2]) for i in range(1, res.Nch)]
df = pd.DataFrame(
    data=res.sig[:, 1:],
    index=pd.Index(data=res.sig[:, 0], name="Time [s]"),
    columns=channel_names,
)


# %% Plot rotor.

fig_name = "isotropic_blade_tip_flap"
fig, ax = plt.subplots(num=fig_name, nrows=2, figsize=(12 / 2.54, 9 / 2.54), dpi=300)
#
ax[1].set_xlabel("Time [s]")
ax[0].set_ylabel("Blade flap [m]")
ax[1].set_ylabel("Blade flap [m]")
#
ax[0].set_title("Blade coordinates")
ax[0].plot(
    df.index.to_numpy(),
    df.loc[
        :,
        "State pos y  Mbdy:blade1 E-nr:  18 Z-rel:1.00 coo: blade1  blade 1 tip pos body",
    ],
    label="Blade 1",
)
ax[0].plot(
    df.index.to_numpy(),
    df.loc[
        :,
        "State pos y  Mbdy:blade2 E-nr:  18 Z-rel:1.00 coo: blade2  blade 2 tip pos body",
    ],
    label="Blade 2",
)
ax[0].plot(
    df.index.to_numpy(),
    df.loc[
        :,
        "State pos y  Mbdy:blade3 E-nr:  18 Z-rel:1.00 coo: blade3  blade 3 tip pos body",
    ],
    label="Blade 3",
)
#
ax[1].set_title("Global coordinates")
ax[1].plot(
    df.index.to_numpy(),
    df.loc[
        :,
        "State pos y  Mbdy:blade1 E-nr:  18 Z-rel:1.00 coo: global  blade 1 tip pos global",
    ],
    label="Blade 1",
)
ax[1].plot(
    df.index.to_numpy(),
    df.loc[
        :,
        "State pos y  Mbdy:blade2 E-nr:  18 Z-rel:1.00 coo: global  blade 2 tip pos global",
    ],
    label="Blade 2",
)
ax[1].plot(
    df.index.to_numpy(),
    df.loc[
        :,
        "State pos y  Mbdy:blade3 E-nr:  18 Z-rel:1.00 coo: global  blade 3 tip pos global",
    ],
    label="Blade 3",
)
#
ax[0].legend(loc="lower right")
ax[0].set_yticks(np.arange(-50, 50, 5))
ax[1].set_yticks(np.arange(-50, 50, 5))
ax[0].set_xlim(0.0, 100.0)
ax[1].set_xlim(0.0, 100.0)
ax[0].set_ylim(-10.0, +10.0)
ax[1].set_ylim(-35.0, +5.0)
plt.tight_layout()
fig.savefig(f"./../figures/IEA10MW/{fig_name}.svg", bbox_inches="tight")


# %% Plot shaft.

fig_name = "isotropic_shaft"
fig, ax = plt.subplots(num=fig_name, nrows=2, figsize=(12 / 2.54, 9 / 2.54), dpi=300)
#
ax[1].set_xlabel("Time [s]")
ax[0].set_ylabel("Shaft [m]")
ax[1].set_ylabel("Shaft [m]")
#
ax[0].set_title("Shaft coordinates")
ax[0].plot(
    df.index.to_numpy(),
    df.loc[
        :,
        "State pos x  Mbdy:shaft E-nr:   5 Z-rel:0.00 coo: shaft  generator pos shaft",
    ],
    label="Shaft x",
)
ax[0].plot(
    df.index.to_numpy(),
    df.loc[
        :,
        "State pos y  Mbdy:shaft E-nr:   5 Z-rel:0.00 coo: shaft  generator pos shaft",
    ],
    label="Shaft y",
)
#
ax[1].set_title("Global coordinates")
ax[1].plot(
    df.index.to_numpy(),
    df.loc[
        :,
        "State pos x  Mbdy:shaft E-nr:   5 Z-rel:0.00 coo: global  generator pos global",
    ],
    label="Shaft x",
)
ax[1].plot(
    df.index.to_numpy(),
    df.loc[
        :,
        "State pos x  Mbdy:shaft E-nr:   5 Z-rel:0.00 coo: global  generator pos global",
    ],
    label="Shaft y",
)
#
ax[0].legend(loc="lower right")
# ax[0].set_yticks(np.arange(-50, 50, 5))
# ax[1].set_yticks(np.arange(-50, 50, 5))
ax[0].set_xlim(0.0, 100.0)
ax[1].set_xlim(0.0, 100.0)
# ax[0].set_ylim(-10.0, +10.0)
# ax[1].set_ylim(-35.0, +5.0)
plt.tight_layout()
fig.savefig(f"./../figures/IEA10MW/{fig_name}.svg", bbox_inches="tight")
