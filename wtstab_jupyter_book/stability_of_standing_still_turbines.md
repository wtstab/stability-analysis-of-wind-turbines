(chap:stability_of_standing_still_wind_turbines)=
# Stability of standing still wind turbines

The modes of standing still, onshore, turbines have been described in {cite}`MHHansen_2007`. We have used HAWCStab2 to visualize the structural modes of the standing-still IEA 10 MW. We will show the animations hereafter and highlight the similarities with the Multi-Blade Coordinates (MBC).


## 1ˢᵗ tower fore-aft

The 1ˢᵗ tower fore-aft mode, sometimes called nodding, is characterized by a movement of the tower in the wind direction. The tower motion is coupled to the collective flap, which depending on the turbine model might even exceed the one of the tower.

<video controls autoplay muted loop height="640">
  <source src="_static/hawcstab2_videos/IEA10MW/structural_standing_still/mode_2.mp4" type="video/mp4">
  Your browser does not support the video tag.
</video>


## 1ˢᵗ tower side-side

The 1ˢᵗ tower side-side mode, is characterized by a movement of the tower orthogonal to the wind direction. The blades move in the edgewise direction, and therefore this mode is very low-damped.

<video controls autoplay muted loop height="640">
  <source src="_static/hawcstab2_videos/IEA10MW/structural_standing_still/mode_3.mp4" type="video/mp4">
  Your browser does not support the video tag.
</video>


## 1ˢᵗ asymmetric flapwise in yaw

The 1ˢᵗ asymmetric flapwise in yaw is dominated by the flap - sin MBC.

<video controls autoplay muted loop height="640">
  <source src="_static/hawcstab2_videos/IEA10MW/structural_standing_still/mode_4.mp4" type="video/mp4">
  Your browser does not support the video tag.
</video>


## 1ˢᵗ asymmetric flapwise in tilt

The 1ˢᵗ asymmetric flapwise in tilt is dominated by the flap - cos MBC.

<video controls autoplay muted loop height="640">
  <source src="_static/hawcstab2_videos/IEA10MW/structural_standing_still/mode_5.mp4" type="video/mp4">
  Your browser does not support the video tag.
</video>


## 1ˢᵗ collective flapwise

The 1ˢᵗ collective flapwise is dominated by the flap - average MBC.

<video controls autoplay muted loop height="640">
  <source src="_static/hawcstab2_videos/IEA10MW/structural_standing_still/mode_6.mp4" type="video/mp4">
  Your browser does not support the video tag.
</video>


## 1ˢᵗ vertical edgewise

The 1ˢᵗ vertical edgewise is dominated by the edge - sin MBC.

<video controls autoplay muted loop height="640">
  <source src="_static/hawcstab2_videos/IEA10MW/structural_standing_still/mode_7.mp4" type="video/mp4">
  Your browser does not support the video tag.
</video>


## 1ˢᵗ horizontal edgewise

The 1ˢᵗ horizontal edgewise is dominated by the edge - cos MBC.

<video controls autoplay muted loop height="640">
  <source src="_static/hawcstab2_videos/IEA10MW/structural_standing_still/mode_8.mp4" type="video/mp4">
  Your browser does not support the video tag.
</video>


## 1ˢᵗ collective edgewise

The 1ˢᵗ collective edgewise is dominated by the edge - average MBC.

<video controls autoplay muted loop height="640">
  <source src="_static/hawcstab2_videos/IEA10MW/structural_standing_still/mode_12.mp4" type="video/mp4">
  Your browser does not support the video tag.
</video>


## Frequencies and damping ratios

The structural frequencies and damping ratios of the standing still IEA 10 MW are reported in {numref}`tab:IEA10MW_standing_still_structural_frequency_and_damping`. Just like for the blades, their names are due to the mode shapes. As we can see, the damping ratios are similar, because the aerodynamics is not included. The tower fore-aft and side-side modes have almost the same frequency, but an aeroelastic analysis will show that the aeroelastic damping ratio of the tower side-side is very low, while the one of the tower fore-aft is quite high.

```{table} Structural frequencies and damping ratios of the standing still IEA 10 MW turbine.
:name: tab:IEA10MW_standing_still_structural_frequency_and_damping

| Name                                      | Damped frequency <br /> [Hz] | Damping ratio <br /> [%]
|-------------------------------------------|------------------------------| ------------------------
1<sup>st</sup> tower fore-aft               |  0.26                        |  0.3222
1<sup>st</sup> tower side-side              |  0.26                        |  0.3339
1<sup>st</sup> asymmetric flapwise in yaw   |  0.39                        |  0.2721
1<sup>st</sup> asymmetric flapwise in tilt  |  0.40                        |  0.3169
1<sup>st</sup> collective flapwise          |  0.43                        |  0.3529
1<sup>st</sup> vertical edgewise            |  0.67                        |  0.3636
1<sup>st</sup> horizontal edgewise          |  0.68                        |  0.3668
2<sup>nd</sup> asymmetric flapwise in yaw   |  0.95                        |  0.6002
2<sup>nd</sup> asymmetric flapwise in tilt  |  1.02                        |  0.7871
2<sup>nd</sup> collective flapwise          |  1.08                        |  0.8390
2<sup>nd</sup> collective edgewise          |  1.25                        |  0.5886
```

