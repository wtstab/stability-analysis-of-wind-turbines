"""

Reads HAWCStab2 files, and plot the mode shapes of the blade.

"""

# %% Initialization.

from pathlib import Path

import matplotlib as mpl

# mpl.use('Agg')
import matplotlib.pyplot as plt
import numpy as np
from routines_read_hs2 import read_hs2_output_amp_blade, read_hs2_output_cmb

plt.close("all")


# %% Input.

# Aeroelastic results.
cmb_file = Path(
    "./../numerical_data/DTU10MW/res_hs2/DTU_10MW_RWT_hs2_no_pitch_actuator_Blade.cmb"
)
amp_file = Path(
    "./../numerical_data/DTU10MW/res_hs2/DTU_10MW_RWT_hs2_no_pitch_actuator_Blade.amp"
)

# Output folder.
fig_folder = Path("./../figures/DTU10MW/Blade_aero")


# Selected wind speed.
wind_speed = 5.0

# Page dimensions.
page_width_pt = 372.0 - 10.0
page_height_pt = 601.90594 + 10

page_width_in = page_width_pt / 72.27
page_height_in = page_height_pt / 72.27


# %% Read.

cmb = read_hs2_output_cmb(cmb_file)
amp = read_hs2_output_amp_blade(amp_file)

cmb_ws = cmb.loc[5.0, ["Frequency [Hz]", "Damping [%]"]]


# %% Plot.


n_mode = amp.columns.levels[0][-1]

radius = amp.index.levels[1]

radius_round = np.ceil(radius[-1] / 10) * 10

mpl.rcParams["axes.grid"] = True
mpl.rcParams["grid.linewidth"] = 0.5
mpl.rcParams["axes.titlesize"] = 9
mpl.rcParams["axes.labelsize"] = 9
mpl.rcParams["axes.labelweight"] = "bold"
mpl.rcParams["axes.titleweight"] = "bold"
mpl.rcParams["xtick.labelsize"] = 8
mpl.rcParams["ytick.labelsize"] = 8
mpl.rcParams["lines.linewidth"] = 2

colors = mpl.rcParams["axes.prop_cycle"].by_key()["color"]

fig_list = []

for i_mode in range(n_mode):
    fig, ax = plt.subplots(
        3,
        3,
        sharex=True,
        num="Mode " + str(i_mode + 1),
        figsize=(page_height_in, page_width_in),
        dpi=300,
    )
    fig_list.append(fig)

    ax[0, 0].set_title("Magnitude [m,deg]")
    ax[0, 1].set_title("Phase [deg]")
    ax[0, 2].set_title("Deflection shape [m,deg]")
    ax[0, 0].set_ylabel("Edgewise")  # x
    ax[1, 0].set_ylabel("Flapwise")  # y
    ax[2, 0].set_ylabel("Torsion")
    ax[2, 0].set_xlabel("Radius [m]")
    ax[2, 1].set_xlabel("Radius [m]")
    ax[2, 2].set_xlabel("Radius [m]")

    ax[0, 0].set_xlim(0, radius_round)
    ax[0, 1].set_xlim(0, radius_round)
    ax[0, 2].set_xlim(0, radius_round)

    ax[0, 0].set_ylim(0, 1)
    ax[1, 0].set_ylim(0, 1)
    # ax[2,0].set_ylim(bottom=0)
    ax[0, 1].set_ylim(-180, +180)
    ax[1, 1].set_ylim(-180, +180)
    ax[2, 1].set_ylim(-180, +180)

    ax[0, 0].set_xticks(np.arange(0.0, radius_round + 1, 10.0))
    ax[0, 1].set_xticks(np.arange(0.0, radius_round + 1, 10.0))
    ax[0, 2].set_xticks(np.arange(0.0, radius_round + 1, 10.0))

    ax[0, 1].set_yticks([-180, -90, 0, 90, 180])
    ax[1, 1].set_yticks([-180, -90, 0, 90, 180])
    ax[2, 1].set_yticks([-180, -90, 0, 90, 180])

    ax[0, 0].plot(
        radius, amp.loc[(wind_speed,), (i_mode, "u_x bld [m]")], color=colors[0]
    )
    ax[1, 0].plot(
        radius, amp.loc[(wind_speed,), (i_mode, "u_y bld [m]")], color=colors[1]
    )
    ax[2, 0].plot(
        radius,
        amp.loc[(wind_speed,), (i_mode, "theta [rad]")] * 180 / np.pi,
        color=colors[2],
    )

    ax[0, 1].plot(
        radius, amp.loc[(wind_speed,), (i_mode, "phase_x [deg]")], color=colors[0]
    )
    ax[1, 1].plot(
        radius, amp.loc[(wind_speed,), (i_mode, "phase_y [deg]")], color=colors[1]
    )
    ax[2, 1].plot(
        radius, amp.loc[(wind_speed,), (i_mode, "phase_t [deg]")], color=colors[2]
    )

    ax[0, 2].plot(
        radius,
        amp.loc[(wind_speed,), (i_mode, "u_x bld [m]")]
        * np.cos(np.deg2rad(amp.loc[(wind_speed,), (i_mode, "phase_x [deg]")])),
        color=colors[0],
    )
    ax[1, 2].plot(
        radius,
        amp.loc[(wind_speed,), (i_mode, "u_y bld [m]")]
        * np.cos(np.deg2rad(amp.loc[(wind_speed,), (i_mode, "phase_y [deg]")])),
        color=colors[1],
    )
    ax[2, 2].plot(
        radius,
        amp.loc[(wind_speed,), (i_mode, "theta [rad]")]
        * 180
        / np.pi
        * np.cos(np.deg2rad(amp.loc[(wind_speed,), (i_mode, "phase_t [deg]")])),
        color=colors[2],
    )

    defl_min = np.min([ax[0, 2].get_ylim()[0], ax[1, 2].get_ylim()[0]])
    defl_max = np.max([ax[0, 2].get_ylim()[1], ax[1, 2].get_ylim()[1]])

    ax[0, 2].set_ylim(defl_min, defl_max)
    ax[1, 2].set_ylim(defl_min, defl_max)

    plt.tight_layout()

    # Save.
    fig.savefig(
        fig_folder / ("Mode_" + str(i_mode + 1) + ".pdf"),
        bbox_inches="tight",
        pad_inches=0,
    )
