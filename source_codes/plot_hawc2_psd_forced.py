# -*- coding: utf-8 -*-
"""
Plot PSD of loads when the turbine is only forced by the wind.

@author: ricriv
"""

# %% Import.

import matplotlib as mpl
import matplotlib.pyplot as plt
import numpy as np
import pandas as pd
from scipy import signal
from wetb.prepost import windIO

# %% Set up.

plt.close("all")

# mpl.use('Agg')
mpl.use("Qt5Agg")

plt.style.use("rcparams.mplstyle")


# %% Load data.

# File name of the HAWC2 response file.
res_folder = "./../numerical_data/IEA10MW_local/res/"
res_file_name = "anisotropic_only_wind_1.hdf5"

# Load the data.
res = windIO.LoadResults(file_path=res_folder, file_name=res_file_name)
channel_names = [str(res.ch_details[i, 2]) for i in range(1, res.Nch)]
df_time = pd.DataFrame(
    data=res.sig[:, 1:],
    index=pd.Index(data=res.sig[:, 0], name="Time [s]"),
    columns=channel_names,
)

time_start = 800.0

# Compute average rotor speed [Hz].
rotor_speed = np.mean(df_time.loc[time_start:, "shaft_rot angle speed"]) / 60.0

# Compute PSD.
dt = np.mean(np.diff(res.sig[:, 0]))
sampling_frequency = 1 / dt
nyquist_frequency = sampling_frequency / 2

window = np.ones(df_time.loc[time_start:, :].index.size)
noverlap = 0
nperseg = len(window)
nfft = nperseg
detrend = "constant"
return_onesided = True
scaling = "density"

y_freq, y_PSD = signal.welch(
    df_time.loc[time_start:, :],
    sampling_frequency,
    window,
    nperseg,
    noverlap,
    nfft,
    detrend,
    return_onesided,
    scaling,
    axis=0,
)

df_psd = pd.DataFrame(
    data=y_PSD,
    index=pd.Index(data=y_freq, name="Frequency [Hz]"),
    columns=channel_names,
)


# %% Plot time series.

fig_name = "anisotropic_only_wind_1_time_series"
fig, ax = plt.subplots(
    num=fig_name,
    # figsize=(12/2.54, 9/2.54),
    dpi=300,
)
#
ax.set_xlabel("Time [s]")
ax.set_ylabel("Moment [kNm]")
ax.plot(
    df_time.loc[time_start:, :].index.to_numpy(),
    df_time.loc[time_start:, "MomentMx Mbdy:tower nodenr:   1 coo: tower"],
    label="Tower bottom Fore-Aft",
)
ax.plot(
    df_time.loc[time_start:, :].index.to_numpy(),
    df_time.loc[time_start:, "MomentMy Mbdy:tower nodenr:   1 coo: tower"],
    label="Tower bottom Side-Side",
)
ax.plot(
    df_time.loc[time_start:, :].index.to_numpy(),
    df_time.loc[time_start:, "MomentMx Mbdy:blade1 nodenr:   1 coo: blade1"],
    label="Blade root flapwise",
)
ax.plot(
    df_time.loc[time_start:, :].index.to_numpy(),
    df_time.loc[time_start:, "MomentMy Mbdy:blade1 nodenr:   1 coo: blade1"],
    label="Blade root edgewise",
)
ax.legend(loc="upper right")


# %% Plot PSD.

fig_name = "anisotropic_only_wind_1_psd"
fig, ax = plt.subplots(num=fig_name, figsize=(6, 6 / 1.62), dpi=300)
#
ax.set_xlabel("Frequency [Hz]")
ax.set_ylabel("Moment [(kNm)²/Hz]")
ax.set_yscale("log")
ax.plot(
    df_psd.index.to_numpy(),
    df_psd.loc[:, "MomentMy Mbdy:blade1 nodenr:   1 coo: blade1"],
    label="Blade root edgewise",
)
ax.plot(
    df_psd.index.to_numpy(),
    df_psd.loc[:, "MomentMy Mbdy:tower nodenr:   1 coo: tower"],
    label="Tower bottom side-side",
)
ax.legend(loc="upper right")
ax.set_xlim(0.0, 2.0)

# Plot nP.
max_psd = np.maximum(
    df_psd.loc[:, "MomentMy Mbdy:tower nodenr:   1 coo: tower"].to_numpy(),
    df_psd.loc[:, "MomentMy Mbdy:blade1 nodenr:   1 coo: blade1"].to_numpy(),
)

i_1p = np.searchsorted(y_freq, rotor_speed)
i_np_all, _ = signal.find_peaks(np.log10(max_psd), distance=i_1p - 10)

# ax.scatter(y_freq[i_np_all], max_psd[i_np_all])

for n in range(1, 11):
    if n % 3 == 0:
        color = "C1"
    else:
        color = "C0"

    ax.annotate(
        f"{n}P",
        (y_freq[i_np_all[n]], max_psd[i_np_all[n]]),
        xytext=(y_freq[i_np_all[n]], max_psd[i_np_all[n]] * 100.0),
        arrowprops={"arrowstyle": "->", "color": color},
        horizontalalignment="center",
        verticalalignment="bottom",
        color=color,
        bbox={"boxstyle": "round", "facecolor": "white", "edgecolor": color},
    )

ax.set_ylim(1e-3, 1e15)
plt.tight_layout()
fig.savefig("./../figures/IEA10MW/hawc2_only_wind_np.svg", bbox_inches="tight")
