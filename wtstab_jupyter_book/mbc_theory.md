# Theory of Multi-Blade Coordinates

## Coleman transformation

The Coleman transformation was developed in {cite}`RPColeman_AMFeingold_1957` with the intent of transforming the degrees of freedom from the rotating to the ground-fixed (i.e. inertial) frame. $\vec{q}$ is transformed into a new set of coordinates called Multi-Blade Coordinates (MBC). A particularly good explanation can be found in {cite}`GBir_2008`. The Coleman transformation is similar to the direct-quadrature-zero transformation used in the analysis of three-phase synchronous machines.

The tower is already in the inertial frame, and therefore the Coleman transformation is simply the identity matrix. We will now write the Coleman transformation for the rotor and shaft.


### Application to the rotor

For a $B$-bladed rotor the azimuth of blade $b$ is
\begin{equation}
    \psi_b(t) = (b- 1) \frac{2\pi}{B} + \Omega t,
\end{equation}
where $\Omega$ is the rotor speed. The degrees of freedom of a rotor can be ordered as
\begin{equation}
    \vec{q}_R = (\vec{q}_1^\transpose,
                 \vec{q}_2^\transpose,
                 \dots,
                 \vec{q}_B^\transpose)^\transpose,
\end{equation}
where $\vec{q}_b \in \mathbb{R}^{N_b}$ are the degrees of freedom of blade $b$.

The direct Coleman transformation transform the degrees of freedom from the rotating frame $\mathcal{R}$ to the inertial frame $\mathcal{I}$. The expression for a rotor is reported in {cite}`PFSkjoldan_MHHansen_2009`)
```{math}
    \vec{q}_{R, \mathit{MBC}}
        = \mat{C}_R^{\mathcal{R} \rightarrow \mathcal{I}} \vec{q}_R,
```
where $\vec{q}_{R, \mathit{MBC}}$ is ordered as
\begin{equation}
    \vec{q}_{R, \mathit{MBC}} = (\vec{q}_a^\transpose,
                                 \vec{q}_{1c}^\transpose,
                                 \vec{q}_{1s}^\transpose,
                                 \vec{q}_{2c}^\transpose,
                                 \vec{q}_{2s}^\transpose,
                                 \dots,
                                 \vec{q}_{\tilde{B}c}^\transpose,
                                 \vec{q}_{\tilde{B}s}^\transpose,
                                 \vec{q}_{B/2}^\transpose)^\transpose,
\end{equation}
with $\tilde{B} = (B - 1) / 2$ for $B$ odd and $\tilde{B} = (B - 2) / 2$ for $B$ even.

The values of $\tilde{B}$ are reported in {numref}`tab:number_of_harmonics_in_isotropic_rotor`.

```{table} Value of $\tilde{B}$ for varying number of blades $B$.
:name: tab:number_of_harmonics_in_isotropic_rotor

|             |   |   |   |   |   |   |   |   |   |    |
|-------------|---|---|---|---|---|---|---|---|---|----|
| $B$         | 1 | 2 | 3 | 4 | 5 | 6 | 7 | 8 | 9 | 10 |
| $\tilde{B}$ | 0 | 0 | 1 | 1 | 2 | 2 | 3 | 3 | 4 | 4  |
```

The transformation is
```{math}
:label: eq:direct_coleman_b_blades
    \mat{C}_R^{\mathcal{R} \rightarrow \mathcal{I}}
        = \frac{2}{B} \begin{bmatrix}
			1/2                      &   1/2                      &   1/2                      &   \cdots   &   1/2                      \\ 
			\cos{          \psi_1}   &   \cos{          \psi_2}   &   \cos{          \psi_3}   &   \cdots   &   \cos{          \psi_B}   \\  
			\sin{          \psi_1}   &   \sin{          \psi_2}   &   \sin{          \psi_3}   &   \cdots   &   \sin{          \psi_B}   \\  
			\cos{2         \psi_1}   &   \cos{2         \psi_2}   &   \cos{2         \psi_3}   &   \cdots   &   \cos{2         \psi_B}   \\ 
			\sin{2         \psi_1}   &   \sin{2         \psi_2}   &   \sin{2         \psi_3}   &   \cdots   &   \sin{2         \psi_B}   \\
			\vdots                   &   \vdots                   &   \cdots                   &   \vdots   &   \vdots                   \\
			\cos{\tilde{B} \psi_1}   &   \cos{\tilde{B} \psi_2}   &   \cos{\tilde{B} \psi_3}   &   \cdots   &   \cos{\tilde{B} \psi_B}   \\  
			\sin{\tilde{B} \psi_1}   &   \sin{\tilde{B} \psi_2}   &   \sin{\tilde{B} \psi_3}   &   \cdots   &   \sin{\tilde{B} \psi_B}   \\
			-1/2                     &   1/2                      &   -1/2                     &   \cdots   &   (-1)^B/2
        \end{bmatrix} \otimes \mat{I}_{N_b}.
```
with $\otimes$ the Kronecker product. The inverse Coleman transformation provides
\begin{equation}
\begin{split}
    \vec{q} &= \mat{C}_R^{\mathcal{I} \rightarrow \mathcal{R}} \vec{q}_{R, \mathit{MBC}}          \\
            &= \mat{C}_R^{{\mathcal{R} \rightarrow \mathcal{I}}^{-1}} \vec{q}_{R, \mathit{MBC}}
\end{split}
\end{equation}
and is given by
```{math}
:label: eq:inverse_coleman_b_blades
    \mat{\mathcal{C}}_R^{\mathcal{I} \rightarrow \mathcal{R}} = \begin{bmatrix}
        1        &   \cos{\psi_1}   &   \sin{\psi_1}   &   \cdots   &   \cos{\tilde{B} \psi_1}   &   \sin{\tilde{B} \psi_1}   &            -  1   \\
        1        &   \cos{\psi_2}   &   \sin{\psi_2}   &   \cdots   &   \cos{\tilde{B} \psi_2}   &   \sin{\tilde{B} \psi_2}   &   \phantom{-} 1   \\
        1        &   \cos{\psi_3}   &   \sin{\psi_3}   &   \cdots   &   \cos{\tilde{B} \psi_3}   &   \sin{\tilde{B} \psi_3}   &            -  1   \\
        \vdots   &   \vdots         &   \vdots         &   \cdots   &   \vdots                   &   \vdots                   &   \vdots          \\
        1        &   \cos{\psi_B}   &   \sin{\psi_B}   &   \cdots   &   \cos{\tilde{B} \psi_B}   &   \sin{\tilde{B} \psi_B}   &   (-1)^B          \\
    \end{bmatrix} \otimes \mat{I}_{N_b}.
```

The most important case is for three-bladed rotors, for which the direct and inverse Coleman transformation are specialized to
```{math}
:label: eq:direct_coleman_3_blades
    \mat{C}_R^{\mathcal{R} \rightarrow \mathcal{I}}
        &= \frac{2}{3} \begin{bmatrix}
			1/2            &   1/2            &   1/2            \\ 
			\cos{\psi_1}   &   \cos{\psi_2}   &   \cos{\psi_3}   \\  
			\sin{\psi_1}   &   \sin{\psi_2}   &   \sin{\psi_3}   \\  
        \end{bmatrix} \otimes \mat{I}_{N_b},
```

```{math}
:label: eq:inverse_coleman_3_blades
   \mat{\mathcal{C}}_R^{\mathcal{I} \rightarrow \mathcal{R}}
        &= \begin{bmatrix}
            1   &   \cos{\psi_1}   &   \sin{\psi_1}   \\
            1   &   \cos{\psi_2}   &   \sin{\psi_2}   \\
            1   &   \cos{\psi_3}   &   \sin{\psi_3}   \\
    \end{bmatrix} \otimes \mat{I}_{N_b}.
```

MBC describe the motion of the rotor as a whole, and they have a clear physical meaning. Let us assume that $B=3$ and that only one among $q_a$, $q_c$ and $q_s$ is nonzero. By using the inverse Coleman transformation, we can see that if $q_b$ is a flapwise motion then:

- $q_a$ is the collective flap or coning;
- $q_c$ is the rotor tilt;
- $q_s$ is the rotor yaw.

If instead $q_b$ is an edgewise motion then:

- $q_a$ is the collective edge;
- $q_c$ is the rotor horizontal;
- $q_s$ is the rotor vertical.

This will be better understood in the next sections.


### Application to the shaft

The Coleman transformation for the shaft has been described in {cite}`MHHansen_2003`. 

The degrees of freedom of a shaft can be ordered as
\begin{equation}
    \vec{q}_S = (\vec{q}_v^\transpose,
                 \vec{q}_h^\transpose)^\transpose,
\end{equation}
where $\vec{q}_v \in \mathbb{R}^{N_s}$ represents the vertical shaft bending and $\vec{q}_h \in \mathbb{R}^{N_s}$ the horizontal shaft bending.

The direct Coleman transformation reads
```{math}
:label: eq:direct_coleman_transformation_shaft
    \vec{q}_{S, \mathit{MBC}}
        = \mat{\mathcal{C}}_S^{\mathcal{R} \rightarrow \mathcal{I}} \vec{q}_S,
```
where $\vec{q}_{S, \mathit{MBC}}$ is ordered as
\begin{equation}
    \vec{q}_{S, \mathit{MBC}} = (\vec{q}_V^\transpose,
                                 \vec{q}_H^\transpose)^\transpose.
\end{equation}
The direct Coleman transformation for the shaft is
\begin{equation}
    \mat{\mathcal{C}}_S^{\mathcal{R} \rightarrow \mathcal{I}}
        = \begin{bmatrix}
            \cos(\Omega t)   &            -  \sin(\Omega t)    \\
            \sin(\Omega t)   &   \phantom{-} \cos(\Omega t)
        \end{bmatrix} \otimes \mat{I}_{N_S}.
\end{equation}
and its inverse is
\begin{equation}
    \mat{\mathcal{C}}_S^{\mathcal{I} \rightarrow \mathcal{R}}
        = \begin{bmatrix}
            \phantom{-} \cos(\Omega t)   &   \sin(\Omega t)    \\
                     -  \sin(\Omega t)   &   \cos(\Omega t)
        \end{bmatrix} \otimes \mat{I}_{N_S}.
\end{equation}



### Complete turbine

The degrees of freedom for the full turbine can be ordered as
\begin{equation}
    \vec{q}_T = (\vec{q}_R^\transpose,
                 \vec{q}_S^\transpose,
                 \vec{q}_F^\transpose)^\transpose,
\end{equation}
where $\vec{q}_F \in \mathbb{R}^{N_f}$ contains the degrees of freedom for the tower and nacelle. The complete Coleman transformation is then a block diagonal matrix
```{math}
:label: eq:direct_coleman_transformation_turbine
    \mat{\mathcal{C}}_T^{\mathcal{R} \rightarrow \mathcal{I}}
        = \blkdiag(\mat{\mathcal{C}}_R^{\mathcal{R} \rightarrow \mathcal{I}},
                   \mat{\mathcal{C}}_S^{\mathcal{R} \rightarrow \mathcal{I}},
                   \mat{I}_{N_f}).
```

