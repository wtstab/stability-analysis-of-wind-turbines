# -*- coding: utf-8 -*-
"""
Draw a 3D rotor and animate it with 1 multi-blade coordinate at a time.

The computations are vectorized and in mixed precision.

@author: ricriv
"""


# %% Import.

import os

import numpy as np
import plotly.graph_objs as go
import plotly.io as pio

pio.renderers.default = "browser"


# %% Apply monkey patch to Plotly JSON encoder.

# This is needed to write floats with their correct precision, rather than always in double.
# From https://github.com/plotly/plotly.py/issues/3232

import importlib

mod_plty = importlib.import_module("_plotly_utils.utils", "plotly")


@staticmethod
def encode_as_list_patch(obj):
    """Attempt to use `tolist` method to convert to normal Python list."""
    if hasattr(obj, "tolist"):
        try:
            if (
                isinstance(obj, np.ndarray)
                and obj.dtype == np.float32
                or obj.dtype == np.float16
                and obj.flags.contiguous
            ):
                return [float("%s" % x) for x in obj]
        except AttributeError:
            raise mod_plty.NotEncodable

        return obj.tolist()
    else:
        raise mod_plty.NotEncodable


mod_plty.PlotlyJSONEncoder.encode_as_list = encode_as_list_patch


# %% Define bodies in body coordinates.
#    Use HAWC2 reference frame.

# Each body has 1 column per node.
# Each node is described by x, y, z.
# In the body frame the beam span is always along z.

tower_span = np.linspace(0.0, 1.5, 2, dtype=np.float32)
tower = np.zeros((3, tower_span.size), dtype=np.float32)
tower[2, :] = -tower_span

shaft_span = np.linspace(0.0, 0.1, 2, dtype=np.float32)
shaft_body = np.zeros((3, shaft_span.size), dtype=np.float32)
shaft_body[2, :] = shaft_span

blade_span = np.linspace(0.0, 1.0, 21, dtype=np.float32)
blade_1_body = np.zeros((3, blade_span.size), dtype=np.float32)
blade_1_body[2, :] = blade_span

blade_2_body = blade_1_body.copy()
blade_3_body = blade_1_body.copy()


# %% Basic 3D rotations.


def rot_x(x):
    s = np.sin(x)
    c = np.cos(x)
    # fmt: off
    return np.array([[1.0, 0.0, 0.0],
                     [0.0,   c,  -s],
                     [0.0,   s,   c]],
                    dtype=np.float32)
    # fmt: on


def rot_y(x):
    s = np.sin(x)
    c = np.cos(x)
    # fmt: off
    return np.array([[c,   0.0,   s],
                     [0.0, 1.0, 0.0],
                     [-s,  0.0,   c]],
                    dtype=np.float32)
    # fmt: on


def rot_z(x):
    s = np.sin(x)
    c = np.cos(x)
    # fmt: off
    return np.array([[c,    -s, 0.0],
                     [s,     c, 0.0],
                     [0.0, 0.0, 1.0]],
                    dtype=np.float32)
    # fmt: on


# %% Set up structure.

# Get the tower top.
base = tower[:, [-1]]

rot_base = rot_x(np.deg2rad(90))
shaft_global = np.float32(rot_base) @ shaft_body
shaft_global += base
base = shaft_global[:, [-1]]

# Create hub frame.
rot_base = rot_x(np.deg2rad(-90)) @ rot_base

# Create reference rotation matrices for blades.
rot_bld1_ref = rot_base.copy()
rot_bld1_ref = np.float32(rot_y(np.deg2rad(180)) @ rot_base)

rot_bld2_ref = rot_base.copy()
rot_bld2_ref = np.float32(rot_y(np.deg2rad(+60)) @ rot_base)

rot_bld3_ref = rot_base.copy()
rot_bld3_ref = np.float32(rot_y(np.deg2rad(-60)) @ rot_base)


# %% Set up cases to animate.

# Folder where the animations will be saved.
html_folder = "./../wtstab_jupyter_book/_static/mbc_animations"
os.makedirs(html_folder, exist_ok=True)

# Eye settings to set plane views.
eye_3d = {"x": -2.5, "y": -2.5, "z": +2.5}
eye_xz = {"x": 0.0, "y": -2.5, "z": 0.0}
eye_yz = {"x": -2.5, "y": 0.0, "z": 0.0}
eye_xy = {"x": 0.0, "y": 0.0, "z": -2.5}

# Amplitude of vibration (exaggerated on purpose).
amplitude = 0.3

# List of cases to plot.
cases = []

# Setup cases for operating turbine.

# Rotor speed [rpm].
rotor_speed_rpm = 5.0

# Rotor speed [rad/s].
rotor_speed = rotor_speed_rpm * np.pi / 30.0

# Number of periods to simulate [-].
n_periods = 1

# Period of rotation [s].
period = 2 * np.pi / rotor_speed

# Time step [s].
dt = period / 180

# Time array [s].
time = np.arange(0.0, period * n_periods, dt)

# Rotor azimuth for all time instants [rad].
azimuth = np.mod(rotor_speed * time, 2 * np.pi)

# Frequency of multi-blade coordinate vibration [rad/s].
frequency = rotor_speed * 2

cases.append(
    {
        "name": "operating_flap_ave",
        "imbc": 0,  # Average
        "ixy": 1,  # Flapwise
        "eye": eye_yz,
        "time": time,
        "azimuth": azimuth,
    }
)

cases.append(
    {
        "name": "operating_flap_cos",
        "imbc": 1,  # Cosine
        "ixy": 1,  # Flapwise
        "eye": eye_yz,
        "time": time,
        "azimuth": azimuth,
    }
)

cases.append(
    {
        "name": "operating_flap_sin",
        "imbc": 2,  # Sine
        "ixy": 1,  # Flapwise
        "eye": eye_xy,
        "time": time,
        "azimuth": azimuth,
    }
)

cases.append(
    {
        "name": "operating_edge_ave",
        "imbc": 0,  # Average
        "ixy": 0,  # Edgewise
        "eye": eye_xz,
        "time": time,
        "azimuth": azimuth,
    }
)

cases.append(
    {
        "name": "operating_edge_cos",
        "imbc": 1,  # Cosine
        "ixy": 0,  # Edgewise
        "eye": eye_xz,
        "time": time,
        "azimuth": azimuth,
    }
)

cases.append(
    {
        "name": "operating_edge_sin",
        "imbc": 2,  # Sine
        "ixy": 0,  # Edgewise
        "eye": eye_xz,
        "time": time,
        "azimuth": azimuth,
    }
)

# Setup cases for standing still turbine.

rotor_speed_rpm = 0.0
rotor_speed = 0.0
n_periods = 1
frequency = 2 * np.pi * 0.5
period = 2 * np.pi / frequency
dt = period / 100
time = np.arange(0.0, period * n_periods, dt)
azimuth = np.zeros((time.size))

cases.append(
    {
        "name": "standing_still_flap_ave",
        "imbc": 0,  # Average
        "ixy": 1,  # Flapwise
        "eye": eye_yz,
        "time": time,
        "azimuth": azimuth,
    }
)

cases.append(
    {
        "name": "standing_still_flap_cos",
        "imbc": 1,  # Cosine
        "ixy": 1,  # Flapwise
        "eye": eye_yz,
        "time": time,
        "azimuth": azimuth,
    }
)

cases.append(
    {
        "name": "standing_still_flap_sin",
        "imbc": 2,  # Sine
        "ixy": 1,  # Flapwise
        "eye": eye_xy,
        "time": time,
        "azimuth": azimuth,
    }
)

cases.append(
    {
        "name": "standing_still_edge_ave",
        "imbc": 0,  # Average
        "ixy": 0,  # Edgewise
        "eye": eye_xz,
        "time": time,
        "azimuth": azimuth,
    }
)

cases.append(
    {
        "name": "standing_still_edge_cos",
        "imbc": 1,  # Cosine
        "ixy": 0,  # Edgewise
        "eye": eye_xz,
        "time": time,
        "azimuth": azimuth,
    }
)

cases.append(
    {
        "name": "standing_still_edge_sin",
        "imbc": 2,  # Sine
        "ixy": 0,  # Edgewise
        "eye": eye_xz,
        "time": time,
        "azimuth": azimuth,
    }
)


# %% Compute blade motion for all time instants.

for case in cases:
    name = case["name"]
    imbc = case["imbc"]
    ixy = case["ixy"]
    eye = case["eye"]
    time = case["time"]
    azimuth = case["azimuth"]

    # Time is first dimension.

    # Rotation matrix for current azimuth angle. Rotate clockwise.
    # Same as rot_y() for arrays.
    s = np.sin(-azimuth)
    c = np.cos(-azimuth)
    rot_azi = np.zeros((time.size, 3, 3), dtype=np.float32)
    rot_azi[:, 0, 0] = c
    rot_azi[:, 0, 2] = s
    rot_azi[:, 1, 1] = 1.0
    rot_azi[:, 2, 0] = -s
    rot_azi[:, 2, 2] = c

    # Azimuth of all blades. Axes ordered as:
    #  - 1: time
    #  - 2: blade (1, 2, 3)
    psi = (
        -azimuth[:, np.newaxis]
        + np.array([np.pi, +np.pi / 3, -np.pi / 3])[np.newaxis, :]
    )

    # Motion in Multi-Blade Coordinates. The order is: average, cosine, sine.
    mbc = np.zeros((time.size, 3), dtype=np.float32)
    mbc[:, imbc] = np.sin(frequency * time)

    # Inverse Coleman transformation.
    invcoleman = np.ones((time.size, 3, 3), dtype=np.float32)
    invcoleman[:, :, 1] = np.cos(psi)
    invcoleman[:, :, 2] = np.sin(psi)

    # Convert MBC to blade coordinates. Axes ordered as:
    #  - 1: time
    #  - 2: blade (1, 2, 3)
    bld = (invcoleman @ mbc[:, :, np.newaxis])[:, :, 0]

    # Motion of the deformed blades in body coordinates. Axes ordered as:
    #  - 1: time
    #  - 2: direction (x, y, z)
    #  - 3: nodes (root to tip)
    # Copy blades initial condition to all time instants.
    blade_1_body_def = np.tile(blade_1_body, (time.size, 1, 1))
    blade_2_body_def = np.tile(blade_2_body, (time.size, 1, 1))
    blade_3_body_def = np.tile(blade_3_body, (time.size, 1, 1))

    # Deform blades in 1 direction.
    # The amplitude of vibration is quadratic with the span.
    blade_1_body_def[:, ixy, :] = (
        amplitude * bld[:, [0]] * blade_span[np.newaxis, :] ** 2
    )
    blade_2_body_def[:, ixy, :] = (
        amplitude * bld[:, [1]] * blade_span[np.newaxis, :] ** 2
    )
    blade_3_body_def[:, ixy, :] = (
        amplitude * bld[:, [2]] * blade_span[np.newaxis, :] ** 2
    )

    # Motion of the deformed blades in global coordinates. Axes ordered as:
    #  - 1: time
    #  - 2: direction (x, y, z)
    #  - 3: nodes (root to tip)
    blade_1_global = rot_azi @ rot_bld1_ref @ blade_1_body_def
    blade_2_global = rot_azi @ rot_bld2_ref @ blade_2_body_def
    blade_3_global = rot_azi @ rot_bld3_ref @ blade_3_body_def

    blade_1_global += base
    blade_2_global += base
    blade_3_global += base

    # Convert to half precision for smaller HTML files.
    blade_1_global = blade_1_global.astype(np.float16)
    blade_2_global = blade_2_global.astype(np.float16)
    blade_3_global = blade_3_global.astype(np.float16)

    # %% Plot.

    fig = go.Figure(
        data=(
            go.Scatter3d(
                x=tower[0, :],
                y=tower[1, :],
                z=tower[2, :],
                mode="lines",
                line={"color": "black", "width": 5},
                name="Tower",
            ),
            #
            go.Scatter3d(
                x=shaft_global[0, :],
                y=shaft_global[1, :],
                z=shaft_global[2, :],
                mode="lines",
                line={"color": "black", "width": 5},
                name="Shaft",
            ),
            #
            go.Scatter3d(
                x=blade_1_global[0, 0, :],
                y=blade_1_global[0, 1, :],
                z=blade_1_global[0, 2, :],
                mode="lines",
                line={"color": "#1f77b4", "width": 5},
                name="Blade 1",
            ),
            #
            go.Scatter3d(
                x=blade_2_global[0, 0, :],
                y=blade_2_global[0, 1, :],
                z=blade_2_global[0, 2, :],
                mode="lines",
                line={"color": "#ff7f0e", "width": 5},
                name="Blade 2",
            ),
            #
            go.Scatter3d(
                x=blade_3_global[0, 0, :],
                y=blade_3_global[0, 1, :],
                z=blade_3_global[0, 2, :],
                mode="lines",
                line={"color": "#2ca02c", "width": 5},
                name="Blade 3",
            ),
            # Add an invisible point to set the range for the z axis.
            go.Scatter3d(x=[0.0], y=[0.0], z=[-3.0], opacity=0.0, showlegend=False),
        ),
        frames=[
            go.Frame(
                data=(
                    go.Scatter3d(
                        x=tower[0, :],
                        y=tower[1, :],
                        z=tower[2, :],
                        mode="lines",
                        line={"color": "black", "width": 5},
                        name="Tower",
                    ),
                    #
                    go.Scatter3d(
                        x=shaft_global[0, :],
                        y=shaft_global[1, :],
                        z=shaft_global[2, :],
                        mode="lines",
                        line={"color": "black", "width": 5},
                        name="Shaft",
                    ),
                    #
                    go.Scatter3d(
                        x=blade_1_global[k, 0, :],
                        y=blade_1_global[k, 1, :],
                        z=blade_1_global[k, 2, :],
                        mode="lines",
                        line={"color": "#1f77b4", "width": 5},
                        name="Blade 1",
                    ),
                    #
                    go.Scatter3d(
                        x=blade_2_global[k, 0, :],
                        y=blade_2_global[k, 1, :],
                        z=blade_2_global[k, 2, :],
                        mode="lines",
                        line={"color": "#ff7f0e", "width": 5},
                        name="Blade 2",
                    ),
                    #
                    go.Scatter3d(
                        x=blade_3_global[k, 0, :],
                        y=blade_3_global[k, 1, :],
                        z=blade_3_global[k, 2, :],
                        mode="lines",
                        line={"color": "#2ca02c", "width": 5},
                        name="Blade 3",
                    ),
                )
            )
            for k in range(time.size)
        ],
        layout=go.Layout(
            scene={
                "xaxis": {
                    "dtick": 0.5,
                    "range": [-1.5, 1.5],
                    "autorange": False,
                    "showticklabels": False,
                    "title": {"text": ""},
                },
                "yaxis": {
                    "dtick": 0.5,
                    "range": [-1.5, 1.5],
                    "autorange": False,
                    "showticklabels": False,
                    "title": {"text": ""},
                },
                "zaxis": {
                    "dtick": 0.5,
                    "range": [-3.0, 0.0],
                    "autorange": "reversed",
                    "showticklabels": False,
                    "title": {"text": ""},
                },
                "aspectmode": "manual",
                "aspectratio": {"x": 1.0, "y": 1.0, "z": 1.0},
                "camera": {"projection": {"type": "orthographic"}, "eye": eye},
            },
            margin={"l": 0, "r": 0, "b": 0, "t": 0},
            autosize=False,
            width=1000,
            height=1000,
            showlegend=False,
            updatemenus=[
                {
                    "type": "buttons",
                    "buttons": [
                        # Add Play / Pause button.
                        {
                            "label": "⏯",
                            "method": "animate",
                            # Play
                            "args": [
                                None,
                                {
                                    "frame": {
                                        "duration": np.round(dt * 1000),
                                        "redraw": True,
                                        "transition": {
                                            "duration": 0,
                                            "easing": "linear",
                                        },
                                    }
                                },
                            ],
                            # Pause
                            "args2": [
                                [None],
                                {
                                    "frame": {
                                        "duration": 0,
                                        "redraw": False,
                                        "transition": {
                                            "duration": 0,
                                            "easing": "linear",
                                        },
                                    },
                                    "mode": "immediate",
                                },
                            ],
                        }
                    ],
                }
            ],
        ),
    )

    # fig.show()

    fig.write_html(
        file=f"{html_folder}/{name}.html",
        auto_play=False,
        include_plotlyjs=False,
        full_html=False,
    )
