# Application to an analytical turbine model

In this section, we will first explain how to apply the Coleman transformation, and then show an example with an analytical turbine model.


## Transformation of a linear mechanical system into Multi-Blade Coordinates

The transformation of a linear mechanical system into Multi-Blade Coordinates has been shown in {cite}`MHHansen_2003` for the constant rotor speed case, and in {cite}`GBir_2008` for the variable rotor speed one. Here we will focus on the former case.

Let us consider a linear mechanical system
\begin{equation}
      \mat{M}(t) \ddot{\vec{x}}
    + \mat{D}(t) \dot{\vec{x}}
    + \mat{K}(t) \vec{x}
    = \vec{f}(t),
\end{equation}
where $\vec{x} \in \mathbb{R}^n$ is the vector of the degrees of freedom, $\mat{M}(t)$ the mass matrix, $\mat{D}(t)$ the damping matrix, $\mat{K}(t)$ the stiffness matrix and $\vec{f}(t)$ the external forcing. We will now apply a generic time-varying, invertible, transformation
\begin{equation}
    \vec{x} = \mat{T}(t) \vec{z}.
\end{equation}
The first and second derivatives of this expression are
\begin{align}
    \dot{\vec{x}}
        &= \dot{\mat{T}}(t) \vec{z} + \mat{T}(t) \dot{\vec{z}},
    \\
    \ddot{\vec{x}}
        &= \ddot{\mat{T}}(t) \vec{z} + 2 \dot{\mat{T}}(t) \dot{\vec{z}} + \mat{T}(t) \ddot{\vec{z}}.
\end{align}
By substituting into the equation of motion we obtain
\begin{equation}
      \mat{M}(t) (\ddot{\mat{T}}(t) \vec{z} + 2 \dot{\mat{T}}(t) \dot{\vec{z}} + \mat{T}(t) \ddot{\vec{z}})
    + \mat{D}(t) (\dot{\mat{T}}(t) \vec{z} + \mat{T}(t) \dot{\vec{z}})
    + \mat{K}(t) \mat{T}(t) \vec{z}
    = \vec{f}(t),
\end{equation}
and by collecting $\vec{z}$ and its time derivatives 
\begin{multline}
      \mat{M}(t) \mat{T}(t) \ddot{\vec{z}}
    + (2\mat{M}(t) \dot{\mat{T}}(t)  + \mat{D}(t) \mat{T}(t) ) \dot{\vec{z}}
    \\
    + (\mat{M}(t) \ddot{\mat{T}}(t) + \mat{D}(t) \dot{\mat{T}}(t) +  \mat{K}(t) \mat{T}(t) ) \vec{z}
    = \vec{f}(t).
\end{multline}
The transformation is completed by left-multiplying the equation of motion by the inverse transformation matrix
\begin{multline}
      \mat{T}^{-1}(t) \mat{M}(t) \mat{T}(t) \ddot{\vec{z}}
    + \mat{T}^{-1}(t) (2\mat{M}(t) \dot{\mat{T}}(t)  + \mat{D}(t) \mat{T}(t) ) \dot{\vec{z}}
    \\
    + \mat{T}^{-1}(t) (\mat{M}(t) \ddot{\mat{T}}(t) + \mat{D}(t) \dot{\mat{T}}(t) +  \mat{K}(t) \mat{T}(t)) \vec{z}
    = \mat{T}^{-1}(t) \vec{f}(t).
\end{multline}
The mechanical system now reads
\begin{equation}
      \bar{\mat{M}} \ddot{\vec{z}}
    + \bar{\mat{D}} \dot{\vec{x}}
    + \bar{\mat{K}} \vec{x}
    = \bar{\vec{f}}(t),
\end{equation}
with
\begin{align}
    \bar{\mat{M}}
        &= \mat{T}^{-1}(t) \mat{M}(t) \mat{T}(t),
    \\
    \bar{\mat{D}}
        &= \mat{T}^{-1}(t) (2\mat{M}(t) \dot{\mat{T}}(t)  + \mat{D}(t) \mat{T}(t)),
    \\
    \bar{\mat{K}}
        &= \mat{T}^{-1}(t) (\mat{M}(t) \ddot{\mat{T}}(t) + \mat{D}(t) \dot{\mat{T}}(t) +  \mat{K}(t) \mat{T}(t)),
    \\
    \bar{\vec{f}}(t)
        &= \mat{T}^{-1}(t) \vec{f}(t).
\end{align}
When the transformation is the Coleman transformation, that is $\mat{T}(t) = \mat{\mathcal{C}}^{\mathcal{I} \rightarrow \mathcal{R}}(t)$, and the rotors have at least 3 blades, then the transformed matrices $\bar{\mat{M}}$, $\bar{\mat{D}}$ and $\bar{\mat{K}}$ are time-invariant. The transformed force vector $\bar{\vec{f}}(t)$ can still depend on time, but is azimuth-independent. The interested reader can consult {cite}`MHHansen_2003` for a slightly different derivation, which leverage some properties of the Coleman transformation.

