# -*- coding: utf-8 -*-
"""
Plots to show Lyapunov and asymptotic stability.
"""

import matplotlib.pyplot as plt
import numpy as np
from matplotlib.patches import Rectangle

plt.close("all")


# %% Lyapunov stability.

time = np.linspace(0.0, 20.0, 10001)

# y = np.sin(2*np.pi * 0.5 * time + np.pi/12) + 0.1 * np.sin(2*np.pi * 10.0 * time + np.pi/6)

omega0 = 2 * np.pi * 0.5
zeta = 0.05
y = (
    1.1
    * np.exp(-zeta * omega0 * time)
    * np.sin(np.sqrt(1.0 - zeta**2) * omega0 * time + np.pi / 12)
)

fig, ax = plt.subplots(figsize=[10, 7])
ax.set_xticks([])
ax.set_yticks([])
plt.axis("off")
ax.add_patch(Rectangle([-10, -1.2], time[-1] + 20.0, 2.4, alpha=1, color="#FFE5D4"))
ax.add_patch(Rectangle([-10, -0.5], time[-1] + 20.0, 1.0, alpha=1, color="#BFD3C1"))
ax.plot(time, y, linewidth=2.0, color="k")
ax.axhline(0.0, color="k", linestyle="--")
# ax.arrow(-1.0, -0.5, 0.0, 1.0, length_includes_head=True)
# ax.annotate('', xy=(-1.0, -0.5), xytext=(-1.0, +0.5),
#             arrowprops={'arrowstyle': '<->'}, va='center')

ax.set_xlim([-1.0, 30.0])
ax.set_ylim([-1.5, +1.5])

ax.set_position([0, 0, 1, 1])
plt.savefig("./lyapunov_stability.svg")


# %% Asymptotic stability.

time = np.linspace(0.0, 20.0, 10001)

# y = np.sin(2*np.pi * 0.5 * time + np.pi/12) + 0.1 * np.sin(2*np.pi * 10.0 * time + np.pi/6)

omega0 = 2 * np.pi * 0.5
zeta = 0.05
y = (
    1.1
    * np.exp(-zeta * omega0 * time)
    * np.sin(np.sqrt(1.0 - zeta**2) * omega0 * time + np.pi / 12)
)

fig, ax = plt.subplots(figsize=[10, 7])
ax.set_xticks([])
ax.set_yticks([])
plt.axis("off")
ax.add_patch(Rectangle([-10, -0.5], time[-1] + 20.0, 1.0, alpha=1, color="#BFD3C1"))
ax.plot(time, y, linewidth=2.0, color="k")
ax.axhline(0.0, color="k", linestyle="--")

ax.set_xlim([-1.0, 30.0])
ax.set_ylim([-1.5, +1.5])

ax.set_position([0, 0, 1, 1])
plt.savefig("./asymptotic_stability.svg")
