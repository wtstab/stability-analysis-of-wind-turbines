%{

Open animations from HAWCStab2 and trim them to 1 period.

%}


%% Initialization.

close all;
clear variables;
clc;


%% Load data.

% Folder that contains the videos.
input_folder_name = './../wtstab_jupyter_book/_static/hawcstab2_videos/IEA10MW/structural_5rpm_original';
% input_folder_name = './../wtstab_jupyter_book/_static/hawcstab2_videos/IEA10MW/structural_standing_still_original';

% Folder where the edited videos will be saved.
output_folder_name = './../wtstab_jupyter_book/_static/hawcstab2_videos/IEA10MW/structural_5rpm';
% output_folder_name = './../wtstab_jupyter_book/_static/hawcstab2_videos/IEA10MW/structural_standing_still';

% File name.
input_file_name = 'mode_3.mp4';

% Output file name.
output_file_path = [output_folder_name '/' input_file_name];

reader = VideoReader([input_folder_name '/' input_file_name]);

all_frames = read(reader);


%% Process data.

% Convert frames to grey scale.
h_small = size(all_frames, 1);
w_small = size(all_frames, 2);
all_frames_grey = zeros(size(all_frames, 1), size(all_frames, 2), reader.NumFrames, 'uint8');
for i = 1:reader.NumFrames
    all_frames_grey(:, :, i) = rgb2gray(all_frames(:, :, :, i));
end

% Compute the difference between the first frame and the others.
df = all_frames_grey(:, :, 1) - all_frames_grey;

% Compute 2-norm for each frame difference.
err = squeeze(sum(df .^ 2, [1 2])); 

% Look for minimum difference.
[~, k_frame_end] = min(err(10:end));
k_frame_end = k_frame_end + 9;

figure();
hold on;
plot(1:reader.NumFrames, err);
scatter(k_frame_end, err(k_frame_end));

return


%% Save.

k_frame_start = 1;
k_frame_end = 76;
frames_cut = all_frames(:, :, :, 1:k_frame_end);

% implay(frames_crop_cut, reader.FrameRate);

writer = VideoWriter(output_file_path, 'MPEG-4');
writer.FrameRate = reader.FrameRate;
writer.Quality = 100;

open(writer);
for k = 1:size(frames_cut, 4)
   writeVideo(writer, frames_cut(:, :, :, k));
end
close(writer);



