(chap:campbell_diagram)=
# Campbell diagram

A tool that is particularly useful to illustrate the stability analysis is the Campbell diagram, which shows the turbine frequencies as a function of the rotor or wind speed. Plotting as a function of the rotor speed is convenient for a structural stability analysis, while plotting with respect to the wind speed is done for an aeroelastic analysis, because otherwise the entire full load region would collapse on one point.

In this chapter we will show the Campbell diagram for the IEA 10 MW Reference Wind Turbine {cite}`PBortolotti_etal_2019_IEA`. The analysis is done using HAWCStab2 {cite}`MHHansen_2011`.

The structural Campbell diagram, in the undeflected configuration, is shown in {numref}`fig:IEA10MW_campbell_struc`. The diagram is shown in the ground-fixed frame, and the modes are named according to the most important component in the mode shape. A few comments are:
- The frequency of the tower Fore-Aft and Side-Side modes is practically constant.
- The mode shape of the tower Fore-Aft has a very strong collective flap component.
- The frequency of the backward whirl decreases at a rate of about $-\Omega$, while the one of the forward whirl increases at a rate of about $+\Omega$. When these two modes are observed in the rotating frame, their frequencies are basically constant.
- The flapwise modes are affected by the centrifugal stiffening, which increases their frequencies.
- Some modes change their most important component depending on the rotor speed.
- The collective flap is usually located between the flapwise whirling modes.
- The collective edge is not located between the edgewise whirling modes, but is usually at a higher frequency, which depends on the flexibility of the shaft.

:::{figure-md} fig:IEA10MW_campbell_struc
<img src="../figures/IEA10MW/campbell_struc.svg">

Structural Campbell diagram in the ground-fixed frame.
:::

The aeroelastic Campbell diagram, in the deflected configuration, is shown in {numref}`fig:IEA10MW_campbell_aeroelastic`, again in the ground-fixed frame. We can see how some modes have changed name, due to the different linearization point, and the introduction of the aerodynamic states. The trend of the whirling modes is also not apparent anymore.

:::{figure-md} fig:IEA10MW_campbell_aeroelastic
<img src="../figures/IEA10MW/campbell_aeroelastic.svg">

Aeroelastic frequencies diagram in the ground-fixed frame.
:::

In the aeroelastic case it is also interesting to plot the damping ratios, which is done in {numref}`fig:campbell_aeroelastic_damping`. We can easily see that the flapwise modes are highly-damped, while the edgewise modes are low-damped. The tower fore-aft is more damped than the tower side-side due to high flapwise components in the mode shape. The edgewise whirling modes tend to increase their damping in the partial load region, where the rotor speed increase. Their damping decreases instead in the full load region, where the pitch angle grows.

:::{figure-md} fig:campbell_aeroelastic_damping
<img src="../figures/IEA10MW/campbell_aeroelastic_damping.svg">

Aeroelastic damping ratios diagram in the ground-fixed frame.
:::
