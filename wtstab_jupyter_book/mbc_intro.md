# Multi-Blade Coordinates

In this chapter, we will explain the concept of Multi-Blade Coordinates (MBC), which is extremely useful to study both standing-still and operating turbines. We will start by defining MBC for the rotor and shaft, and then show an example to better understand their physical meaning.