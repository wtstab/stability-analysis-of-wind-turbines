# Stability analysis of wind turbines

by [Riccardo Riva](https://orbit.dtu.dk/en/persons/riccardo-riva)

![offshore_turbine_photo](../figures/offshore_turbine_photo.jpg)
