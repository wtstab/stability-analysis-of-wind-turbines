# -*- coding: utf-8 -*-
"""
Generate the sitemap, to be used by Google.
The working directory is one level above this script.

@author: ricriv
"""

# %% Import.

import yaml

# %% Load and parse website toc.

# Get the list of MarkDown files from the table of contents.
with open("./wtstab_jupyter_book/_toc.yml", "r") as fid:
    toc = yaml.safe_load(fid)

md_files = [chapter["file"] for chapter in toc["chapters"]]


# %% Make html paths.

# Website address.
address = "https://wtstab.pages.windenergy.dtu.dk/stability-analysis-of-wind-turbines/"

html_files = [address + md_file.replace(".md", ".html") for md_file in md_files]


# %% Write sitemap.

with open(
    "./wtstab_jupyter_book/_build/html/sitemap.txt", "w", encoding="utf-8"
) as fid:
    fid.write("\n".join(html_files))
