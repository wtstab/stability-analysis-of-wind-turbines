# -*- coding: utf-8 -*-
"""
Create a doublet signal, and compute its Power Spectral Density.

@author: ricriv
"""


# %% Import.

import sys

import matplotlib.pyplot as plt
import numpy as np
from scipy.signal import welch

# %% Set up.

plt.close("all")

# Figure properties.
plt.style.use("./rcparams.mplstyle")


# %% Doublet function.


def doublet(time, time_start, step_ramp, step_duration, amplitude=1, i_dir=0):
    """
    Generate a doublet.

    A doublet is a positive step immediately followed by a negative one.

    Parameters
    ----------
    time : array_like
        Time array, assumed to have a constant time step.
    time_start : float
        Start time of the doublet. The closest point in `time` will be selected.
    step_ramp : float
        Time between 0 and maximum amplitude.
    step_duration : float
        Duration of each step.
    amplitude : float
        Amplitude of the doublet.
    i_dir : int
        Index of the direction where the doublet is applied [0, 5].

    Returns
    -------
    y : array_like
        Doublet computed over `time`.
    """
    # Allocate output array.
    y = np.zeros_like(time)

    # Create doublets nodes.
    # fmt: off
    t_nodes = np.array([
        time_start,                                      # 0. Start.
        time_start + step_ramp,                          # 1. Ramp up and begin positive step.
        time_start + step_ramp + step_duration,          # 2. End positive step.
        time_start + 3 * step_ramp + step_duration,      # 3. Ramp down and begin negative step.
        time_start + 3 * step_ramp + 2 * step_duration,  # 4. End negative step.
        time_start + 4 * step_ramp + 2 * step_duration,  # 5. Return to 0.
        ])
    # fmt: on

    # Find start time.
    i_nodes = np.searchsorted(time, t_nodes) - 1

    # Slope of the ramp.
    slope = amplitude / step_ramp

    # Ramp up.
    y[i_nodes[0] : i_nodes[1] + 1] = slope * (
        time[i_nodes[0] : i_nodes[1] + 1] - time[i_nodes[0]]
    )
    # Positive step.
    y[i_nodes[1] : i_nodes[2] + 1] = y[i_nodes[1]]
    # Ramp down.
    y[i_nodes[2] : i_nodes[3] + 1] = (
        -slope * (time[i_nodes[2] : i_nodes[3] + 1] - time[i_nodes[2]]) + y[i_nodes[2]]
    )
    # Negative step.
    y[i_nodes[3] : i_nodes[4] + 1] = y[i_nodes[3]]
    # Return to 0.
    y[i_nodes[4] : i_nodes[5] + 1] = (
        slope * (time[i_nodes[4] : i_nodes[5] + 1] - time[i_nodes[4]]) + y[i_nodes[4]]
    )

    # Write input for force DLL.
    y_red = np.zeros((t_nodes.size + 2, 7))
    y_red[1 : t_nodes.size + 1, 0] = time[i_nodes]
    y_red[t_nodes.size + 1, 0] = 99999.0
    y_red[2:4, i_dir + 1] = amplitude
    y_red[4:6, i_dir + 1] = -amplitude
    print("")
    np.savetxt(sys.stdout, y_red, fmt="%+.6e", header=f"{y_red.shape[0]}", comments="")

    return y


# %% Application.

# Time array.
time = np.arange(0.0, 1000.0, 0.01)

# Doublet.
time_start = 300.01

# For tower top side-side.
y = doublet(
    time,
    time_start=time_start,
    step_ramp=0.04,
    step_duration=0.4,
    amplitude=1.0e5,
    i_dir=1,
)

# For shaft torsion.
# y = doublet(time,
#             time_start=time_start,
#             step_ramp=0.03,
#             step_duration=0.25,
#             amplitude=5.0e7,
#             i_dir=5)

# Power Spectral Density.
dt = np.mean(np.diff(time))
sampling_frequency = 1 / dt
frequency, psd = welch(
    y[:-1],
    fs=sampling_frequency,
    window=np.ones((y.shape[0] - 1)),
    noverlap=0,
    detrend=False,
)

# Define the target frequency.
# target_frequency = 0.84
target_frequency = 1.27


# %% Plot.

# Time series.
fig, ax = plt.subplots(num="Time series", dpi=300)
ax.set_xlabel("Time [s]")
ax.set_ylabel("Signal [N]")
ax.plot(time, y)
ax.set_xlim(time_start - 2.0, time_start + 3.0)

# Power Spectral Density.
fig, ax = plt.subplots(num="Power Spectral Density", dpi=300)
ax.set_xlabel("Frequency [Hz]")
ax.set_ylabel("Signal [N^2/Hz]")
ax.set_yscale("log")
ax.plot(frequency, psd)
ax.axvline(target_frequency, c="k", ls="--")
ax.set_xlim(0.0, 2.0)
lim = np.ceil(np.log10(np.max(psd)))
ax.set_ylim(10 ** (lim - 5), 10**lim)
