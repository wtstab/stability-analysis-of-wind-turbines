# -*- coding: utf-8 -*-
"""
Plot response Campbell diagram by reading HAWCStab2 results.

@author: ricriv
"""

# %% Import.

import matplotlib as mpl
import matplotlib.pyplot as plt
import numpy as np
from cycler import cycler
from routines_read_hs2 import read_hs2_output_amp_turbine, read_hs2_output_cmb

# %% Set up.

mpl.use("Agg")
# mpl.use('Qt5Agg')

# Use common style.
plt.style.use("rcparams.mplstyle")
# Get default colors.
colors = [
    "#1f77b4",
    "#ff7f0e",
    "#2ca02c",
    "#d62728",
    "#9467bd",
    "#8c564b",
    "#e377c2",
    "#7f7f7f",
    "#bcbd22",
    "#17becf",
]
# Loop over all colors before changing line style.
plt.rc(
    "axes",
    prop_cycle=cycler("linestyle", ["-", "--", ":", "-."]) * cycler("color", colors),
)

plt.close("all")


# %% Load data.

# Folder that contains HAWCStab2 results.
res_folder = "./../numerical_data/IEA10MW_local/res"

# Load the data.
cmb_struc = read_hs2_output_cmb(f"{res_folder}/turbine_stability_hs2_struc.cmb")
cmb_aeroelastic = read_hs2_output_cmb(f"{res_folder}/turbine_stability_hs2.cmb")
amp_struc = read_hs2_output_amp_turbine(f"{res_folder}/turbine_stability_hs2_struc.amp")
amp_aeroelastic = read_hs2_output_amp_turbine(f"{res_folder}/turbine_stability_hs2.amp")

mode_names_struc = (
    "Rotor rotation",
    "1ˢᵗ Tower Fore-Aft",
    "1ˢᵗ Tower Side-Side",
    "1ˢᵗ Flapwise Backward Whirl",
    "1ˢᵗ Flapwise Forward Whirl - Collective",
    "1ˢᵗ Flapwise Collective - Forward Whirl",
    "1ˢᵗ Edgewise Backward Whirl",
    "1ˢᵗ Edgewise Forward Whirl",
    "2ⁿᵈ Flapwise Backward Whirl",
    "2ⁿᵈ Flapwise Forward Whirl - Collective",
    "2ⁿᵈ Flapwise Collective - Forward Whirl",
    "1ˢᵗ Edgewise Collective",
    "3ʳᵈ Edgewise Backward Whirl",
    "3ʳᵈ Edgewise Forward Whirl",
)

mode_names_aeroelastic = (
    "Rotor rotation",
    "1ˢᵗ Tower Fore-Aft",
    "1ˢᵗ Tower Side-Side",
    "1ˢᵗ Flapwise Backward Whirl",
    "1ˢᵗ Flapwise Collective",
    "1ˢᵗ Flapwise Forward Whirl",
    "1ˢᵗ Edgewise Backward Whirl",
    "1ˢᵗ Edgewise Forward Whirl",
    "2ⁿᵈ Flapwise Backward Whirl",
    "2ⁿᵈ Flapwise Forward Whirl - Collective",
    "2ⁿᵈ Flapwise Collective - Forward Whirl",
    "1ˢᵗ Edgewise Collective",
    "3ʳᵈ Edgewise Backward Whirl",
    "3ʳᵈ Edgewise Forward Whirl",
)

# Rotor speed in rpm.
rotor_speed_cmb = cmb_struc.index * 30.0 / np.pi
rotor_speed_amp = amp_struc.index * 30.0 / np.pi

# Channels of the mode shapes.
channels = (
    "TWR x",
    "TWR y",
    "TWR yaw",
    "SFT x",
    "SFT y",
    "SFT tor",
    "Sym edge",
    "BW edge",
    "FW edge",
    "Sym flap",
    "BW flap",
    "FW flap",
    "Sym tors",
    "BW tors",
    "FW tors",
)


# %% Plot structural Campbell diagram.

fig_name = "iea10me_campbell_struc"
fig, ax = plt.subplots(num=fig_name, dpi=300, figsize=(8.5, 8))
ax.set_xlabel("Rotor speed [rpm]")
ax.set_ylabel("Damped frequency [Hz]")
for i_mode in range(1, len(mode_names_struc)):
    ax.plot(
        rotor_speed_cmb,
        cmb_struc.loc[:, ("Frequency [Hz]", i_mode)],
        label=mode_names_struc[i_mode],
    )
ax.set_ylim(0.0, 2.0)
ax.legend(bbox_to_anchor=(0, 1.02, 1, 0.2), loc="lower left", mode="expand", ncol=2)
fig.savefig("./../figures/IEA10MW/campbell_struc.svg", bbox_inches="tight")


# %% Plot aeroelastic Campbell diagram.

fig_name = "iea10mw_campbell_aeroelastic"
fig, ax = plt.subplots(num=fig_name, dpi=300, figsize=(8.5, 8))
ax.set_xlabel("Wind speed [m/s]")
ax.set_ylabel("Damped frequency [Hz]")
for i_mode in range(1, len(mode_names_aeroelastic)):
    ax.plot(
        cmb_aeroelastic.index,
        cmb_aeroelastic.loc[:, ("Frequency [Hz]", i_mode)],
        label=mode_names_aeroelastic[i_mode],
    )
ax.set_ylim(0.0, 2.0)
ax.legend(bbox_to_anchor=(0, 1.02, 1, 0.2), loc="lower left", mode="expand", ncol=2)
fig.savefig("./../figures/IEA10MW/campbell_aeroelastic.svg", bbox_inches="tight")


# %% Plot aeroelastic damping.

fig_name = "iea10mw_campbell_aeroelastic_damping"
fig, ax = plt.subplots(
    num=fig_name,
    nrows=2,
    sharex=True,
    height_ratios=[0.5, 1],
    dpi=300,
    figsize=(8.5, 8),
)
ax[1].set_xlabel("Wind speed [m/s]")
# ax[0].set_ylabel('Damping ratio [%]')
fig.supylabel("\n\nDamping ratio [%]")
for i_mode in range(1, len(mode_names_aeroelastic)):
    if np.mean(cmb_aeroelastic.loc[:, ("Damping [%]", i_mode)]) > 20.0:
        ax[0].plot(
            cmb_aeroelastic.index,
            cmb_aeroelastic.loc[:, ("Damping [%]", i_mode)],
            label=mode_names_aeroelastic[i_mode],
        )
        # Add an invisible point to ax[1] for correct line style.
        ax[1].plot(np.nan, np.nan, label=mode_names_aeroelastic[i_mode])
    else:
        ax[1].plot(
            cmb_aeroelastic.index,
            cmb_aeroelastic.loc[:, ("Damping [%]", i_mode)],
            label=mode_names_aeroelastic[i_mode],
        )
        # Add an invisible point to ax[0] for legend and correct line style.
        ax[0].plot(np.nan, np.nan, label=mode_names_aeroelastic[i_mode])
ax[0].set_ylim(15.0, 100.0)
ax[1].set_ylim(0.0, 12.0)
ax[0].legend(bbox_to_anchor=(0, 1.02, 1, 0.2), loc="lower left", mode="expand", ncol=2)
fig.savefig(
    "./../figures/IEA10MW/campbell_aeroelastic_damping.svg", bbox_inches="tight"
)


# %% Plot structural mode shapes.

if False:
    for i_mode in range(1, len(mode_names_struc)):
        fig_name = f"iea10mw_mode_shape_struc_{i_mode}"
        fig, ax = plt.subplots(num=fig_name, dpi=300)
        ax.set_title(mode_names_struc[i_mode])
        ax.set_xlabel("Rotor speed [rpm]")
        ax.set_ylabel("Magnitude [m, rad]")
        # Get mode shape magnitude for the current mode.
        data = amp_struc.loc[:, (i_mode,)].iloc[:, ::2]
        ax.plot(rotor_speed_amp, data.to_numpy(), label=channels)
        ax.legend()


# %% Plot aeroelastic mode shapes.

if False:
    for i_mode in range(1, len(mode_names_aeroelastic)):
        fig_name = f"iea10mw_mode_shape_aeroelastic_{i_mode}"
        fig, ax = plt.subplots(num=fig_name, dpi=300)
        ax.set_title(mode_names_aeroelastic[i_mode])
        ax.set_xlabel("Wind speed [m/s]")
        ax.set_ylabel("Magnitude [m, rad]")
        # Get mode shape magnitude for the current mode.
        data = amp_aeroelastic.loc[:, (i_mode,)].iloc[:, ::2]
        ax.plot(amp_aeroelastic.index, data.to_numpy(), label=channels)
        ax.legend()
