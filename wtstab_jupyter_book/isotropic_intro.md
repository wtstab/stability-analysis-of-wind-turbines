# Isotropic turbines

In this chapter, we will explain the theory of isotropic turbines, which is fundamental to understand the stability analysis.