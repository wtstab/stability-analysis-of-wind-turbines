# %%
"""
Double linked oscillator.
"""

# %%

import matplotlib as mpl
import matplotlib.colors as colors
import matplotlib.pyplot as plt
import numpy as np
import pandas as pd
from scipy import linalg, signal
from scipy.integrate import solve_ivp

mpl.use("Agg")
# mpl.use('Qt5Agg')

plt.style.use("rcparams.mplstyle")

plt.close("all")


# %% Model parameters.

m1 = 2.0
m2 = 5.0

c1 = 0.50
c2 = 0.10
c3 = 0.10

k1 = 20.0
k2 = 10.0
k3 = 5.0


# %% Construct the system matrices.

# Mass, damping and stiffness matrices.
mass_mat = np.diag([m1, m2])
# fmt: off
damping_mat = np.array([[c1 + c2, -c2     ],
                        [-c2,      c2 + c3]])
stiffness_mat = np.array([[k1 + k2, -k2     ],
                          [-k2,      k2 + k3]])
# fmt: on

# Invert the mass matrix.
# This is correct for a diagonal matrix, but in general we should use a factorisation.
inv_mass_mat = np.diag([1.0 / m1, 1.0 / m2])

# System A, B and C matrices.
a_mat = np.block(
    [
        [np.zeros((2, 2)), np.eye(2)],
        [-inv_mass_mat @ stiffness_mat, -inv_mass_mat @ damping_mat],
    ]
)
# fmt: off
b_mat = np.block([[np.zeros((2, 2))],
                  [inv_mass_mat]])
# fmt: on

c_mat = np.block([np.eye(2), np.zeros((2, 2))])

d_mat = np.zeros((2, 2))


# %% Solve the eigenvalue problem.

eig_val, eig_vec = linalg.eig(a_mat)

omega = np.abs(eig_val)
frequency = omega / (2 * np.pi)
damping_ratio = -np.real(eig_val) / omega
observed_mode_shape = c_mat @ eig_vec

# Normalize mode shapes.
factor = np.max(np.abs(observed_mode_shape), axis=0)
observed_mode_shape /= factor

observed_mode_shape = pd.DataFrame(
    data=observed_mode_shape,
    index=["x1", "x2"],
    columns=["Mode 1", "Mode 2", "Mode 3", "Mode 4"],
)

observed_mode_shape_magnitude = observed_mode_shape.abs()

observed_mode_shape_phase = pd.DataFrame(
    data=np.angle(observed_mode_shape, deg=True),
    index=["x1", "x2"],
    columns=["Mode 1", "Mode 2", "Mode 3", "Mode 4"],
)

# print(np.column_stack([frequency, damping_ratio*100.0]))

# Plot mode shapes.

fig, ax = plt.subplots(
    num="mode_shapes",
    nrows=2,
    sharex=True,
    dpi=300,
    # figsize=(25.78/2.54, 9.5/2.54),
)
observed_mode_shape_magnitude.plot.bar(rot=0, ax=ax[0])
observed_mode_shape_phase.plot.bar(rot=0, ax=ax[1])
ax[0].set_ylabel("Magnitude [m]")
ax[1].set_ylabel("Phase [deg]")
ax[1].get_legend().remove()
ax[0].set_axisbelow(True)
ax[1].set_axisbelow(True)
ax[1].set_ylim(-180.0, +180.0)
ax[1].set_yticks([-180.0, -90.0, 0.0, +90.0, +180.0])
# plt.tight_layout()
fig.savefig("./../figures/linked_oscillators/mode_shapes.svg", bbox_inches="tight")


# %% Simulate using mode shapes as initial condition.

tspan = [0.0, 1000.0]
t_eval = np.arange(tspan[0], tspan[-1], 0.01)

x0_0 = np.real(eig_vec[:, 0])
x0_2 = np.real(eig_vec[:, 2])


def motion(t, x):
    Dx = a_mat @ x
    return Dx


sol_0 = solve_ivp(
    motion, tspan, x0_0, t_eval=t_eval, atol=1e-8, rtol=1e-6, vectorized=True
)
sol_2 = solve_ivp(
    motion, tspan, x0_2, t_eval=t_eval, atol=1e-8, rtol=1e-6, vectorized=True
)

# Compute Power Spectral Density.

dt = np.mean(np.diff(t_eval))
sampling_frequency = 1 / dt
nyquist_frequency = sampling_frequency / 2
df = 1 / (t_eval[-1] - t_eval[0])


def compute_PSD(sol):
    window = np.ones(len(sol_0.t))
    noverlap = 0
    nperseg = len(window)
    nfft = nperseg
    detrend = "constant"
    return_onesided = True
    scaling = "density"

    y_freq, y_PSD = signal.welch(
        sol.y[0:2, :].T,
        sampling_frequency,
        window,
        nperseg,
        noverlap,
        nfft,
        detrend,
        return_onesided,
        scaling,
        axis=0,
    )
    y_PSD[0, :] = np.mean(sol.y[0:2, :].T, axis=0)

    y_PSD = pd.DataFrame(
        data=y_PSD,
        index=pd.Index(data=y_freq, name="Frequency [Hz]"),
        columns=["x1", "x2"],
    )

    return y_PSD


df_PSD_0 = compute_PSD(sol_0)
df_PSD_2 = compute_PSD(sol_2)

# Plot.

fig, ax = plt.subplots(
    num="motion_from_mode_shape_1",
    nrows=2,
    dpi=300,
)
ax[0].set_xlabel("Time [s]")
ax[1].set_xlabel("Frequency [Hz]")
ax[0].set_ylabel("Displacement [m]")
ax[1].set_ylabel("Displacement [m²/Hz]")
ax[0].set_xlim(0, 20)
ax[1].set_xlim(0, 1.5)
ax[1].set_ylim(1e-9, 1e-1)
ax[1].set_yscale("log")
ax[0].plot(sol_0.t, sol_0.y[0, :], label="$x_1$")
ax[0].plot(sol_0.t, sol_0.y[1, :], label="$x_2$")
ax[1].plot(df_PSD_0.index, df_PSD_0["x1"], label="$x_1$")
ax[1].plot(df_PSD_0.index, df_PSD_0["x2"], label="$x_2$")
ax[0].legend(loc="upper right")
plt.tight_layout()
fig.savefig(
    "./../figures/linked_oscillators/motion_from_mode_shape_1.svg", bbox_inches="tight"
)

fig, ax = plt.subplots(
    num="motion_from_mode_shape_3",
    nrows=2,
    dpi=300,
)
ax[0].set_xlabel("Time [s]")
ax[1].set_xlabel("Frequency [Hz]")
ax[0].set_ylabel("Displacement [m]")
ax[1].set_ylabel("Displacement [m²/Hz]")
ax[0].set_xlim(0, 20)
ax[1].set_xlim(0, 1.5)
ax[1].set_ylim(1e-9, 1e0)
ax[1].set_yscale("log")
ax[0].plot(sol_2.t, sol_2.y[0, :], label="$x_1$")
ax[0].plot(sol_2.t, sol_2.y[1, :], label="$x_2$")
ax[1].plot(df_PSD_2.index, df_PSD_2["x1"], label="$x_1$")
ax[1].plot(df_PSD_2.index, df_PSD_2["x2"], label="$x_2$")
ax[0].legend(loc="upper right")
plt.tight_layout()
fig.savefig(
    "./../figures/linked_oscillators/motion_from_mode_shape_3.svg", bbox_inches="tight"
)


# %% Modal superposition.

# Arbitrary initial conditions.
x0_r = np.array([0.2, -0.3, -0.1, +0.5])

# Numerical solution.
sol_r = solve_ivp(motion, tspan, x0_r, t_eval=t_eval, vectorized=True)

# Modal scale factors.
rho = linalg.solve(eig_vec, x0_r)

# Modal superposition.

# Array with the modal contributions.
# Dimensions:
# 0: time
# 1: states (i.e. degrees of freedom)
# 2: modes
# sol_m = np.full((len(sol_r.t), eig_vec.shape[0], eig_vec.shape[1]), np.nan, dtype=np.complex128)
# Loop over the modes.
# for i in range(eig_vec.shape[1]):
#    # Modal response for each mode.
#    sol_m[:, :, i] = eig_vec[:, i].reshape(1, -1) * rho[i] * np.exp(eig_val[i] * sol_r.t.reshape(-1, 1))
# Sum over the modes.
# sol_m_all = np.real(sol_m.sum(axis=2))
# Vectorized solution.
sol_m_all = np.real(
    np.sum(eig_vec * rho * np.exp(eig_val * sol_r.t.reshape(-1, 1, 1)), axis=2)
)

# Plot.
fig, ax = plt.subplots(dpi=300, num="Test modal decomposition")
ax.set_xlabel("Time [s]")
ax.set_ylabel("Displacement [m]")
ax.plot(sol_r.t, sol_r.y[0, :], "-", label="$x_1$ ODE")
ax.plot(sol_r.t, sol_m_all[:, 0], "--", label="$x_1$ modal")
ax.plot(sol_r.t, sol_r.y[1, :], "-", label="$x_2$ ODE")
ax.plot(sol_r.t, sol_m_all[:, 1], "--", label="$x_2$ modal")
ax.legend()
ax.set_xlim([0, 20])
ax.set_ylim([-0.4, 0.5])
fig.savefig(
    "./../figures/linked_oscillators/check_modal_decomposition.svg", bbox_inches="tight"
)


df_PSD_r = compute_PSD(sol_r)
fig, ax = plt.subplots(dpi=300, num="Two modes PSD")
ax.set_xlabel("Frequency [Hz]")
ax.set_ylabel("Displacement [m^2/Hz]")
ax.grid()
ax.plot(df_PSD_r.index, df_PSD_r["x1"], label="x1")
ax.set_yscale("log")
ax.set_xlim(0.0, 1.0)
ax.set_ylim(1e-6, None)
fig.savefig("./../figures/linked_oscillators/two_modes_psd.svg", bbox_inches="tight")


# %% Custom chirp function.
#    Adapted from https://dsp.stackexchange.com/a/75305/10782


def lchirp(time, fmin=0.0, fmax=1.0, zero_phase_tmin=True, cos=True):
    a = (fmin - fmax) / (time[0] - time[-1])
    b = (fmin * time[-1] - fmax * time[0]) / (time[-1] - time[0])
    phi = (a / 2) * (time**2 - time[0] ** 2) + b * (time - time[0])
    phi *= 2 * np.pi
    if zero_phase_tmin:
        phi *= (phi[-1] - phi[-1] % (2 * np.pi)) / phi[-1]
    else:
        phi -= phi[-1] % (2 * np.pi)
    if cos:
        return np.cos(phi)
    else:
        return np.sin(phi)


# %% Define the input.

# Time array.
time = np.linspace(0.0, 1000.0, 100001)

# Frequency properties.
dt = np.mean(np.diff(time))
sampling_frequency = 1 / dt
nyquist_frequency = sampling_frequency / 2
frequency_resolution = 1.0 / time[-1]

# We use a custom chirp signal.
input = np.column_stack(
    (
        -0.8 * lchirp(time, fmin=0.0, fmax=1.0, zero_phase_tmin=True, cos=False),
        +1.2 * lchirp(time, fmin=0.0, fmax=1.0, zero_phase_tmin=True, cos=False),
    )
)

# Compute input PSD.
window = np.ones(time.size)
noverlap = 0
nperseg = len(window)
nfft = nperseg
detrend = "constant"
return_onesided = True
scaling = "density"

frequency, input_psd = signal.welch(
    input,
    sampling_frequency,
    window,
    nperseg,
    noverlap,
    nfft,
    detrend,
    return_onesided,
    scaling,
    axis=0,
)

# Plot time history and PSD.
fig, ax = plt.subplots(nrows=2, dpi=300, num="Input time history and PSD")
ax[0].set_xlabel("Time [s]")
ax[0].set_ylabel("Input 1 [N]")
ax[1].set_xlabel("Frequency [Hz]")
ax[1].set_ylabel("Input 1 [N²/Hz]")
ax[1].set_yscale("log")
ax[0].plot(time, input[:, 0])
ax[1].plot(frequency, input_psd[:, 0])
ax[0].set_xlim(0.0, 100.0)
ax[1].set_xlim(0.0, 1.2)
ax[1].set_ylim(1e-5, 1e1)
plt.tight_layout()
fig.savefig("./../figures/linked_oscillators/input_chirp.svg", bbox_inches="tight")


# %% Simulate state space system in continuous time.

# Continuous time state space model.
sys_c = signal.StateSpace(a_mat, b_mat, c_mat, d_mat)

# Simulate.
x0 = np.zeros((4))
_, y_c, _ = signal.lsim(sys_c, input, time, x0)


# %% Spectral analysis.

# Compute output PSD.
window = np.ones(time.size)
# window = signal.get_window('hann', time.size, fftbins=True)
nperseg = len(window)
noverlap = 0
nfft = nperseg
detrend = "constant"
return_onesided = True
scaling = "density"

_, y_c_psd = signal.welch(
    y_c,
    sampling_frequency,
    window,
    nperseg,
    noverlap,
    nfft,
    detrend,
    return_onesided,
    scaling,
    axis=0,
)


# Plot time history and PSD.
fig, ax = plt.subplots(nrows=2, dpi=300, num="Output time history and PSD")
ax[0].set_xlabel("Time [s]")
ax[0].set_ylabel("Output [m]")
ax[1].set_xlabel("Frequency [Hz]")
ax[1].set_ylabel("Output 1 [m²/Hz]")
ax[1].set_yscale("log")
ax[0].plot(time, y_c[:, 0])
ax[1].plot(frequency, y_c_psd[:, 0])
# ax[0].set_xlim(0.0, 50.0)
ax[1].set_xlim(0.0, 1.2)
ax[1].set_ylim(1e-6, 1e0)
plt.tight_layout()
fig.savefig("./../figures/linked_oscillators/output_chirp.svg", bbox_inches="tight")

# Compute Short Time FFT.
# Long windows increase frequency resolution.
# High window overlap increases time resolution.
window = signal.get_window("hann", 2**15, fftbins=True)
nperseg = len(window)
noverlap = round(window.size / 1.1)
nfft = nperseg
detrend = "constant"
return_onesided = True
boundary = "zeros"
padded = True
scaling = "psd"

f_stft, t_stft, z_stft = signal.stft(
    y_c[:, 0],
    sampling_frequency,
    window,
    nperseg,
    noverlap,
    nfft,
    detrend,
    return_onesided,
    boundary,
    padded,
    0,
    scaling,
)
z_stft = np.abs(z_stft)

# Plot STFT.
fig, ax = plt.subplots(dpi=300, num="Output STFT")
ax.grid(False)
ax.pcolor(
    t_stft,
    f_stft,
    z_stft,
    shading="auto",
    norm=colors.LogNorm(vmin=1e-5, vmax=z_stft.max()),
    rasterized=True,
)
plt.ylabel("Frequency [Hz]")
plt.xlabel("Time [s]")
ax.set_xlim(time[0], time[-1])
ax.set_ylim(0.0, 1.2)
fig.savefig("./../figures/linked_oscillators/output_stft.svg", bbox_inches="tight")


# %% Compute Frequency Response Function.

# Maximum frequency for transfer function [Hz].
# Must be lower or equal than input/output Nyquist frequency.
# frequency_max_tf = 1.5

# Transfer function frequency array [Hz].
# The frequency resolution of the FRF must be equal to the one of the time series.
# frequency_frf = np.arange(0.0, frequency_max_tf, frequency_resolution)
frequency_frf = np.arange(
    0.0, nyquist_frequency + frequency_resolution, frequency_resolution
)

# Frequency resolution [Hz].
# Must bet the same for transfer function and input/output.
df = np.mean(np.diff(frequency_frf))

# Compute transfer function.
nx = a_mat.shape[0]
ny = c_mat.shape[0]
nu = b_mat.shape[1]

# Compute Frequency Response Function.
g_mat = np.zeros((frequency_frf.size, ny, nu), np.complex128)
iw = np.eye(nx, dtype=np.complex128)
for k in range(frequency_frf.size):
    np.fill_diagonal(iw, 1j * 2 * np.pi * frequency_frf[k])
    fac = linalg.solve(iw - a_mat, b_mat)
    g_mat[k, :, :] = c_mat @ fac + d_mat

# Compute modulus and phase.
g_mat_abs = np.abs(g_mat)
g_mat_phase = np.angle(g_mat)

# Plot Bode diagram.
fig, ax = plt.subplots(nrows=2, sharex=True, dpi=300, num="Bode diagram")
ax[1].set_xlabel("Frequency [Hz]")
ax[0].set_ylabel("Magnitude")
ax[1].set_ylabel("Phase [deg]")
for i in range(ny):
    for j in range(nu):
        ax[0].plot(frequency_frf, g_mat_abs[:, i, j], label=f"G({i+1}, {j+1})")
        ax[1].plot(
            frequency_frf,
            g_mat_phase[:, i, j] * 180.0 / np.pi,
            label=f"G({i+1}, {j+1})",
        )
ax[0].legend(loc="upper right")
ax[0].set_yscale("log")
ax[0].set_xlim(0.0, 1.2)
ax[0].set_ylim(1e-4, 1e1)
ax[1].set_ylim(-190, +190)
ax[1].set_yticks(np.arange(-180, +181, 90))
fig.savefig("./../figures/linked_oscillators/bode.svg", bbox_inches="tight")


# %% Evaluate response with FRF.

# Compute FFT of the input.
# Real input, and therefore symmetry of the FFT is assumed.
input_fft = np.fft.rfft(input, axis=0)

# Compute the low-frequency part of the output spectrum.
y_fft = (g_mat @ input_fft.reshape(-1, nu, 1))[:, :, 0]

# Same as:
# y_fft = np.zeros((frequency_frf.size, ny), np.complex128)
# for k in range(frequency_frf.size):
#     y_fft[k, :] = g_mat[k, :, :] @ input_fft[k, :]

# Convert output to time domain.
y_frf = np.fft.irfft(y_fft, axis=0)

# Plot time history.
fig, ax = plt.subplots(dpi=300, num="Output time history from ODE and FRF")
ax.set_xlabel("Time [s]")
ax.set_ylabel("Displacement [m]")
ax.set_xlim(300.0, 350.0)
ax.set_ylim(-0.6, +0.6)
for i in range(ny):
    ax.plot(time, y_c[:, i], "-", label=f"$x_{i+1}$ from ODE")
    ax.plot(time[:-1], y_frf[:, i], "--", label=f"$x_{i+1}$ from FRF")
ax.legend()


# fig, ax = plt.subplots(nrows=2, sharex=True, dpi=300, num='Output time history from ODE and FRF')
# ax[1].set_xlabel('Time [s]')
# for i in range(ny):
#     ax[i].set_ylabel(f'Output {i+1} [N]')
#     ax[i].plot(time, y_c[:, i], '-', label='From ODE')
#     ax[i].plot(time[:-1], y_frf[:, i], '--', label='From FRF')
# ax[0].legend()
# ax[0].set_xlim(200.0, 320.0)
plt.tight_layout()
fig.savefig(
    "./../figures/linked_oscillators/output_ode_vs_frf.svg", bbox_inches="tight"
)
