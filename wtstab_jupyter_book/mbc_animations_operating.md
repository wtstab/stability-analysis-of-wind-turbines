# Animations of Multi-Blade Coordinates: the operating case

<script src="https://cdn.plot.ly/plotly-2.14.0.min.js"></script>
<noscript>This browser does not support JavaScript, therefore the animations will not be shown.</noscript>

These plots are interactive. Please click on the Play/Pause button, or use the mouse to rotate the view.


## Flap average: collective flap or coning

```{raw} html
:file: _static/mbc_animations/operating_flap_ave.html
```

## Flap cos: rotor tilt

```{raw} html
:file: _static/mbc_animations/operating_flap_cos.html
```

## Flap sin: rotor yaw

```{raw} html
:file: _static/mbc_animations/operating_flap_sin.html
```

## Edge average: collective edge

```{raw} html
:file: _static/mbc_animations/operating_edge_ave.html
```

## Edge cos: rotor horizontal

```{raw} html
:file: _static/mbc_animations/operating_edge_cos.html
```

## Edge sin: rotor vertical

```{raw} html
:file: _static/mbc_animations/operating_edge_sin.html
```
