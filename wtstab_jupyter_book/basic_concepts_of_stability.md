(chap:basic_concepts_of_stability)=
# Basic concepts of stability

In this chapter we will review some basic concepts of stability.

(sec:stability_of_nonlinear_systems)=
## Stability of nonlinear systems.

Given an autonomous (i.e. non-forced) nonlinear dynamical system
\begin{equation}
    \dot{\vec{q}}(t) = \vec{f}(\vec{q}(t)),
    %
    \quad
    %
    \vec{q}(0) = \vec{q}_0,
\end{equation}
with $\vec{q}$ the vector of degrees of freedom at time $t$, suppose that the system has an equilibrium point $\bar{\vec{q}}$, then this equilibrium is:

```{prf:definition} Lyapunov stable
:label: Lyapunov_stable
:nonumber:

$$
\forall \epsilon > 0, \exists \delta > 0: \text{if } \norm{\vec{q}_0 - \bar{\vec{q}}} < \delta \text{ then } \norm{\vec{q}(t) - \bar{\vec{q}}} < \epsilon \quad \forall t \ge 0.
$$
```

This definition means that if the response starts sufficiently close to an equilibrium point, it will always remain close to it. Since the system is not forced, the only characteristic that can drive the response is indeed its stability. For example, if a system starts from $\vec{q}_0$ and moves further and further from the equilibrium point $\bar{\vec{q}}$, it will not be Lyapunov stable. On the other hand, a system that is Lyapunov stable will always remain close to $\bar{\vec{q}}$, possibly without ever reaching it. This case is better described by a second definition.

```{prf:definition} Asymptotically stable
:label: Asymptotically_stable
:nonumber:

If it is Lyapunov stable and

$$
\exists \delta > 0: \text{if } \norm{\vec{q}_0 - \bar{\vec{q}}} < \delta \text{ then } \lim_{t \to \infty} \norm{\vec{q}(t) - \bar{\vec{q}}} = 0.
$$
```

This definition means that if the response starts sufficiently close to an equilibrium point, then it will (almost) reach it in the future.

Although general, these two definitions quantify the stability only through the initial and final distance from the equilibrium point, excluding the time information. To properly quantify the stability we will assume that the system is either Linear Time-Invariant (LTI) or Linear Time-Periodic (LTP).

(sec:Stability_analysis_of_LTI_systems)=
## Stability analysis of Linear Time-Invariant systems

In this section we will review the stability of time-invariant systems. The explanation is based on {cite}`DRowell_2004`.

A nonlinear time-invariant system can be modeled as
\begin{align}
    \dot{\vec{q}}(t) &= \vec{f}(\vec{q}(t), \vec{w}(t)),
    %
    \quad
    %
    \vec{q}(0) = \vec{q}_0,
    %
    \\
    \vec{z}(t) &= \vec{g}(\vec{q}(t), \vec{w}(t)),
\end{align}
with $\vec{w}(t)$ the input and $\vec{z}(t)$ the output. If the input has a constant value $\bar{\vec{w}}$, the system an equilibrium point $\bar{\vec{q}}$, and output $\bar{\vec{z}}$, then we can linearize the system in the
neighborhood of $\{\bar{\vec{q}}, \bar{\vec{w}}\}$. The resulting LTI system is
```{math}
:label: eq:lti_sys
    \dot{\vec{x}}(t) &= \mat{A} \vec{x}(t) + \mat{B} \vec{u}(t),
    %
    \\
    %
    \vec{y}(t) &= \mat{C} \vec{x}(t) + \mat{D} \vec{u}(t),
```
with
\begin{align}
    \vec{x}(t) &= \vec{q}(t) - \bar{\vec{q}},
    \\
    \vec{u}(t) &= \vec{w}(t) - \bar{\vec{w}},
    \\
    \vec{y}(t) &= \vec{z}(t) - \bar{\vec{z}}.
\end{align}
$\vec{x}(t) \in \mathbb{R}^{N_x}$ is the state, $\vec{u}(t) \in \mathbb{R}^{N_u}$ the input and $\vec{y}(t) \in \mathbb{R}^{N_y}$ the output. The system matrices are
given by
\begin{align}
    \mat{A} &= \left. \vec{\nabla}_{\vec{q}} \vec{f} \right|_{\bar{\vec{q}}, \bar{\vec{w}}},
    &
    \mat{B} &= \left. \vec{\nabla}_{\vec{w}} \vec{f} \right|_{\bar{\vec{q}}, \bar{\vec{w}}},
    \\
    \mat{C} &= \left. \vec{\nabla}_{\vec{q}} \vec{g} \right|_{\bar{\vec{q}}, \bar{\vec{w}}},
    &
    \mat{D} &= \left. \vec{\nabla}_{\vec{w}} \vec{g} \right|_{\bar{\vec{q}}, \bar{\vec{w}}},
\end{align}
and have compatible dimensions.

Given an autonomous linear time-varying system
```{math}
:label: eq:ltv_autonomous_sys
    \dot{\vec{x}}(t) = \mat{A}(t) \vec{x}(t),
    %
    \quad
    %
    \vec{x}(0) = \vec{x}_0,
```
The transition matrix $\mat{\Phi}(t; \tau)$ provides
\begin{equation}
    \vec{x}(t) = \mat{\Phi}(t; \tau) \vec{x}(\tau),
\end{equation}
and obeys the matrix ODE
\begin{equation}
    \frac{\partial \mat{\Phi}(t; \tau)}{\partial t} = \mat{A}(t) \mat{\Phi}(t; \tau),
    %
    \quad
    %
    \mat{\Phi}(\tau; \tau) = \mat{I}.
\end{equation}
The state transition matrix determines the motion, and hence the stability, of the system. If system {eq}`eq:ltv_autonomous_sys` is also time-invariant then
```{math}
:label: eq:lti_state_transition_matrix
    \mat{\Phi}(t; \tau) = e^{\mat{A} (t-\tau)},
```
where $e^{\mat{A}}$ denotes the matrix exponential (see for example {cite:ps}`CMoler_CVanLoan_2003`).

If $\mat{A}$ is diagonalizable then
```{math}
:label: eq:eigendecomposition_of_a
    \mat{A} = \mat{\Psi} \mat{\Lambda} \mat{\Psi}^{-1},
```
with $\mat{\Lambda} = \diag(\dots, \lambda_j, \dots)$ and $\mat{\Psi} = [\cdots | \vec{\psi}_j | \cdots]$, for $j \in [1, N_x]$. $\lambda_j$ are named eigenvalues and $\vec{\psi}_j$ eigenvectors or mode shapes. By substituting {eq}`eq:eigendecomposition_of_a` into {eq}`eq:lti_state_transition_matrix` we get
\begin{gather}
    \mat{\Phi}(t; \tau) = e^{\mat{\Psi} \mat{\Lambda} \mat{\Psi}^{-1} (t-\tau)},
    \\
    \mat{\Phi}(t; \tau) = \mat{\Psi} e^{\mat{\Lambda} (t-\tau)} \mat{\Psi}^{-1},
    \\
    \mat{\Phi}(t; \tau) = \mat{\Psi} \diag\left(\dots, e^{\lambda_j (t-\tau)}, \dots \right) \mat{\Psi}^{-1}.
\end{gather}
We write $\mat{\Psi}^{-1}$ as
\begin{equation}
    \mat{\Psi}^{-1} = \begin{bmatrix}
        \vdots   \\
        \vec{\varrho}_j^\transpose   \\
        \vdots
    \end{bmatrix},
\end{equation}
where each $\vec{\varrho}_j^\transpose$ represents a different row. The modal decomposition of the state transition matrix is then
\begin{equation}
    \mat{\Phi}(t; \tau)
        = \sum_{j=1}^{N_x} \vec{\psi}_j \vec{\varrho}_j^\transpose e^{\lambda_j (t-\tau)}.
\end{equation}
Since $\vec{\varrho}_j^\transpose \vec{x}(\tau) = r_j$, the modal decomposition of the state is
```{math}
:label: eq:lti_modal_decomposition_of_state
    \vec{x}(t)
        = \sum_{j=1}^{N_x} \vec{\psi}_j r_j e^{\lambda_j (t-\tau)}.
```
Comments:
- If $\Re(\lambda_j) < 0 \: \forall j \in [1, N_x]$ then the system is asymptotically stable.

- If $\exists \lambda_j: \Re(\lambda_j) = 0$, then the system is simply stable.

- If $\exists \lambda_j: \Re(\lambda_j) > 0$, then the system is unstable.

- The scalars $r_j$ depend on the initial conditions and the arbitrary magnitude of $\vec{\psi}_j$.

The part of the output due to the free response is
\begin{equation}
    \vec{y}(t) = \mat{C} \mat{\Phi}(t; \tau) \vec{x}(\tau),
\end{equation}
which allows to define the observed state transition matrix $\mat{\Phi}^{\vec{y}}(t; \tau) = \mat{C} \mat{\Phi}(t; \tau) \in \mathbb{R}^{N_y \times N_x}$. The modal decomposition of $\mat{\Phi}^{\vec{y}}(t; \tau)$ is
\begin{equation}
\begin{split}
    \mat{\Phi}^{\vec{y}}(t; \tau)
        &= \sum_{j=1}^{N_x} \mat{C} \vec{\psi}_j \vec{\varrho}_j^\transpose e^{\lambda_j (t-\tau)}
        \\
        &= \sum_{j=1}^{N_x} \vec{\psi}_j^{\vec{y}} \vec{\varrho}_j^\transpose e^{\lambda_j (t-\tau)},
\end{split}
\end{equation}
where we have defined the observed mode shape $\vec{\psi}_j^{\vec{y}} = \mat{C} \vec{\psi}_j \in \mathbb{R}^{N_y}$. Using the observed state transition matrix, the modal decomposition of the output reads
\begin{equation}
\label{eq:lti_modal_decomposition_of_output}
    \vec{y}(t) = \sum_{j=1}^{N_x} \vec{\psi}_j^{\vec{y}} r_j e^{\lambda_j (t-\tau)}.
\end{equation}

The stability of the LTI system {eq}`eq:lti_sys` can be quantified using various quantities, that we list in {numref}`tab:lti_sys_stability`. The frequency, both natural and damped, measure how quickly the response due to a mode is oscillating. The damping ratio and logarithmic decrement measure how fast the response of a mode is decaying. For undamped modes, the natural and damped frequency coincide. The observed mode shape specifies the direction of the oscillation, as measured on the sensors location. Since it is a complex quantity, it is normally specified in terms of magnitude and phase. The deflection shape combines the magnitude and phase to get a real quantity, which is what it is seen at $t - \tau = 0$. The reader might wonder why one should compute the observed mode shapes rather than the usual ones, and the reason lies in the basis of the state. Sometimes the basis of the state is known, and the usual mode shapes provide meaningful information. However, it is impossible to identify the basis of the state from measurements, and therefore for an identified system only the observed mode shapes are useful. Similarly, when model order reduction is applied to a finite element model, the state loses meaning and again one should compute the observed mode shapes.

```{table} Elements to quantify the stability of a Linear Time-Invariant system.
:name: tab:lti_sys_stability

| Name                       | Expression                                                       | Unit              |
|----------------------------|------------------------------------------------------------------| ------------------|
| Natural frequency          | $f = \abs{\lambda} / (2 \pi)$                                    | Hz                |
| Damped frequency           | $\hat{f} = \abs{\Im(\lambda)} / (2 \pi)$                         | Hz                |
| Damping ratio              | $\zeta = - \Re(\lambda) / \abs{\lambda}$                         | --                |
| Logarithmic decrement      | $\delta = 2 \pi \zeta / \sqrt{1 - \zeta^2}$                      | --                |
| Mode shape                 | $\vec{\psi}$                                                     | Same as $\vec{x}$ |
| Observed mode shape        | $\vec{\psi^y} = \mat{C} \vec{\psi}$                              | Same as $\vec{y}$ |
| Deflection shape           | $\vec{\psi}_r = \abs{\vec{\psi}} \cos(\angle \vec{\psi})$        | Same as $\vec{x}$ |
| Observed deflection shape  | $\vec{\psi^y}_r = \abs{\vec{\psi^y}} \cos(\angle \vec{\psi^y})$  | Same as $\vec{y}$ |
```

````{prf:example} Harmonic oscillator
:label: harmonic_oscillator

We will now illustrate these concepts by meams of an harmonic oscillator, shown in {numref}`fig:harmonic_oscillator_drawing`. The Equation Of Motion (EOM) is given by

\begin{equation}
    m \ddot{x} + c \dot{x} + k x = f.
\end{equation}

:::{figure-md} fig:harmonic_oscillator_drawing
<img src="../figures/harmonic_oscillator/drawing.svg" width="300">

Harmonic oscillator.
:::

We divide the EOM by the mass and introduce $\omega_0^2 = k / m$ and $2 \zeta \omega_0 = c / m$, and by substitution we obtain
\begin{equation}
    \ddot{x} + 2 \zeta \omega_0 \dot{x} + \omega_0^2 x = f / m.
\end{equation}
This second order ODE is equivalent to the following first order ODE, which we call state space form
\begin{equation}
    \begin{pmatrix}
        \dot{x}   \\
        \ddot{x}
    \end{pmatrix} = \begin{bmatrix}
          0           &    1                  \\
        - \omega_0^2  &  - 2 \zeta \omega_0
    \end{bmatrix} \begin{pmatrix}
        x   \\
        \dot{x}
    \end{pmatrix} + \begin{bmatrix}
        0        \\
        1 / m
    \end{bmatrix} f.
\end{equation} 

To better understand the meaning of $\omega_0$ and $\zeta$ we perform a parametric evaluation. The result is shown in the time domain in {numref}`fig:harmonic_oscillator_effect_of_omega0_and_xi_time` and in the frequency domain in {numref}`fig:harmonic_oscillator_effect_of_omega0_and_xi_PSD`. We can observe that:

- Increasing $\omega_0$ leads to shorter periods.
- For $\xi = 0$ the system is undamped and therefore simply stable. The response is periodic and therefore all the energy is contained in exactly 1 frequency bin.
- For $0 <\xi < 1$ the system is underdamped and therefore asymptotically stable. The response will reach 0 in the limit. For low $\xi$ the peaks in the PSD are sharp, while they become smooth for higher values.
- For $\xi = 1$ the system is critically damped. The response does not oscillate and therefore no peak is present in the PSD.


:::{figure-md} fig:harmonic_oscillator_effect_of_omega0_and_xi_time
<img src="../figures/harmonic_oscillator/effect_of_omega0_and_xi_time.svg">

Time series of a parametric evaluation of the harmonic oscillator for various combinations of $\omega_0$ and $\xi$.
:::

:::{figure-md} fig:harmonic_oscillator_effect_of_omega0_and_xi_PSD
<img src="../figures/harmonic_oscillator/effect_of_omega0_and_xi_PSD.svg">

PSD of a parametric evaluation of the harmonic oscillator for various combinations of $\omega_0$ and $\xi$.
:::

````


````{prf:example} Stability of linked oscillators
:label: linked_oscillators

To better understand systems with multiple degrees of freedom we will analyze the system shown in {numref}`fig:linked_oscillators_drawing`. The Equation Of Motion (EOM) is given by

\begin{gather}
    \mat{M} \ddot{\vec{q}} + \mat{C} \dot{\vec{q}} + \mat{K} \vec{q} = \vec{f}
    \\
    \begin{bmatrix}
        m_1  &  0     \\
        0    &  m_2
    \end{bmatrix} \begin{pmatrix}
        \ddot{x}_1   \\
        \ddot{x}_2
    \end{pmatrix} + \begin{bmatrix}
        c_1+c_2  &  -c_2     \\
        -c_2     &  c_2+c_3
    \end{bmatrix} \begin{pmatrix}
        \dot{x}_1   \\
        \dot{x}_2
    \end{pmatrix} + \begin{bmatrix}
        k_1+k_2  &  -k_2     \\
        -k_2     &  k_2+k_3
    \end{bmatrix} \begin{pmatrix}
        x_1   \\
        x_2
    \end{pmatrix} = \begin{pmatrix}
        f_1   \\
        f_2
    \end{pmatrix},
\end{gather}

and is equivalent to the following state space form
\begin{gather}
    \begin{pmatrix}
        \dot{\vec{q}}   \\
        \ddot{\vec{q}}
    \end{pmatrix} = \begin{bmatrix}
          \mat{0}               &    \mat{I}              \\
        - \mat{M}^{-1} \mat{K}  &  - \mat{M}^{-1} \mat{C}
    \end{bmatrix} \begin{pmatrix}
        \vec{q}        \\
        \dot{\vec{q}}
    \end{pmatrix} + \begin{bmatrix}
        \mat{0}         \\
        \mat{M}^{-1}
    \end{bmatrix} \vec{f},
    %
    \\
    %
    \dot{\vec{x}}
        = \mat{A} \vec{x}
        + \mat{B} \vec{u}.
\end{gather}


:::{figure-md} fig:linked_oscillators_drawing
<img src="../figures/linked_oscillators/drawing.svg" width="600">

Linked oscillators.
:::

Let us set some numbers, so that we can compute the stability of the system
\begin{align}
    m_1 &= 2    &  m_2 &= 5    &               \\
    c_1 &= 0.5  &  c_2 &= 0.1  &  c_3 &= 0.1   \\
    k_1 &= 20   &  k_2 &= 10   &  k_3 &= 5     \\
    f_1 &= 0    &  f_2 &= 0    &
\end{align}

By solving the eigenvalue problem we obtain the data in {numref}`tab:linked_oscillators_stability`, which shows that the modes are complex conjugate. The mode shapes are shown in {numref}`fig:linked_oscillators_mode_shapes`. As we can see, for mode 1 and 2 the two masses are moving out-of-phase, while for mode 3 and 4 they are moving in-phase.

```{table} Frequencies and damping ratios of the linked oscillators.
:name: tab:linked_oscillators_stability

|                        | Mode 1         | Mode 2         | Mode 3          | Mode 4          |
|------------------------|----------------|----------------|-----------------|-----------------|
| Eigenvalue [rad/s]     | -0.15 + 3.97 i | -0.15 - 3.97 i | -0.02 + 1.489 i | -0.02 - 1.489 i |
| Natural frequency [Hz] | 0.63           | 0.63           | 0.24            | 0.24            |
| Damping ratio [%]      | 3.77           | 3.77           | 1.35            | 1.35            |
```

:::{figure-md} fig:linked_oscillators_mode_shapes
<img src="../figures/linked_oscillators/mode_shapes.svg">

Mode shapes of the linked oscillators.
:::

The simplest way to observe a mode shape is to use its real part as initial condition, that is $\vec{x}_0 = \Re(\vec{\psi}_j)$. We have done this in {numref}`fig:linked_oscillators_motion_from_mode_shape_1` and {numref}`fig:linked_oscillators_motion_from_mode_shape_3`, which show the response in the time and frequency domain. These plots allow us to observe that:
- A motion that starts on a mode shape tends to remain on it.
- For mode 1 the masses move out-of-phase, while for mode 3 they move in-phase.
- The different damping of the 2 modes is evident by the speed at which the amplitude decays.

:::{figure-md} fig:linked_oscillators_motion_from_mode_shape_1
<img src="../figures/linked_oscillators/motion_from_mode_shape_1.svg">

Response of the linked oscillators from $\vec{x}_0 = \Re(\vec{\psi}_1) \simeq (-0.0091, 0.003 , 0.9581, -0.1497)^\transpose$.
:::

:::{figure-md} fig:linked_oscillators_motion_from_mode_shape_3
<img src="../figures/linked_oscillators/motion_from_mode_shape_3.svg">

Response of the linked oscillators from $\vec{x}_0 = \Re(\vec{\psi}_3) \simeq (0.0059, 0.007 , -0.3024, -0.7732)^\transpose$.
:::

````

The modal decomposition can also be used to evaluate the free response of the system. The forced part of the response requires to compute an integral, which depending on the input time history might not be trivial. Instead of integrating the ODE
\begin{equation}
    \dot{\vec{x}}
        = \mat{A} \vec{x}
    %
    \quad
    %
    \text{from $\vec{x}(0) = \vec{x}_0$},
\end{equation}
one can:
- Compute the eigenvalue decomposition of the system matrix 
\begin{equation}
    \mat{A} \mat{\Psi} = \mat{\Lambda} \mat{\Psi}.
\end{equation}

- Solve the linear system 
\begin{equation}
    \mat{\Psi} \vec{r} = \vec{x}_0,
\end{equation}
to get the scaling factor $\vec{r} = (\dots, r_j, \dots)$.

- Evaluate 
\begin{equation}
    \vec{x}(t) = \sum_{j=1}^{N_x} \vec{\psi}_j r_j e^{\lambda_j t}.
\end{equation}

Since the mode shapes are complex-conjugate the result will be real. Practically, small numerical errors might lead to a complex result, in which case it is sufficient to extract the real part.

The result of this algorithm for the linked oscillators of {prf:ref}`linked_oscillators` is shown in {numref}`fig:linked_oscillators_check_modal_decomposition` which shows a perfect agreement between the two approaches.

:::{figure-md} fig:linked_oscillators_check_modal_decomposition
<img src="../figures/linked_oscillators/check_modal_decomposition.svg">

Comparison of the responses obtained by integrating the ODE and evaluating the modal decomposition for the linked oscillators of {prf:ref}`linked_oscillators`.
:::


(sec:Forced_response_of_LTI_systems)=
## Forced response of Linear Time-Invariant systems

We will now address the problem of the forced response. Given the LTI system {eq}`eq:lti_sys`, with $\vec{x}_0 = \vec{0}$, we have
```{math}
:label: eq:lti_transfer_function_use
    \vec{y}(s) = \mat{G}(s) \vec{u}(s),
```
where $s$ is the Laplace variable and
\begin{equation}
\label{eq:lti_transfer_function_formula}
    \mat{G}(s) = \mat{C} (s \mat{I} - \mat{A})^{-1} \mat{B} + \mat{D}
\end{equation}
the transfer function, with size $N_y \times N_u$. By using {eq}`eq:lti_transfer_function_use`, instead of integrating an ODE we can simply do a multiplication, but we need to compute the Laplace transform of $\vec{u}(t)$ and then antitransform $\vec{y}(s)$. Moreover, since $s$ is a complex variable, $\mat{G}(s)$ can be expensive to compute.

The problem is substantially simplified by restricting $s$ to lie on the imaginary axis. By setting $s = \imath \omega$ we get the Frequency Response Function (FRF). The fundamental theorem of the frequency response states that: a LTI system forced with a sinusoidal input with frequency $\omega_0$ responds with a sinusoidal output with the same frequency, but with different magnitude and phase. In formula:
\begin{equation}
    \vec{u}(t) = \bar{\vec{u}} \sin(\omega_0 t)
    \qquad
    \Rightarrow
    \qquad
    \vec{y}_{\text{steady state}}(t) = \abs{\mat{G}(\imath \omega_0)} \bar{\vec{u}} \sin(\omega_0 t + \angle \mat{G} (\imath \omega_0)).
\end{equation}
The FRF is particularly useful because the Fourier transform allows expressing many signals as a sum of sinusoids (e.g. turbulence). Furthermore, the linearity allows to superimpose the responses. The FRF implies also that only modes with frequency close to $\omega_0$ will be excited.

````{prf:example} Forced response of linked oscillators
:label: linked_oscillators_forced

To better understand the forced response of a LTI system let us further develop {prf:ref}`linked_oscillators`. We will excite the system with a modified chirp signal, shown in {numref}`fig:linked_oscillators_input_chirp`. The chirp is a sinusoidal signal, whose frequency increases linearly with time. The modification consists in the fact that a normal chirp is a cosine, and therefore starts from 1, while in this case we prefer an input that starts from 0, and therefore use a sin function. The chirp goes from 0 to 1 Hz in 1000 s.

:::{figure-md} fig:linked_oscillators_input_chirp
<img src="../figures/linked_oscillators/input_chirp.svg">

First 100 s of the modified chirp signal.
:::

The response of the system is obtained by numerically integrating the ODE, and is shown in {numref}`fig:linked_oscillators_output_chirp`. This figure reveals that, as the frequency of the chirp increases, the system goes through two resonances. The two peaks due to the system modes are clearly visible in the PSD. As shown also in {numref}`fig:linked_oscillators_motion_from_mode_shape_1` and {numref}`fig:linked_oscillators_motion_from_mode_shape_3`, the peak due to the low damped modes is sharp, while the one due to the highly damped modes is smooth.

:::{figure-md} fig:linked_oscillators_output_chirp
<img src="../figures/linked_oscillators/output_chirp.svg">

Time history and PSD of $x_1$.
:::

Just like a white noise, the spectrum of the chirp is flat in the frequency band of interest. However, while a white noise excites all system modes at the same time, a chirp will first excite the low frequency modes and later the high frequency ones. To better observe this, we can compute the Short Time Fourier Transform (STFT), which is shown in {numref}`fig:linked_oscillators_output_stft`. The STFT is a 2D plot, with on the x axis the time and on the y axis the frequency. When the frequency of the chirp crosses the low frequency mode, the first resonance takes place. Since this mode is low damped, it is possible to observe it for a long time. Similarly, around 600 seconds the chirp excites the high frequency mode, but because of its high damping, the resonance fades quickly.

:::{figure-md} fig:linked_oscillators_output_stft
<img src="../figures/linked_oscillators/output_stft.svg">

Short Time Fourier Transform (STFT) of $x_1$.
:::

We will now proceed to compute the FRF, whose Bode diagram is shown in {numref}`fig:linked_oscillators_bode`. We can observe how each input has a different effect on each output. The system modes are clearly visible in the magnitude plot.

:::{figure-md} fig:linked_oscillators_bode
<img src="../figures/linked_oscillators/bode.svg">

Bode diagram for the linked oscillators.
:::

````

As mentioned above, an alternative approach to compute the system response is based on the FRF. In fact, one can:
- Compute the Fast Fourier Transform (FFT) of the input
\begin{equation}
    \hat{\vec{u}}(\omega) = \mathcal{F}(\vec{u}(t)).
\end{equation}
Since the input signal $\vec{u}(t)$ is real, its FFT $\hat{\vec{u}}(\omega)$ is Hermitian-symmetric. This means that the negative frequency terms are just the complex conjugates of the corresponding positive-frequency ones, $\hat{\vec{u}}(\omega) = \hat{\vec{u}}^*(-\omega)$. There are ad hoc versions of the standard FFT that reduce the computational cost and memory for this case, by only computing the left part of the spectrum.
- Compute the FRF
\begin{equation}
    \mat{G}(\omega) = \mat{C} (\imath\omega \mat{I} - \mat{A})^{-1} \mat{B} + \mat{D}.
\end{equation}
- Multiply the transformed input by the FRF, to obtain the output in the frequency domain.
\begin{equation}
     \hat{\vec{y}}(\omega) = \mat{G}(\omega) \hat{\vec{u}}(\omega)
     \qquad
     \forall \omega.
\end{equation}
- Compute the inverse FFT of the output, to convert it in the time domain
\begin{equation}
    \vec{y}(t) = \mathcal{F}^{-1} \hat{\vec{y}}(\omega).
\end{equation}

The result of this algorithm for the linked oscillators of {prf:ref}`linked_oscillators_forced` is shown in {numref}`fig:output_ode_vs_frf` which shows a perfect agreement between the two approaches.

:::{figure-md} fig:output_ode_vs_frf
<img src="../figures/linked_oscillators/output_ode_vs_frf.svg">

Comparison between the response obtained by integrating the ODE and the one obtained by using the FRF for the linked oscillators of {prf:ref}`linked_oscillators_forced`.
:::

Evaluating the response in the frequency domain can be more efficient than integrating the ODE in some applications, and has been used in various works. For example, this approach has been adopted by A. Pegalajar-Jurado in the offshore wind turbine simulator QuLAF {cite}`APegalajarJurado_etal_QuLAF_2018`, and more recently in RAFT {cite}`MHall2022_RAFT`. The frequency domain approach is efficient because it adopts fast operations, namely: FFT, inverse FFT and matrix multiplication. It is thus convenient when the analyst needs to evaluate several load cases. The most computationally demanding part is the computation of the FRF, and in particular solving the linear system $(\imath\omega \mat{I} - \mat{A})^{-1} \mat{B}$, which for high dimensional systems becomes very expensive. In such cases, model order reduction techniques should be applied first.
